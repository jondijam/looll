@extends('layouts/master')

@section('content')
<div class="row">
  <div class="col-sm-6 col-sm-offset-3 search-box">
    <h1>looll <small class="white">Search</small></h1>
    {{ Form::open(['route' => 'search.index', 'method' => 'GET']) }}
        <div class="form-group">
          <div class="input-group">
            {{ Form::input('search', 'q', null, ['placeholder' => Lang::get('looll.SearchWords'), 'class' => 'form-control'] ) }}
            <span class="input-group-btn">
              {{ Form::submit(Lang::get('looll.Search'), ['class' => 'btn btn-default']); }}
            </span>
          </div>
        </div>
        <div>
        @if(Auth::guest())
        <div class="row">
          <div class="col-sm-4 col-sm-offset-3">
            <h4 class="text-left">Already a member</h4>        
          </div>
          <div class="col-sm-5">
            {{
                link_to_route('login', 'Sign &nbsp;in', $parameters = array(), $attributes = array('class'=>'btn btn-primary btn-sm'));
            }}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 col-sm-offset-3">
            <h4>Join for free</h4>
          </div>
           <div class="col-sm-5">
            {{
                link_to_route('register', 'Sign up', $parameters = array(), $attributes = array('class'=>'btn btn-primary btn-sm'));
            }}
          </div>
        </div>
        @endif
       
    

      {{ Form::close() }}
  </div>
</div>
<div class="row">

  <div class="col-sm-6 col-sm-offset-3 box">
    <div class="well" style="opacity: 0.8;">
    <p>Looll.is is a website that helps individuals and organizations to provide contact information to those who need it. All information placed on the site is visible to everyone and when a user deletes information from the site, it is permanently deleted from our servers.</p>
  
  <h4><strong>Individuals</strong></h4>
  <p>For individuals, the site is an advanced address book with information entered by the users. It is our hope that users make their page personal with pictures and descriptive texts about themselves. It is up to each user to decide what information he or she likes to share.</p>
  
  <h4>Groups</h4>
  <p>The site makes it easier for organizations to get messages to members.  Pages for organizations are created by their current members who enter the relevant information and become administrators for the organization´s page.  The administrator can publish messages or news, which are easy to change or update.  In the near future the messages will automatically be sent to members  </p>
  </div>
  </div>
</div>
@stop

@section('javascript')
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53044404-1', 'looll.is');
      ga('send', 'pageview');
    </script>
@stop