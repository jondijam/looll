<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>
		<div>
			Your username is {{$user->username}}
		</div>
		<div>
			To reset your password, {{ link_to('password/reset/'.$token.'', "complete this form.", $attributes = array()); }}
		</div>
	</body>
</html>
