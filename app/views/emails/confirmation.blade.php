<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Welcome to looll.is</h2>
		<div>
			<p>Please verify your email by using your mouse and push on the left button. Maybe once, sometimes twice.</p> 
			<p>
				{{
					link_to_route('confirmed', "Please verify!", $parameters = array('username' => $username, 'code' => $confirmation_code), $attributes = array());
				}}
			</p>
		</div>
	</body>
</html>
