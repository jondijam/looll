    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@lang('looll.AboutText')" />
    <meta name="robots" content="index,follow" />
    <title>Looll.is</title>

    <!-- Bootstrap -->
    {{ HTML::style('css/min/looll.css?v=2') }}
    {{ HTML::style('css/jquery-te-1.4.0.css') }}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="/img/favicon.ico" />
    <link rel="canonical" href="http://looll.is/" />
    <!--[if IE]>
    <script type="text/javascript" src="/js/css3-multi-column.js"></script>
    <![endif]-->