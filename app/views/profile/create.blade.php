@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-sm-3 box"></div>
		{{
			Form::open(array('route' => 'users.profiles.store', 'data-remote'))
		}}
		<div class="col-sm-5 box">

		</div>	
		<div class="col-sm-4 box">
			<div class="panel-group" id="accordion">
			@include('includes.profile.form')
			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="save" id="save" data-profile-id="{{{$profileId}}}" value="@lang('looll.Save')" />
				<input type="submit" data-profile-id="{{{$profileId}}}" class="btn btn-success hidden" value="Your profile was saved" id="success" >
			</div>
			</div>
		</div>
		{{ 
			Form::close() 
		}}
	</div>
@stop
@section('javascript')
<script src="/js/profile/dist/main.bundle.min.js?v=1.3"></script>
		<script type="text/javascript">
				$('textarea').jqte();
		</script>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
@stop