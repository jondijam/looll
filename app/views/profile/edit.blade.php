@extends('layouts/master')
@section('content')
	<div class="row">
		<div class="col-sm-3 box">
			<div class="panel panel-default  @if(count($photos) == 5) hidden @endif">
				<div class="panel-heading">
					<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#profile_images">@lang('looll.Images')</a></h4>
				</div>
				<div id="profile_images" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="form-group form-images">
							<label>@lang('looll.YourImage')</label>
							<div id="droparea" data-organisation-id="0" data-profile-id="{{$profile->id}}">
								<div class="dropareainner">
									<p class="dropfiletext">@lang('looll.DropFilesHere')</p>
									<p>@lang('looll.Or')</p>
									<p>
									<input id="uploadbtn" type="button" value="@lang('looll.SelectFiles')"/></p>
											<!-- extra feature -->
									<p id="err"><!-- error message -->
									</p>
								</div>
								<input id="upload" type="file" multiple="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="result">
				@if(count($photos))
					@foreach($photos as $image)
						<div class="imageholder" id="imageholder{{{$image['id']}}}">
							<figure>
								<img src="{{{$image['src']}}}" alt="" class="image" data-image-id="{{{$image['id']}}}"/>
								<span class="fa fa-times-circle remove-images" data-image-id="{{{$image['id']}}}"></span>
								<span class="fa @if($image['image_primary']) fa-star @else fa-star-o @endif primary-image" data-profile-id="{{$profile->id}}" data-image-id="{{{$image['id']}}}" data-primary="{{$image['image_primary']}}"></span>
							</figure>
						</div>
					@endforeach
				@endif
			</div>
		</div>
		{{
			Form::model($profile, array('route' => array('users.profiles.update', 'user' => $username, 'profile'=>$profile->id), 'data-remote', 'method' => 'PUT'))
		}}
		<div class="col-sm-5 box">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">@lang('looll.Announcements')</h4>
				</div>
				<div class="panel-body">
					@if(count($announcementsByOrganisations))
						<div class="list-group">
						@foreach($announcementsByOrganisations as $organisation)
							@if(!empty($organisation->announcement) || !empty($organisation->private_announcement) )
							<div class="list-group-item">
								<h4 class="list-group-item-heading">{{ $organisation->name }} <small>{{$organisation->updated_at->diffForHumans(); $organisation->updated_at }}</small></h4>
								@if(!empty($organisation->announcement)) 
								<div class="list-group-item-text">{{ $organisation->announcement }}</div>
								@endif

								@if(!empty($organisation->private_announcement))
								<div class="list-group-item-text box">{{ $organisation->private_announcement }}</div>
								@endif
							</div>
							@endif
						@endforeach
						</div>
					@endif

					 {{ $announcementsByOrganisations->links() }}
				</div>
			</div>
		</div>	
		<div class="col-sm-4 box">
			<div class="panel-group" id="accordion">
				@include('includes.profile.form')
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#groups">
					        	@lang('looll.Organisations')
					        </a>
					     </h4>
					</div>
					<div id="groups" class="panel-collapse collapse">
						<div class="panel-body">
							@if(isset($organisations))
								<table class="table table-condensed">
									<thead>
										<tr>
											<th>@lang('looll.Name')</th>
											<th>@lang('Action')</th>
										</tr>
									</thead>
									<tbody>
										@foreach($organisations as $organisation)
											@if($organisation['pivot']['role_id'] == 3)
												<tr id="{{$organisation['id']}}">
													<td>
														<a href="/organisation/{{{$organisation['id']}}}">{{$organisation['name']}}</a></td>
													<td>
													<td>
														{{
															link_to_route('organisations.users.destroy', Lang::get('looll.LeaveGroup'), [$organisation['id'], Auth::id()], ['data-organisation-id'=>$organisation['id'] ,'class' => 'btn btn-warning leaveGroup'])

														}}
													</td>
												</tr>
											@endif
											
											@if($organisation['pivot']['role_id'] == 1)
												<tr id="{{$organisation['id']}}">
													<td>
														<a href="/organisation/{{{$organisation['id']}}}">{{$organisation['name']}}</a></td>
													<td>
													<td>
														{{
															link_to_route('organisation.edit', Lang::get('looll.Edit'), $parameters = array($organisation['id']), $attributes = array('class'=>'btn btn-primary')) 
														}}

														{{
															link_to_route('organisation.destroy', Lang::get('looll.Delete'), $parameters = array($organisation['id']), $attributes = array('data-organisation-id'=>$organisation['id'], 'data-user-id'=>Auth::id(), 'class'=>'btn btn-danger deleteGroup')) 
														}}
													</td>
												</tr>
											@endif
										@endforeach
									</tbody>
								</table>
							@endif
							{{
								link_to_route('organisation.create', Lang::get('looll.CreateOrganisation'),[],['class' => 'btn btn-primary'])
							}}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="save" id="save" data-profile-id="{{{$profile->id}}}" value="@lang('looll.Save')" />
				<input type="submit" data-profile-id="{{{$profile->id}}}" class="btn btn-success hidden" value="Your profile was saved" id="success" >
			</div>
		</div>
		{{ 
			Form::close() 
		}}
	</div>
@stop
@section('javascript')
<script type="text/javascript" src="/js/modernizr.custom.js"></script>
<script src="/js/profile/dist/main.bundle.min.js?v=2.3"></script>
		<script type="text/javascript">
				$('textarea').jqte();
		</script>
    
@stop