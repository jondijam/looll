@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="page-header">
      <h1>{{$profile->name}}</h1> 
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-3">
    @if(count($photos))
      <div class="row">
        <div class="col-sm-12">
          @foreach($photos as $image)
              @if($image['image_primary'] == 1)
                <img src="{{{$image['src']}}}" alt="{{ $profile->name }}" class="img-thumbnail" />
              @endif
          @endforeach
        </div>
        <div class="col-sm-12">
          <div class="crop">
            @foreach($photos as $image)
              @if($image['image_primary'] == 0)
                <img src="{{{$image['src']}}}" alt="{{ $profile->name }}" class="img-thumbnail" />
              @endif
            @endforeach
          </div>
        </div>
      </div>
    @endif
  </div>
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">@lang('looll.Information')</h4>
      </div>
      <div class="panel-body">
        @if($profile->address != "")
        <div class="clearfix">
           <label class="address">@lang('looll.Address')</label>
           <span class="pull-right">{{$profile->address}}</span>
        </div>
        @endif
        @if( ($profile->city != "") && ($profile->zip != ""))
        <div class="clearfix">
           <label class="address">@lang('looll.City')</label>
           <span class="pull-right">{{{$profile->zip}}} {{$profile->city}}</span>
        </div>
        @endif
        @if(!empty($profile->country->name))
        <div class="clearfix">
          <label class="address">@lang('looll.Country')</label>
          <span class="pull-right"> @if(!empty($profile->country->name)){{$profile->country->name}} @endif</span>
        </div>
        @endif
        @if($email != "")
        <div class="clearfix">
           <label class="address">@lang('looll.Email')</label>
           <span class="pull-right">{{{$email}}}</span>
        </div>
        @endif
        @if(count($homePhone))
        <div class="clearfix">
          <label class="address">@lang('looll.Tel')</label>
          <span class="pull-right"> @if(count($homePhone)){{$homePhoneNumber}} @endif</span>
        </div>
        @endif
        @if(count($mobilePhone))
        <div class="clearfix">
          <label>@lang('looll.Mobile')</label>
          <span class="pull-right"> @if(count($mobilePhone)) {{{$mobilePhoneNumber}}} @endif </span>
        </div>
        @endif
        <div class="clearfix">
           <label class="address">@lang('looll.MaritalStatus')</label>
           <span class="pull-right">
           @if($profile->marital_status == 1)
            @lang('looll.Single')
           @elseif($profile->marital_status == 2)
            @lang('looll.Relationship')
           @elseif($profile->marital_status == 3)
            @lang('looll.Engaged')
           @elseif($profile->marital_status == 4)
            @lang('looll.Married')<
           @endif
           </span>
        </div>
        @if(!empty($profile->birthdate))
        <div class="clearfix">
           <label class="address">@lang('looll.Birthday')</label>
           <span class="pull-right">{{{$profile->birthdate}}}</span>
        </div>
        @endif
        <br />
        @if(!empty($profile->company))
        <div class="clearfix">
          <label class="address">@lang('looll.Company')</label>
          <span class="pull-right">{{$profile->company}}</span>
        </div>
        @endif
        @if(!empty($profile->position))
        <div class="clearfix">  
          <label class="address">@lang('looll.Position')</label>
          <span class="pull-right">{{{$profile->position}}}</span>
        </div>
        @endif
        @if(count($buisnessPhone))
        <div class="clearfix">
          <label class="address">@lang('looll.WorkPhoneNumber')</label>
          <span class="pull-right"> @if(count($buisnessPhone)){{{$buisnessPhoneNumber}}} @endif </span>
        </div>
        @endif
      </div>
    </div>
    @if(!empty($profile->announcement))
    <div class="panel panel-default">
      <div class="panel-heading"><h4 class="panel-title">@lang('looll.Announcement')</h4></div>
      <div class="panel-body">{{$profile->announcement}}</div>
    </div>
    @endif
  </div>
  <div class="col-sm-3">
     <div class="panel panel-default">
            <div class="panel-heading">
               <h4 class="panel-title">@lang('looll.Description')</h4>
            </div>
            <div class="panel-body">
               {{$profile->description}}
            </div>
      </div>
  </div>
</div>
@stop