<!DOCTYPE html>
<html lang="en">
	<head>
    	@include('head')
  	</head>
  	<body>
  		@include('includes/navigation')
  		<div class="container">
  			<div class="row">
  				<div class="col-sm-3">
  	 			</div>
  	 			<div class="col-sm-5">
  	 				<h2>@lang("looll.ConfirmYourEmail")</h2>
  	 				<p>@lang("looll.BeforeYourLoginPleaseConfirmYourEmail")</p>
  	 			</div>
  			</div>
  		</div>
  	</body>
</html>