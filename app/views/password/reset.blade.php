@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-sm-5 col-sm-offset-3 search-box">
      @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
      @endif
      <form action="{{ action('RemindersController@postReset') }}" method="POST">
        <div class="form-group">
          {{ Form::label('email', 'Email') }}
          <input type="text" name="email" id="email" class="form-control" value="" />
        </div>
        <div class="form-group">
          {{ Form::label('password', 'Password') }}
          <input type="password" class="form-control" name="password" id="password" />
        </div>
        <div class="form-group">
          {{ Form::label('password_confirmation', 'Password confirm') }}
          <input name="password_confirmation" class="form-control" type="password" value="" id="password_confirmation">
        </div>
        <input type="hidden" name="token" value="{{ $token }}" />
        <div class="form-group">{{ Form::submit('Reset') }}</div>
      </form>
  </div>
</div>
@stop