@extends('layouts.master')

@section('content')
 <div class="row">
  <div class="col-sm-5 col-sm-offset-3 search-box">
    @if(Session::has('error'))
      <div class="alert alert-danger"><p>{{Session::get('error')}}</p></div>
    @elseif(Session::has('status'))
      <div class="alert alert-success"><p>{{Session::get('status')}}</p></div>
    @endif
    <form action="{{ action('RemindersController@postRemind') }}" method="POST">
       <div class="form-group">
          {{
            Form::label('email', Lang::get(('looll.Email')));
          }}
          <input type="email" name="email" id="email" class="form-control" />
       </div>
       <div class="form-group">
           <input type="submit" value="@lang('looll.SetPassword')" class="btn btn-primary" />
       </div>
    </form>
  </div>
 </div>
@stop