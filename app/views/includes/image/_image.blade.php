<div class="imageholder" id="imageholder{{$photo->id}}">
	<figure>
		<img src="{{$photo->src}}" alt="{{$photo->filename}}" class="image" data-image-id="{{$photo->id}}">
		<span class="fa fa-times-circle remove-images" data-image-id="{{$photo->id}}"></span>
		<span class="fa @if($photo->image_primary == 0) fa-star-o @else fa-star @endif primary-image" data-image-id="{{$photo->id}}" data-primary="{{$photo->image_primary}}"></span>
	</figure>
</div>