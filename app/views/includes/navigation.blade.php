<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <a href="/" class="navbar-brand">looll.is</a>
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse" id="navbar-main">
        <ul class="nav navbar-nav">
          <li class="{{ set_active('about') }}">
            <a href="/about">@lang('looll.About')</a>
          </li>
        </ul>
        @if(Auth::check())
          <ul class="nav navbar-nav navbar-right">
            <li class="{{ set_active(Auth::user()->username) }}">
               {{ link_to_profile(Lang::get('looll.MySite')) }}
            </li>
            <li class="{{ set_active('profile/'.Auth::user()->username.'/edit') }}" >
              {{ link_to_edit_profile(Lang::get('looll.MyProfile')) }}
            </li>
            <li>
              <a href="/logout">@lang('looll.Logout')</a>
            </li>
          </ul>
        @else
        <ul class="nav navbar-nav navbar-right">
          <li class="{{ set_active('register') }}">
            <a href="/register">@lang('looll.Registration')</a>
          </li>
          <li class="{{ set_active('login') }}">
            <a href="/login">@lang('looll.Login')</a>
          </li>
        </ul>
        @endif     
      </div>
    </div>
</div>