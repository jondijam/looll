<div class="col-sm-5 search-box">
	<div class="panel-group" id="accordion">
	
		<div class="panel panel-default">
			<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#organisation_announcement">
							@lang('looll.Announcement')
						</a>
					</h4>
			</div>
			<div id="organisation_announcement" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="form-group">
						{{ Form::textarea('announcement', $announcement,['class'=>'form-control']) }}
					</div>
				</div>
			</div>
		</div>
		@if(isset($organisation))
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#organisation_private_announcement">
						@lang('looll.PrivateAnnouncement')
					</a>
				</h4>
			</div>
			<div id="organisation_private_announcement" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="form-group">
						{{ Form::textarea('private_announcement', $private_announcement ,['class'=>'form-control']) }}
					</div>
				</div>
			</div>
		</div>
		@endif
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#organisation_informations">
							@lang("looll.Information")
						</a>
					</h4>
			</div>
			<div id="organisation_informations" class="panel-collapse collapse @if($errors->any()) in @endif)">
				<div class="panel-body">
					<div class="form-group">
					{{
						Form::label('name', Lang::get('looll.Name')); 
					}}
					{{
						Form::text('name', null, array('class' => "form-control"));
					}}
					<label class="label label-danger @if(!$errors->any()) hidden @endif name">{{$errors->first('name')}}</label>
					</div>

					<div class="form-group">
					{{
						Form::label('description', Lang::get('looll.About')); 
					}}
					{{
						Form::textarea('description', $description, ['class'=>'form-control'])
					}}
					</div>
					<div class="form-group">
					{{
						Form::label('email', Lang::get('looll.Email')); 
					}}
					{{
						Form::email('email', null, array('class' => "form-control"));
					}}
					<label class="label label-danger hidden email"></label>
					</div>
					<div class="form-group">
					{{
						Form::label('address', Lang::get('looll.Address')); 
					}}
					{{
						Form::text('address', null, array('class' => "form-control"));
					}}
					</div>
					<div class="form-group">
					{{
						Form::label('zip', Lang::get('looll.Zip')); 
					}}
					{{
						Form::text('zip', null, array('class' => "form-control"));
					}}
					</div>
					<div class="form-group">
					{{
						Form::label('city', Lang::get('looll.City')); 
					}}
					{{
						Form::text('city', null, array('class' => "form-control"));
					}}
					</div>
					<div class="form-group">
					{{
						Form::label('country', Lang::get('looll.Country')); 
					}}
					{{
						Form::select('country_id', $countries, $country_id, ['class'=>'form-control', 'id'=>'country_id']);
					}}
					</div>
					<div class="form-group">
					{{
						Form::label('member_type_id', Lang::get('looll.TypeOfMember')); 
					}}
					{{
						Form::select('member_type_id', ['1' => Lang::get('looll.NoMembersCanJoin'), '2'=>Lang::get('looll.PeopleCanJoin') ,'3'=> Lang::get('looll.OrganisationsCanJoin')], $member_type_id, ['class'=>'form-control']);
					}}
					</div>
					@if(count($parents))
						<div class="form-group">
							{{
								Form::label('parent_id', Lang::get('looll.MemberOf')); 
							}}
							{{
								Form::select('parent_id', $parents, $parent_id, ['class'=>'form-control', 'id'=>'parent_id']);
							}}
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#organisation_contacts">
						@lang("looll.Contacts")
					</a>
				</h4>
			</div>
			<div id="organisation_contacts" class="panel-collapse collapse">
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>
									@lang('looll.Name')
								</th>
								<th>
									@lang('looll.Action')
								</th>
							</tr>
						</thead>
						<tbody class="contacts-tbody">	
							@if(isset($contacts))
								@foreach($contacts as $contact)
									<tr id="deleteContact{{$contact->id}}">
										<th>
											{{$contact->profile->name}}
										</th>
										<th>
											<a href="" class="removeContact btn btn-danger" id="removeContact{{$contact->id}}" data-organisation-id="{{$id}}" data-user-id="{{$contact->id}}" data-contacts-count="{{$contacts_counts}}">@lang('looll.Delete')</a>
										</th>
									</tr>
								@endforeach
							@else
								<tr class="empty">
									<td colspan="2">
										There are no contacts registered
									</td>
								</tr>
							@endif	
						</tbody>
					</table>
					<div class="form-group">
						<input type="text" name="contact" id="contact_search"  class="@if( count($organisation) == 0 || count($contacts) >= 2) hidden @endif form-control" data-organisation-id="{{$id}}" data-contacts-count="{{count($contacts)}}" value="" />
						
						<div class="result @if(count($contacts) == 2) hidden @endif"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="submit" value="@lang('looll.Save')" id="save" data-organisation-id="{{$id}}" class="btn btn-primary">
</div>