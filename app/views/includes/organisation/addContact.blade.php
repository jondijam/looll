<tr id="deleteContact{{$userId}}">
	<td>{{$user->profile->name}}</td>
	<td><a href="" class="removeContact btn btn-danger" id="removeContact{{$userId}}" data-contacts-count="{{$contacts_count}}" data-organisation-id="{{$organisationId}}" data-user-id="{{$userId}}">@lang('looll.Delete')</a></td>
</tr>