<div class="alert alert-danger hidden"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
	        <a data-toggle="collapse" data-parent="#accordion" href="#account">
	          @lang('looll.Account')
	        </a>
	    </h4>
	</div>
	<div id="account" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="form-group">
				<select class="form-control" id="active" name="active">
					<option @if($active) selected="selected" @endif value="1">@lang('looll.Active')</option>
					<option @if(!$active) selected="selected" @endif value="0">@lang('looll.Inactive')</option>
				</select>
			</div>
			<div class="form-group">
				<a href="/" class="btn btn-danger deleteAccount">@lang('looll.DeleteMyAccount')</a>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#profile_announcement">
				@lang('looll.Announcement')
			</a>
		</h4>
	</div>
	<div id="profile_announcement" class="panel-collapse collapse in">
		<div class="panel-body">
			<div class="form-group">
				{{
					Form::textarea('announcement', $announcement, array('id'=>'announcement', 'class'=>'form-control'));
					}}
				{{--
					<textarea id="announcement" name="announcement" class="form-control">{{{$announcement}}}</textarea>
				--}}
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#profile_personal_info">
								@lang('looll.PersonalInformation')
							</a>
						</h4>
					</div>
					<div id="profile_personal_info" class="panel-collapse collapse @if(!isset($profile)) in @endif">
						<div class="panel-body">
							<div class="form-group">
								{{ Form::label('name', Lang::get("looll.Name")) }}
								{{ Form::text('name', null, ['class'=>'form-control', 'id'=>'name']) }}
								<span class="label label-danger message-name hidden"></span>
							</div>
							<div class="form-group">
							{{
								Form::label('description', Lang::get("looll.Description"))
								}}
							{{
								Form::textarea('description', null, ['class'=>'form-control', 'id'=>'description'])
								}}
							
							</div>
							<div class="form-group">
								{{
									Form::label('email', Lang::get("looll.Email"))
									}}
								{{
									Form::email('email', null, ['class' => 'form-control', 'id'=>'email'])
									}}
							</div>
							<div class="form-group">
								{{
									Form::label('birthday', Lang::get("looll.Birthday"))
									}}
								{{
									Form::input('date','birthdate', null, ['class'=>'form-control', 'id'=>'birthday'])
									}}
							</div>
							<div class="form-group">
								{{
									Form::label('marital_status', Lang::get("looll.MaritalStatus"))
									}}
								{{
									Form::select('marital_status', array('0' => "", '1' => Lang::get("looll.Single"),'2' => Lang::get("looll.Relationship"),'3' => Lang::get("looll.Engaged"),'4' => Lang::get("looll.Married")), $marital_status,  ['class'=>'form-control', 'id'=>'marital_status']);
									}}
							</div>
							<div class="form-group">
								{{
									Form::label('gender', Lang::get("looll.Gender"))
									}}
								{{
									Form::select('gender', ['male'=>Lang::get("looll.Male"), "female" => Lang::get("looll.Female")], null, ['class'=>'form-control', 'id'=>'gender'])
									}}
							</div>
							<div class="form-group">
							{{
								Form::label('address', Lang::get('looll.Address')); 
							}}
							{{
								Form::text('address', null, array('class' => "form-control"));
							}}
							</div>
							<div class="form-group">
							{{
								Form::label('zip', Lang::get('looll.Zip')); 
							}}
							{{
								Form::text('zip', null, array('class' => "form-control"));
							}}
							</div>
							<div class="form-group">
							{{
								Form::label('city', Lang::get('looll.City')); 
							}}
							{{
								Form::text('city', null, array('class' => "form-control"));
							}}
							</div>
							<div class="form-group">
								{{
									Form::label('country', Lang::get('looll.Country')); 
								}}
								{{
									Form::select('country_id', $countries, $country_id, ['class'=>'form-control', 'id'=>'country_id']);
								}}
							</div>
							<div class="form-group">
							{{
								Form::label('home_phone_number', Lang::get('looll.TelephoneNumber')); 
							}}
							{{
								Form::text('home_phone_number', $homePhoneNumber, array('class' => "form-control", 'data-homephone-id'=>$homePhoneId));
							}}
							{{
								Form::hidden('home_phone_id', $homePhoneId);
							}}
							</div>
							<div class="form-group">
							{{
								Form::label('mobile_phone_pumber', Lang::get('looll.MobileNumber')); 
							}}
							{{
								Form::text('mobile_phone_number', $mobilePhoneNumber, array('class' => "form-control", 'data-mobile-id'=>$mobilePhoneId));
							}}
							{{
								Form::hidden('mobile_phone_id', $mobilePhoneId);
							}}
							</div>
							<h3>@lang('looll.YourWork')</h3>
							<div class="form-group">
							{{
								Form::label('company', Lang::get('looll.Company')); 
							}}
							{{
								Form::text('company', null, array('class' => "form-control",'id'=>'company'));
							}}
							</div>
							<div class="form-group">
							{{
								Form::label('buisness_phone_number', Lang::get('looll.WorkPhoneNumber')); 
							}}
							{{
								Form::text('buisness_phone_number', $buisnessPhoneNumber, array('class' => "form-control",'id'=>'buisness_phone_number', 'data-business-phone-id'=>$buisnessPhoneId));
							}}
							{{
								Form::hidden('buisness_phone_id', $buisnessPhoneId);
							}}
							</div>
							<div class="form-group">
								{{
									Form::label('position', Lang::get('looll.Position')); 
									}}
								{{
									Form::text('position', null, array('class' => "form-control",'id'=>'position'));
									}}
							</div>
						</div>
					</div>
				</div>