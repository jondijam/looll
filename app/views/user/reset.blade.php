<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
  	<div class="navbar navbar-default">
  		<div class="container">
  	 		<div class="navbar-header">
	          <a href="/" class="navbar-brand">looll.is</a>
	          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	        </div>
	        <div class="navbar-collapse collapse" id="navbar-main">
	        <ul class="nav navbar-nav navbar-right">
	            <li>
	            	<a href="/register">@lang("Register")</a>
	            </li>
	            <li  class="active">
	            	<a href="/login">@lang("Login")</a>
	            </li>
	        </ul>
	        </div>
	    </div>
  	</div>
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-3">
  	 		</div>
  	 		<div class="col-sm-5">
  	 			@if (Session::has('error'))
          {{ trans(Session::get('reason')) }}
        @endif
         
        {{ Form::open(array('route' => array('password.update', $token))) }}
         
          <div class="form-group">
            {{ Form::label('email', 'Email') }}
            <input type="text" name="email" id="email" class="form-control" value="" />
          </div>
          <div class="form-group">
            {{ Form::label('password', 'Password') }}
            <input type="password" class="form-control" name="password" id="password" />
          </div>
          <div class="form-group">
            {{ Form::label('password_confirmation', 'Password confirm') }}
            <input name="password_confirmation" class="form-control" type="password" value="" id="password_confirmation">
          </div>
          {{ Form::hidden('token', $token) }}
         
          <div class="form-group">{{ Form::submit('Submit') }}</div>
         
        {{ Form::close() }}
  	 		</div>
  		</div>
  	</div>
  </body>
 </html>