<!DOCTYPE html>
<html lang="en">
  <head>
    @include('head')
  </head>
  <body>
  	<div class="navbar navbar-default">
  		<div class="container">
  	 		<div class="navbar-header">
	          <a href="/" class="navbar-brand">looll.is</a>
	          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	        </div>
	        <div class="navbar-collapse collapse" id="navbar-main">
	        <ul class="nav navbar-nav navbar-right">
	            <li>
	            	<a href="/register">@lang("Register")</a>
	            </li>
	            <li  class="active">
	            	<a href="/login">@lang("Login")</a>
	            </li>
	        </ul>
	        </div>
	    </div>
  	</div>
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-3">
  	 		</div>
  	 		<div class="col-sm-5">
  	 			<form action="/login/forgotpass" method="post">
  	 				<div class="form-group">
  	 					<label>@lang('looll.Email')</label>
  	 					<input type="text" class="form-control" name="email" value="" />
  	 				</div>
  	 			
  	 				<div class="form-group">
  	 					<input type="submit" class="btn btn-primary" name="login" value="@lang('looll.SetPassword')" />
  	 				</div>
  	 			</form>
  	 		</div>
  		</div>
  	</div>
  </body>
 </html>