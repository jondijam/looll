<!DOCTYPE html>
	<html lang="en">
		<head>
	    	@include('head')
		</head>
		<body>
			<div class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand">looll.is</a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav navbar-right">
							@if(count($count_user_organisations) > 0 && $profile_id > 0)
							<li>
								<a href="/organisation">@lang('looll.Organisation')</a>
							</li>
							@endif
							<li class="active">
								<a href="/profile">@lang('looll.MyProfile')</a>
							</li>
							@if($profile_id > 0)
							<li>
								<a href="/profile/{{$profile_id}}">@lang('looll.MySite')</a>
							</li>
							@endif
							<li>
								<a href="/logout">@lang('looll.Logout')</a>
							</li>	
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div id="result">
							@if(count($images))
								@foreach($images as $image)
								<div class="imageholder" id="imageholder{{{$image->image_id}}}">
									<figure>
										<img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="image" data-image-id="{{{$image->image_id}}}"/>
										<span class="fa fa-times-circle remove-images" data-image-id="{{{$image->image_id}}}"></span>
										<span class="fa @if($image->image_primary) fa-star @else fa-star-o @endif primary-image" data-image-id="{{{$image->image_id}}}" data-primary="{{$image->primary}}"></span>
									</figure>
								</div>
								@endforeach
							@endif
						</div>
					</div>
					<div class="col-sm-5">
						<form action="" method="post">
						<h3>@lang('looll.Profile')</h3>
						<div class="form-group form-images @if(count($images) == 5 && $profile_id > 0) hidden @endif">
							<label>@lang('looll.YourImage')</label>
							<div id="droparea" data-organisation-id="0" data-profile-id="{{$profile_id}}">
											<div class="dropareainner">
												<p class="dropfiletext">@lang('looll.DropFilesHere')</p>
												<p>@lang('looll.Or')</p>
												<p>
												<input id="uploadbtn" type="button" value="@lang('looll.SelectFiles')"/></p>
												<!-- extra feature -->
												<p id="err"><!-- error message -->
												</p>
											</div>
								<input id="upload" type="file" multiple="">
							</div>
						</div>
						
						<div class="alert alert-danger hidden">
							<ul class="error">
								
							</ul>
						</div>
						
						<div class="form-group">
							<label>@lang('looll.Name')*</label>
							<input type="text" id="name" class="form-control" name="name" value="{{{$name}}}"/>
						</div>
						<div class="form-group">
							<select class="form-control" id="active">
								<option @if($active) selected="selected" @endif value="1">Active</option>
								<option @if(!$active) selected="selected" @endif value="0">InActive</option>
							</select>
						</div>
						<div class="form-group">
							<label>@lang('looll.Description')</label>
							<textarea id="description" class="form-control">{{{$description}}}</textarea>
						</div>
						<div class="form-group">
							<label>@lang('looll.Announcement')</label>
							<textarea id="announcement" class="form-control">{{{$announcement}}}</textarea>
						</div>
						<div class="form-group">
							<label>@lang('looll.Email')*</label>
							<input type="email" id="email" class="form-control" name="email" value="{{{$email}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.Birthday')</label>
							<input type="date" id="birthday" name="birthday" class="form-control" value="{{{$birthday}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.MaritalStatus')</label>
							<select class="form-control" name="marital_status" id="marital_status">
								<option @if($marital_status == 0) selected="selected" @endif value="0"></option>
								<option @if($marital_status == 1) selected="selected" @endif value="1">@lang('looll.Single')</option>
								<option @if($marital_status == 2) selected="selected" @endif value="2">@lang('looll.Relationship')</option>
								<option @if($marital_status == 3) selected="selected" @endifvalue="3">@lang('looll.Engaged')</option>
								<option @if($marital_status == 4) selected="selected" @endifvalue="4">@lang('looll.Married')</option>
							</select>
						</div>
						<div class="form-group">
							<label>@lang('looll.Address')*</label>
							<input type="text" class="form-control" id="address" name="address" value="{{{$address}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.Zip')*</label>
							<input type="text" id="zip" class="form-control" name="zip" value="{{{$zip}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.City')*</label>
							<input type="text" id="city" class="form-control" name="city" value="{{{$city}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.Country')*</label>
							<input type="text" id="country" class="form-control" name="country" value="{{{$country}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.TelephoneNumber')</label>
							<input type="text" id="telephone_number" class="form-control" data-telephone-id="{{{$telephone_id}}}" name="telephoneNumber" value="{{{$telephone_number}}}" />
						</div> 
						<div class="form-group">
							<label>@lang('looll.MobileNumber')</label>
							<input type="text" id="mobile_phone_number" data-mobile-id="{{{$mobile_id}}}" class="form-control" name="mobileNumber" value="{{{$mobile_phone_number}}}" />
						</div>		
						<h3>@lang('YourWork')</h3>
						<div class="form-group">
							<label>@lang('looll.Company')</label>
							<input type="text" class="form-control" id="company" name="company" value="{{{$company}}}" />
						</div>
						<div class="form-group">
							<label>@lang('looll.WorkPhoneNumber')</label>
							<input type="text" id="working_phone_number" name="businessPhone" data-buisness-phone-id="{{{$buisness_phone_id}}}" class="form-control" value="{{{$working_phone}}}" >
						</div>
						<div class="form-group">
							<label>@lang('looll.Position')</label>
							<input type="text" id="position" name="position" class="form-control" value="{{{$position}}}" />
						</div>
						<div class="form-group">
							<input type="submit" id="save" data-profile-id="{{{$profile_id}}}" class="btn btn-primary" name="save" value="@lang('looll.Save')" />
							<input type="submit" data-profile-id="{{{$profile_id}}}" class="btn btn-success hidden" value="Your profile was saved" id="success" >
						
								<a href="/organisation/edit/0" id="createorganisation" class="btn btn-primary 	@if(count($profile) == 0) 
								hidden 
								@endif">
								@lang('looll.CreateOrganisation')</a>
						</div>
						</form>
					</div>
				</div>
				@if(count($organisations) > 0)
				<div class="row">
					<div class="col-sm-5 col-sm-offset-3">
						<h3>@lang('looll.MyMemberships')</h3>
						<table class="table">
							<thead>
								<tr>
									<th>@lang('looll.Name')</th>
									<th>@lang('Action')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($organisations as $organisation)
									<tr id="{{$organisation->organisation_id}}">
										<td>{{$organisation->name}}</td>
										<td>
											<a href="" data-organisation-id="{{$organisation->organisation_id}}" data-user-id="{{$user_id}}" class="disconnect btn btn-warning">Cancel my membership</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				@endif
			</div>
			<div class="footer">
		      <div class="container">
		        <p class="text-muted"><a href="/account/delete/{{$user_id}}" class="btn deleteAccount">@lang('looll.DeleteMyAccount')</a></p>
		      </div>
		    </div>
			@include('footer')
			<script type="text/javascript" src="/js/profile.js?v=4"></script>
			<script src="/js/modernizr.custom.js"></script>
			<script src="/js/script.js"></script>
		</body>
	</html>