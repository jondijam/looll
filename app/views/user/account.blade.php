<!DOCTYPE html>
	<html lang="en">
		<head>
	    	@include('head')
		</head>
		<body>
			<div class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand">looll.is</a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav navbar-right">
							@if(count($count_user_organisations) > 0)
							<li>
								<a href="/organisation">@lang('looll.Organisation')</a>
							</li>
							@endif
							<li>
								<a href="/profile">@lang('looll.MyProfile')</a>
							</li>
							<li class="active">
								<a href="/account">@lang('looll.Account')</a>
							</li>
							<li>
								<a href="/logout">@lang('looll.Logout')</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<form action="" method="post">
							<div class="form-group">
								<label>@lang('looll.Username')</label>
								<input type="text" class="form-control" disabled="disabled" value="{{$username}}" name="username" value="" />
							</div>
							<div class="form-group">
								<label>@lang('looll.Email')</label>
								<input type="email" class="form-control" name="email" value="{{$email}}" />
							</div>
							<div class="form-group">
								<label>@lang('looll.Password')</label>
								<input type="password" name="password" class="form-control" />
							</div>
							<div class="form-group">
								<label>@lang('looll.ConfirmPassword')</label>
								<input type="password" name="confirmPassword" class="form-control" />
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" id="save" value="@lang('looll.Save')" />
							</div>
							<a href="/account/delete">@lang('looll.DeleteAccount')</a>
						</form>
					</div>
				</div>
			</div>
		</body>
	</html>