@extends('layouts.master')
@section('content')
	<div class="row">
		<div class="col-sm-5 col-sm-offset-3 search-box">
			@if (Session::has('flash_message'))
	            <div class="alert alert-danger">
	                <p>{{ Session::get('flash_message') }}</p>
	            </div>
	        @elseif($errors->any())
	        	<div class="alert alert-danger">
	        		@foreach ($errors->all() as $error) 
	        			<p>{{$error}}</p>
	        		@endforeach
	        	</div>
	        @endif
			{{ Form::open(array('route' => 'sessions.store')) }}
			 	<div class="form-group">
	          		{{ Form::label('username', Lang::get('looll.Username')) }}
	          		{{ Form::text('username', $value = null, array('class' => 'form-control', 'id' => 'username'))}}
	        	</div>
	        	<div class="form-group">
         			{{ Form::label('password', Lang::get('looll.Password')) }}
          			{{ Form::password('password', array('class' => 'form-control', 'id' => 'password'))}}
        		</div>

        		<div class="form-group">
        			{{Form::submit(Lang::get('looll.Login'), array('class' => 'btn btn-primary')) }}
        			<a href="/password/remind" class="btn btn-primary" style="margin-left:10px">@lang('looll.ForgetYourPassword')</a>
        		</div>
			{{ Form::close() }}
			
	        
		</div>
	</div>
@stop