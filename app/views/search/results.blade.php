@extends('layouts/master')
@section('content')
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 search-box">
      <h1>looll <small class="white">Search</small></h1>
      {{ Form::open(['route' => 'search.index', 'method' => 'GET']) }}
        <div class="form-group">
          <div class="input-group">
            {{ Form::input('search', 'q', null, ['placeholder' => Lang::get('looll.SearchWords'), 'class' => 'form-control'] ) }}
            <span class="input-group-btn">
              {{ Form::submit(Lang::get('looll.Search'), ['class' => 'btn btn-default']); }}
            </span>
          </div>
        </div>
      {{ Form::close() }}
      @if(count($results) == 0 )
       <h2>Sorry, but we did not find it.</h2>
       <p>We did our best to search, but did not find anything relate to {{ Request::get('q')}}. </p>
      @endif
      
      @for($i = 0; $i < count($results); $i++)
        @if(isset($results[$i]['user']) || isset($results[$i]['profile']) ||  isset($results[$i]['parent_id']))
        <div class="panel panel-default">
          <div class="panel-body">
            @if(isset($results[$i]['user']))

              {{
                 link_to_route('profile', $results[$i]['profile']['name'] , $parameters = array($results[$i]['username']), $attributes = array());
                }}

            @elseif(isset($results[$i]['profile']))
              {{
                 link_to_route('profile', $results[$i]['profile']['name'] , $parameters = array($results[$i]['username']), $attributes = array());
                }}
            @elseif(isset($results[$i]['parent_id']))
              
              {{
                  link_to_route('organisation.show', $results[$i]['name'] , $parameters = array($results[$i]['id']), $attributes = array());
              }}

              
            @endif 
          </div>
        </div>
        @endif
      @endfor

    </div>
</div>
@stop