<div class="form-group">
	<ul class="list-group">
	@if(isset($results))
		@foreach($results as $user)		
			@if( (in_array($user->id, $temp)) && $user->pivot->role_id == 2)
				<li class="list-group-item">
					<label class="checkbox addContact" data-user-id="{{$user->id}}" data-organisation-id="{{$organisationId}}" data-contacts-count="{{ $contactsCount }}">
					{{ Form::checkbox('addContact', $user->id, true, ['data-organisation-id' => $organisationId]) }}
					{{{$user->profile->name}}}
					</label>
				</li>
			@elseif((!in_array($user->id, $temp)) && $user->pivot->role_id == 3)
				<li class="list-group-item">
					<label class="checkbox addContact" data-user-id="{{$user->id}}" data-organisation-id="{{$organisationId}}" data-contacts-count="{{ $contactsCount }}">
						@if($user->pivot->role_id !== 2)
							{{ Form::checkbox('addContact', $user->id, false, ['data-organisation-id' => $organisationId]) }}
						@endif
						{{{$user->profile->name}}}
						
					</label>
				</li>
			@endif
		@endforeach
	@else
		<li class="list-group-item">
			Nothing found
		</li>
	@endif
	</ul>
</div>