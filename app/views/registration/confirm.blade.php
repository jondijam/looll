@extends('layouts.master')
@section('content')
<div class="row">
	<div class="col-sm-5 col-sm-offset-3 box">
  	 	<h2>@lang("looll.ConfirmYourEmail")</h2>
  	 	<p>@lang("looll.BeforeYourLoginPleaseConfirmYourEmail")</p>
  	</div>
</div>
@stop