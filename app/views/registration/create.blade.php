@extends('layouts.master')
@section('content')
   <div class="row">
    <div class="col-sm-5 col-sm-offset-3 search-box">
      {{ Form::open(array('route' => 'registration.store')) }}
        <div class="form-group">
            {{ Form::label('username', Lang::get('looll.Name')) }}
            {{ Form::text('name', $value = null, array('class' => 'form-control', 'id' => 'name'))}}
            {{ $errors->first('name', '<span class="label label-danger">:message</span>') }}
        </div>   
        <div class="form-group @if($errors->has('username'))has-error @endif">
          {{ Form::label('username', Lang::get('looll.Username')) }}
          {{ Form::text('username', $value = null, array('class' => 'form-control', 'id' => 'username'))}}
          {{ $errors->first('username', '<span class="label label-danger">:message</span>') }}
        </div>
        <div class="form-group @if($errors->has('email'))has-error @endif">
          {{ Form::label('email', Lang::get('looll.Email')) }}
          {{ Form::email('email', $value = null, array('class' => 'form-control', 'id' => 'email'))}}
          {{ $errors->first('email', '<span class="label label-danger">:message</span>') }}
        </div>
        <div class="form-group @if($errors->has('password'))has-error @endif">
          {{ Form::label('password', Lang::get('looll.Password')) }}
          {{ Form::password('password', array('class' => 'form-control', 'id' => 'password'))}}
          {{ $errors->first('password', '<span class="label label-danger">:message</span>') }}
        </div>
        <div class="form-group @if($errors->has('password_confirmation'))has-error @endif">
          {{ Form::label('password_confirmation', Lang::get('looll.ConfirmPassword')) }}
          {{ Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'passwordConfirmation'))}}
          {{ $errors->first('password_confirmation', '<span class="label label-danger">:message</span>') }}
        </div>
        <div class="form-group">
          {{Form::submit(Lang::get('looll.Register'), array('class' => 'btn btn-primary')) }}
        </div>
      {{ Form::close() }}
    </div>
   </div>
@stop