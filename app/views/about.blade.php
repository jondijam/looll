@extends('layouts.master')
@section('content')
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3 search-box">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>@lang('looll.About')</h1>
					<p>@lang('looll.AboutText')</p>
					<address>
				  	<strong>Leikfang ehf.</strong><br>
				  		Loftur Már Sigurðsson<br>
				  		<a href="mailto:loftur@looll.is">loftur@looll.is</a>
					</address>
				</div>
			</div>
		</div>
	</div>
@stop