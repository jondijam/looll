@extends('layouts.master')
@section('content')
		<div class="col-sm-3 search-box">
			<div id="result">
			</div>
		</div>
		{{ Form::open(array('route' => 'organisation.store')) }}
			@include("includes.organisation.form")
			
			<div class="col-sm-4 search-box">
			</div>
		{{ Form::close() }}
@stop

@section('javascript')
	<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-te-1.4.0.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootswatch.js"></script>
    <script type="text/javascript" src="/js/organisation.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
    <script type="text/javascript">
		$('textarea').jqte();
	</script>
@stop