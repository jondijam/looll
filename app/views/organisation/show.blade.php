@extends('layouts.master')
@section('content')
	<div class="row">
		<div class="col-lg-12">
          <div class="page-header">
            <h1>{{{$organisation->name}}}</h1> 
          </div>
        </div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			@if(!empty($images))
				<div class="row">
					<div class="col-sm-12">
						@foreach($images as $image)
			                @if($image->image_primary == 1)
			                	<img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="img-thumbnail" />
			                @endif
			            @endforeach
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="crop">
			                @foreach($images as $image)
			                  @if($image->image_primary == 0)
			                      <img src="{{{$image->src}}}" alt="{{{$image->name}}}" class="img-thumbnail" />
			                  @endif
			                @endforeach  
			            </div>
					</div>
				</div>
			@endif
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">@lang('looll.Information')</h4>
						</div>
						<div class="panel-body">
							<address>
				                {{{$organisation->address}}}<br>
				                {{{$organisation->zip}}} {{{$organisation->city}}}<br>
				                @if($organisation->country()->count() > 0)
				                	{{{$organisation->country()->first()->name}}}<br />
				                @endif
				                {{{$organisation->email}}}
				            </address>
				              <h5>@lang('looll.Contacts')</h5>
				            <address>
				                @foreach($contacts as $contact)
				                  <a href="/{{$contact->username}}">{{$contact->profile->name}}</a><br />           
				                @endforeach
				            </address>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">@lang('looll.About', array('name'=>$organisation->name))</h4>
						</div>
						<div class="panel-body">
							{{$organisation->description}}
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">@lang('looll.Announcement')</h4>
						</div>
						<div class="panel-body">
							{{$organisation->announcement}}	
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					@if(Auth::check() && $organisation->member_type_id == 2 && $organisation_count == 0)
	                  		<a href="/" class="btn btn-primary pull-right" id="connect" data-user-id="{{$user_id}}" data-organisation-id="{{$id}}" data-token="{{ Form::token() }}">@lang('looll.Connect')</a>              

	              	@endif
				</div>	
			</div>
		</div>
		<div class="col-sm-3">
			<div class="panel-group" id="accordion">
				@if($organisation->member_type_id == 2 && count($members) > 0)
					<div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                              @lang('looll.Members')
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                           <div class="panel-body">
                              <ul class="media-list">
                                @if(count($members))
                                @foreach($members as $member)
                                  @if(isset($member->profile))
                                  	<li><a href="/{{$member->username}}">{{$member->profile->name}}</a></li>
                                  @endif
                                @endforeach
                                @endif
                              </ul>
                           </div>
                        </div>
                      </div>
				@endif
				@if($organisation->member_type_id == 3 && count($associated_organizations) > 0) 
					<div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                             @lang('looll.AssociatedOrganizations')
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in">
                         <div class="panel-body">
                              <ul class="media-list">
                              	@if(count($associated_organizations))
                                @foreach($associated_organizations as $associated_organisation)
                                  <li><a href="/organisation/{{$associated_organisation->id}}">{{$associated_organisation->name}}</a></li>
                                @endforeach
                                @endif
                              </ul>
                           </div>
                        </div>
                    </div>
				@endif
			</div>
		</div>
	</div>
@stop
@section('javascript')
	<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-te-1.4.0.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootswatch.js"></script>
    <script type="text/javascript" src="/js/organisation.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
	
@stop

