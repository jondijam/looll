@extends('layouts.master')
@section('content')
	<div class="row">
		<div class="col-sm-3 box">
				@if(count($organisation))
				
					<div class="panel panel-default organisation-image form-images @if($organisation->photos()->count() == 5) hidden @endif">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#organisation-image">@lang('looll.Images')</a>
								</h4>
							</div>
							<div id="organisation-image" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="form-group form-images">
										<div id="droparea" data-organisation-id="{{$id}}" data-profile-id="0">
											<div class="dropareainner">
												<p class="dropfiletext">@lang('looll.DropFilesHere')</p>
												<p>@lang('looll.Or')</p>
												<p>
														<input id="uploadbtn" type="button" class="btn btn-primary" value="@lang('looll.SelectFiles')"/>
												</p>
															<!-- extra feature -->
												<p id="err"><!-- error message --></p>
											</div>
											<input id="upload" type="file" multiple="" />
										</div>
									</div>
								</div>
							</div>
					</div>
				@endif
				<div id="result">
					@if(count($organisation) && $organisation->photos()->count())
						@foreach($organisation->photos()->get() as $photo)
							<div class="imageholder" id="imageholder{{{$photo->id}}}">
								<figure>
										{{ HTML::image($photo->src, $alt=$photo->name, $attributes = array("class"=>"image", "data-image-id" => $photo->id)) }}
										<span class="fa fa-times-circle remove-images" data-organisation-id="{{$id}}" data-profile-id="0" data-image-id="{{{$photo->id}}}"></span>
								
										<span class="fa @if($photo->image_primary) fa-star @else fa-star-o @endif primary-image" data-organisation-id="{{$id}}" data-profile-id="0" data-image-id="{{{$photo->id}}}" data-primary="{{$photo->image_primary}}"></span>
								</figure>
							</div>
						@endforeach
					@endif
				</div>
		</div>
		{{ Form::model($organisation, array('route' => $url, 'data-remote', 'novalidate', 'method' => 'PUT')) }}
			@include("includes.organisation.form")
			<div class="col-sm-4 box">
				<div class="panel-group" id="accordion">
						@if($member_type_id == 3 && count($children) > 0)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#organisationsMembers">
											@lang('looll.AssocitedMembers')
										</a>
									</h4>
								</div>
								<div id="organisationsMembers" class="panel-collapse collapse">
									<div class="panel-body">
										<table class="table">
											<thead>
												<tr>
														<th>@lang('looll.Organisation')</th>
														<th>@lang('looll.Action')</th>
												</tr>
											</thead>
											<tbody>
													@if(count($children))
														@foreach($children as $child)
															<tr>
																<td>{{link_to_route('organisation.show', $title = $child->name, $parameters = array('id'=>$child->id), $attributes = array()) }}</td>
																<td>
																		<button data-url="{{ route('organisations.children.update', array('id' => $id, 'organisation_id'=>$child->id)) }}" id="accept{{$child->id}}" class="acceptGroup btn @if($child->parent_acceptance == 1) hidden @endif btn-primary">@lang('looll.Accept')</button>
																		<button data-url="{{ route('organisations.children.update', array('id' => $id, 'children'=>$child->id)) }}" id="decline{{$child->id}}" class="declineGroup btn @if($child->parent_acceptance == 0) hidden @endif btn-warning">@lang('looll.Decline')</button>
																</td>
															</tr>
														@endforeach
													@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
										<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#usersMembers">
											@lang('looll.UserMembers')
										</a>
										</h4>
								</div>
								<div id="usersMembers" class="panel-collapse collapse">
										<div class="panel-body">
											<table class="table">
												<thead>
													<tr>
														<th>@lang('looll.Name')</th>
													</tr>
												</thead>
												<tbody>
													@foreach($children as $child)
														@if($child->member_type_id == 2)
															@foreach($child->users()->get() as $user)
																<tr>
																	<td>{{ $user->username }}</td>
																</tr>
																@if(isset($user->profile->name))
																	<tr>
																		<td>{{ $user->profile->name }}</td>
																	</tr>
																@endif
															@endforeach
														@endif
													@endforeach
												</tbody>
											</table>
										</div>	
								</div>
							</div>
						@elseif($member_type_id == 2 && count($users) > 0)
								<div class="panel panel-default">
										<div class="panel-heading">
												<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#usersMembers">@lang('looll.UserMembers')</a></h4>
										</div>
										<div id="usersMembers" class="panel-collapse collapse">
												<div class="panel-body">
														<table class="table">
																<thead>
																		<tr>
																				<th>@lang('looll.Name')</th>
																				<th>@lang('looll.Action')</th>
																		</tr>	
																</thead>
																<tbody>
																		<tr>
																				@foreach($users as $user)
																						@if(isset($user->profile))
																								<tr id="deleteUser{{$user->id}}">
																										<td>
																											{{
																													link_to_route('profile', $user->profile->name, array('username' => $user->username), array("target" => "_blank"));
																											}}
																										</td>
																										<td>
																												<button data-user-id="{{$user->id}}" data-organisation-id="{{$id}}" id="acceptUser{{$user->id}}" class="acceptUser btn @if($user->pivot->accepted == 1 ) hidden @endif btn-primary btn-sm">@lang('looll.Accept')</button>
																												<button data-user-id="{{$user->id}}" data-organisation-id="{{$id}}" id="declineUser{{$user->id}}" class="declineUser btn @if($user->pivot->accepted == 0) hidden @endif btn-warning btn-sm">@lang('looll.Decline')</button>
																												<button class="removeUser btn btn-warning btn-sm" data-user-id="{{$user->id}}" data-organisation-id="{{$id}}">@lang('looll.Remove')</button></td>
																								</tr>
																						@endif
																				@endforeach
																		</tr>
																</tbody>
														</table>
												</div>
										</div>
								</div>
						@endif
				</div>
			</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
		<script src="/js/organisations/dist/main.bundle.min.js?v=1.4"></script>
		<script type="text/javascript">
				$('textarea').jqte();
		</script>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
@stop