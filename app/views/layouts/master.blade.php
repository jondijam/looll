<!DOCTYPE html>
<html>
<head>
	@include('head')
</head>
<body class="background">
	@include('includes/navigation')
	<div class="container">
		@yield('content')
	</div>
	@include('footer')
	@yield('javascript')
</body>
</html>