<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<title>@lang('otkell.OtkellSportsSolutions')</title>

		<!-- start: CSS FILES -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/font-awesome.min.css" rel="stylesheet">
		<link href="/css/otkell-site.css" rel="stylesheet">
		<!-- end: CSS FILES -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="otkell-index">

		<!-- start: MODAL -->
		<div class="modal-background" tabindex="-1"> <!-- MODAL BACKGROUND -->
			<div class="modal-box"> <!-- MODAL BOX CONTAINER -->
				<div class="modal-content">
					<div class="modal-header clearfix">
						<a href="" class="close close-modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</a>
						<h4 class="modal-title" id="myModalLabel">@lang('otkell.Informations')</h4>
					</div><!-- .modal-header -->
					<div class="modal-body clearfix">
						<div class="modal-body-add-info">
							<div class="match-info-date clearfix">
								<label for="mobile-date-info" class="firstLabel">@lang('otkell.Date')</label>
								<input type="date" id="mobile-date-info" placeholder="00/00/00" />
							</div><!-- .match-info-date -->
							<div class="match-info-time clearfix">
								<label for="mobile-time-info">@lang('otkell.Timing')</label>
								<input type="text" id="mobile-time-info" class="matches-timeInput" placeholder="00:00" />
							</div><!-- .match-info-date -->
							<div class="match-info-result clearfix">
								<label for="">@lang('otkell.Result')</label>
								<input type="text" class="matches-resultsInputs matches-resultsInput-homeTeam" placeholder="0" />
								<input type="text" class="matches-resultsInputs matches-resultsInput-awayTeam" placeholder="0" />
							</div><!-- .match-info-add-right -->
						</div><!-- .modal-body-add-info -->
					</div><!-- .modal-body -->
					<div class="modal-footer clearfix">
						<a href="" class="btn btn-default close-modal">Close</a>
						<a href="" class="btn btn-primary">Save changes</a>
					</div><!-- .modal-footer -->
				</div><!-- .modal-content -->
			</div><!-- .modal-box -->
		</div><!-- .modal-background -->
		<!-- end: MODAL -->
	
		<!-- start: HEADER -->
		<div id="smallHeader">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<a href="" id="lang" class="pull-left">
							<i class="fa fa-globe"></i> IS <i class="fa fa-caret-down"></i>
						</a>
						<!-- <a href="login.html" class="loginBtn pull-right" title="Innskrá"><i class="fa fa-lock"></i> Innskráning</a> -->
						<!-- <a href="" class="pull-right">Hafa samband</a> -->
						<a href="" class="pull-right" title="Tæknileg þjónusta">@lang('otkell.TechnicalService')</a>
						<a href="" class="pull-right" title="Hafa samband">@lang('otkell.Contact')</a>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .smallHeader -->
		<div id="header">
			<div class="container clearfix">
				<div class="row">
					<div class="col-sm-12">
						<!-- <h1 class="logoH1" title="OTKELL Deildarkerfi"> -->
							<a href="index.html" id="logo" title="Otkell deildarkerfi">
								<img src="img/otkell-logo-mobile.png" alt="Otkell deildarkerfi" />
							</a>
						<!-- </h1> -->
						<ul class="header-navList pull-left">
							<li>
								<a href="" class="navItem">
									<span data-hover="Lausnir">@lang('otkell.Solutions')</span>
								</a>
							</li>
							<li>
								<a href="" class="navItem">
									<span data-hover="Afhverju OTKELL?">@lang('otkell.WhyOtkell')</span>
								</a>
							</li>
							<!-- <li>
								<a href="" class="navItem">
									<span data-hover="Taktu túrinn">Taktu túrinn</span>
								</a>
							</li> -->
							<li>
								<a href="" class="navItem">
									<span data-hover="Hafa samband">@lang('otkell.Contact')</span>
								</a>
							</li>
						</ul>
						<a href="/login" id="login-trigger" class="btn btn-primary btn-lg pull-right" title="Panta kerfi">
							<i class="fa fa-lock"></i> @lang('otkell.Login')
						</a>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- #header -->
		<!-- end: HEADER -->

		<div id="cover">
			<h1 class="coverText" title="@lang('otkell.CoverTextTitle')">
				@lang('otkell.CoverText')
			</h1>
			<a href="" class="btn btn-default btn-lg">@lang('otkell.ViewSubscriptionOptions')</a>
		</div><!-- #cover -->

		<div class="sellBox clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 clearfix">
						<div class="sellBox-inner">
							<h3 class="pull-left" title="Tilbúinn að panta kerfi?">Tilbúin/n að panta?</h3>
							<a href="" class="btn btn-primary btn-lg pull-right" title="Panta kerfi">Panta kerfi</a>
						</div><!-- .sellBox-inner -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .sellBox -->
		
		<!-- start: SECTION 1 -->
		<div id="section-01" class="section clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="sectionTitle">Afhverju velja kerfi frá OTKELL?</h2>
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				<!-- start: POINT 1 -->
				<div class="pointWrapper">
					<div class="row">
						<div class="col-sm-6">
							<div class="point point-01">
								<h3 class="sectionHeading">Einfaldleiki</h3>
								<h4 class="sectionSubHeading">Einfaldur rekstur deildar</h4>
								<p class="firstParagraph">Deildarkerfi frá OTKELL einfaldar rekstur deildarinnar svo um munar. Segðu bless við excel skjalið og upplifðu það sem nútímatæknin hefur upp á að bjóða.</p>

								<h4 class="sectionSubHeading">Hlaðið aukahlutum</h4>
								<p>Kerfið er hlaðið aukahlutum sem henta allskyns deildum og Íþróttum af öllu tagi.</p>
							</div><!-- .point -->
						</div><!-- .col-sm-6 -->
						<div class="col-sm-6">
							<img src="img/display.png" alt="OTKELL Íþróttakerfi" />
						</div><!-- .col-sm-6 -->
					</div><!-- .row -->
				</div><!-- .pointWrapper -->
				<!-- end: POINT 1 -->
				<!-- start: POINT 2 -->
				<div class="pointWrapper">
					<div class="row">
						<div class="col-sm-6">
							<img src="img/display.png" alt="OTKELL Íþróttakerfi" />
						</div><!-- .col-sm-6 -->
						<div class="col-sm-6">
							<div class="point point-02">
								<h3 class="sectionHeading">Fljótlegt</h3>
								<p class="firstParagraph">Deildarkerfi frá OTKELL einfaldar ekki bara rekstur deildarinnar heldur flýtir fyrir skráningum og útreikningum  á t.d. liðum í deild, stöðu í riðlum og úrslit leikja svo eitthvað sé nefnt.</p>

								<h4 class="sectionSubHeading">Sjálfvirkt og virkar í öll snjalltæki</h4>
								<p>Kerfið er að mestum hluta sjálfvirkt og sér um flesta þætti deildarinnar fyrir þig. Einnig virkar kerfið i flest öllum tækjum alveg frá sjónvarpsskjá niður í snjallsíma.</p>
							</div><!-- .point -->
						</div><!-- .col-sm-6 -->
					</div><!-- .row -->
				</div><!-- .pointWrapper -->
				<!-- end: POINT 2 -->
				<!-- start: POINT 3 -->
				<div class="pointWrapper lastPointWrapper">
					<div class="row">
						<div class="col-sm-6">
							<div class="point point-03">
								<h3 class="sectionHeading">Tæknileg þjónusta</h3>
								<p class="firstParagraph">Bacon ipsum dolor sit amet flank meatloaf tenderloin, ham hock swine leberkas sausage ball tip salami jowl beef fatback kielbasa ham. Chicken ribeye leberkas ham.</p>

								<p>Beef meatball short loin spare ribs rump pork loin jerky andouille beef ribs pork biltong. Pig ribeye short loin pork belly salami pastrami turducken pork loin shank bacon prosciutto. Short ribs salami landjaeger pork belly. Pork loin pork belly biltong filet mignon swine jowl.</p>
							</div><!-- .point -->
						</div><!-- .col-sm-6 -->
						<div class="col-sm-6">
							<img src="img/display.png" alt="OTKELL Íþróttakerfi" />
						</div><!-- .col-sm-6 -->
					</div><!-- .row -->
				</div><!-- .pointWrapper -->
				<!-- end: POINT 3 -->
			</div><!-- .container -->
		</div><!-- .section -->
		<!-- end: SECTION 1 -->

		<div class="sellBox clearfix">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 clearfix">
						<div class="sellBox-inner">
							<h3 class="pull-left" title="Tilbúinn að panta kerfi?">Tilbúin/n að panta?</h3>
							<a href="" class="btn btn-primary btn-lg pull-right" title="Panta kerfi">Panta kerfi</a>
						</div><!-- .sellBox-inner -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .sellBox -->


		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mediaQuery">MediaQuery</div>
				</div>
			</div>
		</div>

		<!-- start: JAVASCRIPT FILES -->
		<script src="/js/jquery-1.11.1.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery-scrollto.js"></script>
		<script src="/js/otkell-site.js"></script>
		<!-- end: JAVASCRIPT FILES -->

	</body>
</html>