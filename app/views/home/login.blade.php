<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<title>OTKELL deildarkerfi</title>

		<!-- start: CSS FILES -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href="/css/font-awesome.min.css" rel="stylesheet">
		<link href="/css/jquery-te.css" rel="stylesheet">
		<link href="/css/otkell-admin.css" rel="stylesheet">
		<!-- end: CSS FILES -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="otkell-login">
		
		<!-- start: LOGIN -->
		<div class="loginWrapper">
			<!-- start: LOGIN TOP -->
			<div class="loginTop">
				<a href="/login" title="Otkell Deildarkerfi"><img src="img/otkell-logo.png" alt="Otkell Deildarkerfi"></a>
			</div><!-- .loginTop -->
			<!-- end: LOGIN TOP -->

			<!-- start: LOGIN STEP -->
			
			<div class="loginBox loginStep clearfix">
				<form action="/login" id="loginForm" method="post">
					<h2 class="loginHeading">Innskráning</h2>
					<div class="loginForms">
						<input type="text" name="username" id="username" placeholder="Notandanafn" />
						<div class="passwordWrap">
							<input type="password" name="password" id="password" placeholder="Lykilorð" />
							<a href="/password/remind" class="forgotPassLink">Gleymt lykilorð?</a>
						</div><!-- .passwordWrap -->
									
						<div id="loginError" class="@if (!$errors->has())error @endif alert alert-danger">
							@if ($errors->has())
								{{$errors->first('password')}}
							@endif
						</div><!-- #loginError -->
						
						<div class="btnRow btnRowRelative clearfix">
							<div class="customCheckmark emptyCheckbox-white pull-left"></div><!-- .customCheckmark -->
							<input type="checkbox" id="keepLoggedin" name="rememberMe" value="1" /> 
							<label for="keepLoggedin" class="keepLoggedInLabel pull-left">@lang('otkell.RememberMe')</label>
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="submit" id="loginStepBtn" class="btn btn-login btn-large pull-right" value="@lang('otkell.Login')" >

							{{--

								<a href="admin/" id="loginStepBtn" class="btn btn-login btn-large pull-right">Innskrá</a>
							--}}
						</div><!-- .buttonRow -->
					</div><!-- .loginForms -->
				</form><!-- #loginForm -->
			</div><!-- .loginBox -->
			
			<!-- end: LOGIN STEP -->
		</div><!-- .loginWrapper -->
		<!-- end: LOGIN -->

		<div class="container clearfix">
			<div class="mediaQuery">MediaQuery</div>
		</div><!-- .container -->

	<!-- start: JAVASCRIPT FILES -->
	<script src="/js/jquery-1.11.1.min.js"></script>
	<script src="/js/jquery.nicescroll.min.js"></script>
	<script src="/js/jquery-te.min.js"></script>
	<script src="/js/otkell-admin.js"></script>
	<!-- end: JAVASCRIPT FILES -->

	</body>
</html>