<?php namespace Looll\Files;

use Illuminate\Support\ServiceProvider;
use Looll\Files\Photos;
/**
* 
*/
class FilesServiceProvider extends ServiceProvider
{
	public function register()
	{
		$app = $this->app;

		$app->bind('Looll\Files\FileInterface', 'Looll\Files\Photos');
	}
}


?>