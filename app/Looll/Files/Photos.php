<?php namespace Looll\Files;

use URL;
/**
* 
*/
use Looll\Files\FileInterface;

class Photos implements FileInterface
{
	public function upload($file)
	{
		
		$upload_folder = "/img/profile/";

		$extension = $file->getClientOriginalExtension(); 
		$filename = $file->getClientOriginalName();

		$destination_path = public_path().$upload_folder;
			
		$realpath = $file->getRealPath();

		$new_filename = str_random(60).'.'.$extension;

		$file = $file->move($destination_path, $new_filename);
		$path = $file->getRealPath();
		$src = URL::asset($upload_folder.''.$new_filename);
		
		return array('path'=>$path, 'new_filename'=>$new_filename, 'src'=>$src);	
	}
}

?>