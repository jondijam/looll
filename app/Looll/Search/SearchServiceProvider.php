<?php  namespace Looll\Search;

/**
* 
*/
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->bind('search', 'Looll\Search\Search');
	}
}

?>