<?php  namespace Looll\Search;


use Illuminate\Support\Collection;
use Profile;
use Organisation;
use User;
/**
* 
*/
class Search 
{
	public function profiles($search)
	{
		return Profile::search($search)->with('User')->get();
	}

	public function organisations($search)
	{
		return Organisation::search($search)->get();
	}

	public function user($search)
	{
		return User::search($search)->with('Profile')->get();
	}

	public function members($search, $organisation)
	{
		return $organisation->users()->with('profile', 'roles')->searchMembers($search)->where('role_id', '!=', 1)->where('accepted', '=', 1)->get();
	}

	public function looll($search)
	{
		$users = array();
		$organisations = array();
		$profiles = array();

		if (strlen($search) > 1) 
		{
			$users = $this->user($search);
			$organisations = $this->organisations($search);

			$users = $users->toArray();
			$organisations = $organisations->toArray();
		}

		$collection = new Collection($users);
		$collection = $collection->merge($organisations);
		$collection = $collection->merge($profiles);

		return $collection;

	}
}

?>