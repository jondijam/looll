<?php  namespace Looll\Repo;

use Illuminate\Support\ServiceProvider;

use Looll\Repo\User\EloquentUser;
use Looll\Repo\Profile\EloquentProfile;
use Looll\Repo\Country\EloquentCountry;
use Looll\Repo\PhoneNumber\EloquentPhoneNumber;
use Looll\Service\Cache\LaravelCache;
use Looll\Repo\Profile\CacheDecorator;
use Looll\Repo\Country\CacheDecorator as CacheCountry;
use Looll\Repo\Organisation\CacheDecorator as CacheOrganisation;
use Looll\Repo\Organisation\EloquentOrganisation;
use Looll\Repo\Photo\EloquentPhoto;

use User;
use Photo;
use Organisation;
use Profile;
use Country;
use Auth;
use PhoneNumber;


class RepoServiceProvider extends ServiceProvider
{
	public function register()
	{
		$app = $this->app;
		
		$app->bind('Looll\Repo\User\UserInterface', function($app)
		{
			return new EloquentUser( new User);
		});

		$app->bind('Looll\Repo\PhoneNumber\PhoneNumberInterface', function($app)
		{
			return new EloquentPhoneNumber( new PhoneNumber );
		});

		
		$app->bind('Looll\Repo\Country\CountryInterface', function($app)
		{
			$country = new EloquentCountry( new Country );
			$country = new \Looll\Repo\Country\CacheDecorator($country, new LaravelCache($app['cache'], 'countries', 10));

			return $country;

		});

		$app->bind('Looll\Repo\Profile\ProfileInterface', function($app)
		{
			$profile = new EloquentProfile( new Profile, $app->make('Looll\Repo\User\UserInterface'), new Country, $app->make('Looll\Repo\PhoneNumber\PhoneNumberInterface'));
			$profile = new CacheDecorator($profile, new LaravelCache($app['cache'], 'profiles', 10));
			
			return $profile;
			
		});


		$app->bind('Looll\Repo\Organisation\OrganisationInterface', function($app)
		{
			$organisation = new EloquentOrganisation( new Organisation, $app->make('Looll\Repo\Country\CountryInterface'), $app->make('Looll\Repo\User\UserInterface'));
			$organisation = new CacheOrganisation($organisation, new LaravelCache($app['cache'], 'organisations', 10));
			
			return $organisation;
		});

		$app->bind('Looll\Repo\Photo\PhotoInterface', function($app)
		{
			return new EloquentPhoto(new Photo, $app->make('Looll\Repo\Organisation\OrganisationInterface'), $app->make('Looll\Repo\Profile\ProfileInterface'));
		});
	}

}


?>