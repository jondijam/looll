<?php  namespace Looll\Repo\Photo;

use Looll\Repo\Organisation\OrganisationInterface;
use Looll\Repo\Profile\ProfileInterface;
use Looll\Repo\Profile\CacheDecorator as CacheProfile;
use Looll\Repo\Organisation\CacheDecorator as CacheOrganisation;
use Illuminate\Database\Eloquent\Model;
use Looll\Repo\DbRepository;
/**
* @var 
*/
class EloquentPhoto extends DbRepository implements PhotoInterface
{
	protected $model;
	protected $organisation;
	protected $profile;

	function __construct(Model $model, CacheOrganisation $organisation, CacheProfile $profile)
	{
		$this->model = $model;
		$this->organisation = $organisation;
		$this->profile = $profile;
	}

	public function create($data, $profile_id, $organisation_id)
	{
		$photo = $this->model->create($data);
		$photo_id = $photo->id;

		if($profile_id > 0) 
		{
			$this->profile->savePhoto($photo, $profile_id);
		}

		
		if ($organisation_id > 0) 
		{
			$this->organisation->savePhoto($photo, $organisation_id);
		}
		
		return $photo_id;
	}

	public function update($data, $photo_id)
	{
		$profile_id = $data['profile_id'];
		$organisation_id = $data['organisation_id'];
		
		if ($profile_id > 0) 
		{
			$photos = $this->profile->photos($profile_id);
		}

		if ($organisation_id > 0) 
		{
			$photos = $this->organisation->getPhotos($organisation_id);
		}
		
		foreach ($photos as $photo) 
		{
			$photo = $this->model->find($photo['id']);
			$photo->image_primary = 0;
			$photo->save();
		}

		$image = $this->model->find($photo_id);
		$image->image_primary = 1;
		$image->save();
	}

	public function destroy($photoId)
	{
		$photo = $this->model->find($photoId);
		$photo->delete();		
	}
}


?>