<?php namespace Looll\Repo\Photo;

interface PhotoInterface
{
	public function getById($id);
	public function getAll();
	public function create($data, $profile_id, $organisation_id);
	public function update($data, $photo_id);
	public function destroy($photo_id);
}

?>
