<?php namespace Looll\Repo\Profile;

use Looll\Service\Cache\CacheInterface;

/**
* 
*/
class CacheDecorator extends AbstractProfileDecorator
{
	protected $cache;
	
	function __construct(ProfileInterface $nextProfile, CacheInterface $cache)
	{
		parent::__construct($nextProfile);
		$this->cache = $cache;
	}
	
	public function getById($id)
	{
		$key = md5('id.'.$id);
		
	
		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$profile = $this->nextProfile->find($id);
		$this->cache->put($key, $profile);
		return $profile;
	}

	public function getByUser($username)
	{
		$key = md5('id.'.$username);
			
		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$profile = $this->nextProfile->getByUser($username);

		$this->cache->put($key, $profile);
		return $profile;
	}
	
	public function getPhoneNumber($profile_id, $phoneType)
	{
		$key = md5('id.'.$profile_id.'.'.$phoneType);
		
		
		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$phoneNumber = $this->nextProfile->getPhoneNumber($profile_id, $phoneType);
		
		$this->cache->put($key, $phoneNumber);
		
		return $phoneNumber;
	}	

	public function photos($id)
	{
		$key = md5('photos.'.$id);

		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}
		
		$photos = $this->nextProfile->photos($id);

		$this->cache->put($key, $photos);
			
		return $photos;
	}

	
}

?>