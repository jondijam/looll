<?php namespace Looll\Repo\Profile;

interface ProfileInterface
{
	public function getById($id);
	public function getAll();
	public function savePhoto($photo, $id);
	public function create($data, $country_id, $user_id);
	public function update($data, $country_id, $profile_id);
	public function destroy($id);
	public function getByUser($username);
	public function getPhoneNumber($profile_id, $phoneType);
	public function createPhoneNumber($phoneNumber, $profile);
	public function photos($id);
}


?>