<?php namespace Looll\Repo\Profile;

	use Looll\Repo\User\UserInterface;
	use Looll\Repo\Country\CountryInterface;
	use Looll\Repo\PhoneNumber\PhoneNumberInterface;
	use Illuminate\Database\Eloquent\Model;
	use Looll\Repo\DbRepository;

	class EloquentProfile extends DbRepository implements ProfileInterface
	{
		protected $model;
		protected $user;
		protected $country;
		protected $phoneNumber;

		function __construct(Model $model, UserInterface $user, Model $country, PhoneNumberInterface $phoneNumber)
		{
			$this->user = $user;
			$this->model = $model;
			$this->country = $country;
			$this->phoneNumber = $phoneNumber;
		}

		public function create($data, $country_id, $user_id)
		{
			$user = $this->user->find($user_id);
			$country = $this->country->find($country_id);

			$profile = $this->model->create($data);
			$profile->user()->associate($user);
			$profile->country()->associate($country);
			$profile->save();

			return $profile;
		}

		public function update($data, $country_id, $profile_id)
		{
			$country = $this->country->find($country_id);
			$profile = $this->model->find($profile_id);

			$profile->fill($data);
			$profile->country()->associate($country);
			$profile->save();

			return $profile;
		}
		
		public function destroy($id)
		{
			$this->model->delete($id);
		}
		
		public function getById($id)
		{
			return $this->model->with('photos')->find($id);
		}

		public function getByUser($username)
		{
			$user = $this->user->withProfile($username);

			return $user->profile;
		}
		
		public function getPhoneNumber($profile_id, $phoneType)
		{
			$profile = $this->model->find($profile_id);

			$phone_numbers = $profile->phone_numbers()->where('phone_type', $phoneType)->first();	


			if(!isset($phone_numbers))
			{
				return array();
			}
			
			return $phone_numbers->toArray();
		}
		
		public function createPhoneNumber($data, $profile_id)
		{
			$profile = $this->model->find($profile_id);
			$phone = $this->phoneNumber->create($data, $profile);

			return $phone;
		}
		
		public function photos($profile_id)
		{
			$profile = $this->model->with('photos')->find($profile_id);

			$profile = $profile->toArray();

			$photos = $profile['photos'];

			return $photos;
		}
	}
?>