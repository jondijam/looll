<?php namespace Looll\Repo\Profile;

abstract class AbstractProfileDecorator implements ProfileInterface
{
	protected $nextProfile;

	public function __construct(ProfileInterface $nextProfile)
	{
		$this->nextProfile = $nextProfile;
	}

	public function getById($id)
	{
		return $this->nextProfile->getById($id);
	}

	public function getAll()
	{
		return $this->nextProfile->getAll();
	}

	public function savePhoto($photo, $id)
	{
		return $this->nextProfile->savePhoto($photo, $id);
	}

	public function create($data, $country_id, $user_id)
	{
		return $this->nextProfile->create($data, $country_id, $user_id);
	}

	public function update($data, $country_id, $profile_id)
	{
		return $this->nextProfile->update($data, $country_id, $profile_id);
	}
	
	public function destroy($id)
	{
		return $this->nextProfile->destroy($id);
	}

	public function getByUser($username)
	{
		return $this->nextProfile->getByUser($username);
	}
	
	public function getPhoneNumber($profile_id, $phoneType)
	{
		return $this->nextProfile->getPhoneNumber($profile_id, $phoneType);
	}
	
	public function createPhoneNumber($phoneNumber, $id)
	{
		return $this->nextProfile->createPhoneNumber($phoneNumber, $id);
	}
	
	public function photos($id)
	{
		return $this->nextProfile->photos($id);
	}

}

?>