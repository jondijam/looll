<?php namespace Looll\Repo\Organisation;

	abstract class AbstractOrganisationDecorator implements OrganisationInterface
	{
		protected $nextOrganisation;

		public function __construct(OrganisationInterface $nextOrganisation)
		{
			$this->nextOrganisation = $nextOrganisation;
		}

		public function getById($id)
		{
			return $this->nextOrganisation->getById($id);
		}

		public function getAll()
		{
			return $this->nextOrganisation->all();
		}

		public function byPage($page=1, $limit=10, $username)
    	{
        	return $this->nextOrganisation->byPage($page, $limit, $username);
    	}

		public function savePhoto($photo, $id)
		{
			return $this->nextOrganisation->savePhoto($photo, $id);
		}

		public function create($data, $country_id, $parent_id, $user_id, $role_id = 1, $accepted = 1)
		{
			return $this->nextOrganisation->create($data, $country_id, $parent_id, $user_id, $role_id = 1, $accepted = 1);
		}

		public function addUser($id, $userId, $role_id = 3, $accepted = 0)
		{
			return $this->nextOrganisation->addUser($id, $userId, $role_id = 3, $accepted = 0);
		}

		public function updateUser($id, $userId, $roleId, $accepted)
		{
			return $this->nextOrganisation->updateUser($id, $userId, $roleId, $accepted);
		}

		public function getByUser($username)
		{
			return $this->nextOrganisation->getByUser($username);
		}

		public function getParentsByOrganisation($organisationId)
		{
			return $this->nextOrganisation->getParentsByOrganisation($organisationId);
		}

		public function getChildren($id, $accepted)
		{
			return $this->nextOrganisation->getChildren($id, $accepted);
		}

		public function getChildrenCount($id, $accepted)
		{
			return $this->nextOrganisation->getChildrenCount($id, $accepted = 1);
		}

		public function updateChild($organisationId, $data)
		{
			return $this->nextOrganisation->updateChild($organisationId, $data);
		}

		public function getParents($id)
		{
			return $this->nextOrganisation->getParents($id);
		}

		public function getParentsByType($typeId = 3)
		{
			return $this->nextOrganisation->getParentsByType($id);
		}

		public function update($organisationId, $input, $countryId, $parentId)
		{
			return $this->nextOrganisation->update($organisationId, $input, $countryId, $parentId);
		}

		public function destroy($id)
		{
			return $this->nextOrganisation->destroy($id);
		}

		public function getUsers($id, $roleId, $accepted, $active)
		{
			return $this->nextOrganisation->getUsers($id, $roleId, $accepted, $active);
		}

		public function activeUsers($id, $roleId = 2, $roleIncluded = true)
		{
			return $this->nextOrganisation->activeUsers($id, $roleId = 2, $roleIncluded = true);
		}

		public function getActiveUsersCount($organisationId, $roleId, $accepted = 1, $active = 1)
		{
			return $this->nextOrganisation->getActiveUsersCount($organisationId, $roleId, $accepted = 1, $active = 1);
		}

		public function getUsersCount($id, $roleId)
		{
			return $this->nextOrganisation->getUsersCount($id, $roleId);
		}

		public function getPhotos($id)
		{
			return $this->nextOrganisation->getPhotos($id);
		}

	} 	

?>