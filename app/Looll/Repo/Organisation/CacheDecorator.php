<?php namespace Looll\Repo\Organisation;

use Looll\Service\Cache\CacheInterface;

class CacheDecorator extends AbstractOrganisationDecorator
{
	protected $cache;
	
	function __construct(OrganisationInterface $nextOrganisation, CacheInterface $cache)
	{
		parent::__construct($nextOrganisation);
		$this->cache = $cache;
	}

	public function getById($id)
	{
		$key = md5('id.'.$id);
		
	
		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$organisation = $this->nextOrganisation->getById($id);
		$this->cache->put($key, $organisation);
		return $organisation;
	}

	public function getAll()
	{
		$key = md5('getAll');

		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}
		$organisations = $this->nextOrganisation->getAll();
		$this->cache->put($key, $organisations);
		
		return $organisations;
	}

	/**
     * Attempt to retrieve from cache
     * {@inheritdoc}
     */
    public function byPage($page=1, $limit=10, $username)
    {
        $key = md5('page.'.$page.'.'.$limit.'.'.$username);
        
        if( $this->cache->has($key) )
        {
            return $this->cache->get($key);
        }
        
        $paginated = $this->nextOrganisation->byPage($page, $limit, $username);
        $this->cache->put($key, $paginated);
        
        return $paginated;
    }

	public function getPhotos($organisationId)
	{
		$key = md5('photos.'.$organisationId);

		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}
		
		$photos = $this->nextOrganisation->getPhotos($organisationId);

		$this->cache->put($key, $photos);
			
		return $photos;
	}
}

?>