<?php namespace Looll\Repo\Organisation;

use Looll\Repo\Country\CountryInterface;
use Looll\Repo\User\UserInterface;
use Illuminate\Database\Eloquent\Model;
use Looll\Repo\DbRepository;
/**
* 
*/
class EloquentOrganisation extends DbRepository implements OrganisationInterface
{
	protected $model;
	protected $country;
	protected $user;

	public function __construct(Model $model, CountryInterface $country, UserInterface $user)
	{
		$this->model = $model;
		$this->country = $country;
		$this->user = $user;
	}

	public function getAll()
	{
		return $this->model->all();
	}

	/**
    * Get paginated organisations
    *
    * @param int Current Page
    * @param int Number of articles per page
    * @return StdClass Object with $items and $totalItems for pagination
    */
    public function byPage($page=1, $limit=10, $username)
    {
    	$result = new \StdClass;
    	$result->page = $page;
		$result->limit = $limit;
		$result->totalItems = 0;
		$result->items = array();

    	$user = $this->user->getOrganisations($username);
		$organisations = $user->organisations()->orderBy('updated_at', 'DESC')->skip( $limit * ($page-1) )->take($limit)->get();

		$result->items = $organisations->all();
		$result->totalItems = $this->totalOrganisatons($username);

		return $result;
    }

    protected function totalOrganisatons($username)
    {
    	$user = $this->user->getOrganisations($username);
		return $user->organisations()->count();
    }
	
	public function create($data, $country_id, $parent_id, $user_id, $role_id = 1, $accepted = 1)
	{
		$parent_organisation = array();
		$country = array();

		if ($parent_id > 0) $parent_organisation = $this->model->find($parent_id);

		$country = $this->country->getById($country_id);
		$organisation = $this->model->create($data);
		$organisation = $this->model->find($organisation->id);

		if (!empty($parent_organisation))
		{
			$organisation->parent()->associate($parent_organisation);
		}

		if (!empty($country))
		{
			$organisation->country()->associate($country);
		}

		$organisation->save();
		
		$organisation->users()->attach($user_id, array('role_id' => $role_id, 'accepted'=> $accepted));

		return $organisation;
	}

	public function addUser($id, $userId, $role_id = 3, $accepted = 0)
	{
		$organisation = $this->model->find($id);

		$organisation->users()->attach($userId, array('role_id' => $role_id, 'accepted'=>$accepted));
	}

	public function updateUser($id, $userId, $role_id, $accepted)
	{
		
		$organisation = $this->model->find($id);
		
		if ($role_id == 2) 
		{
			if($accepted)
			{
				$organisation->users()->attach($userId, array('role_id' => $role_id, 'accepted'=>$accepted));
			}

			if(!$accepted)
			{
				$organisation->users()->newPivotStatementForId($userId)->whereRoleId($role_id)->delete();
			}

			
		}
		elseif($role_id != 2)
		{
			$organisation->users()->updateExistingPivot($userId, ['role_id'=>$role_id, 'accepted'=>$accepted]);
		}
	}

	public function getByUser($username)
	{
		$users = $this->user->getOrganisations($username);
		$organisations = $users->organisations()->orderBy('created_at', 'DESC')->get();

		return $organisations;
	}

	public function getChildren($id, $accepted = 1)
	{
		$getChildren = array();

		$organisation = $this->model->find($id);
		
		if($accepted) $getChildren = $organisation->children()->where('parent_acceptance', $accepted)->get();
		if(!$accepted) $getChildren = $organisation->children()->get();

		if(empty($getChildren)) return array();

		return $getChildren;
	}

	public function getParentsByOrganisation($organisationId)
	{
		$parentsCount = $this->model->whereIsBefore($organisationId)->count();

		if($parentsCount == 0) return array();

		$parents = $this->model->whereIsBefore($organisationId)->get();

		return $parents;
	}



	public function getChildrenCount($id, $accepted)
	{
		$organisation = $this->model->find($id);
		$getChildren = $organisation->children()->where('parent_acceptance', $accepted)->count();

		return $getChildren;
	}

	public function getParents($id, $memberTypeId = 3)
	{
		$parents = $this->model->where('member_type_id', $memberTypeId)->where('id', '!=', $id)->lists('name', 'id');
		return $parents;
	}

	public function getParentsByType($typeId = 3)
	{
		return $this->organisation->where('member_type_id', $typeId)->lists('name', 'id');
	}

	public function updateChild($organisationId, $data)
	{
		$organisation = $this->model->find($organisationId);
		$organisation->fill($data);
		$organisation->save();
	}

	public function update($organisationId, $input, $countryId, $parentId)
	{
		$parent = $this->model->find($parentId);

		$organisation = $this->model->find($organisationId);
		$organisation->fill($input);
		$country = $this->country->getById($countryId);

		if ($countryId > 0) $organisation->country()->associate($country);
		if (isset($parent)) $organisation->parent()->associate($parent);
		
		$organisation->save();
	}

	public function destroy($id)
	{
		$organisation = $this->model->find($id);
		$organisation->users()->detach();
		$organisation->delete();
	}

	public function getUsers($id, $roleId, $accepted, $active)
	{
		$users = array();
		$organisation = $this->model->find($id);

		$users = $organisation->users()->with(array('profile' => function($query) use ($active, $accepted)
		{
			if($accepted) $query->where('active', $active);
		}))->where( function($query) use ($roleId, $accepted)
		{
			if($accepted) 
			{
				$query->where('accepted', 1)->where('role_id', '=', '1')->orWhere('role_id', '=', '' . $roleId . '');
			}
			elseif ($accepted == 2) 
			{
				$query->where('accepted', 1)->where('role_id', '!=', '1')->where('role_id', '=', '' . $roleId . '');
			}
        	elseif (!$accepted ) 
        	{
        		$query->where('role_id', '=', '' . $roleId . '');
        	}

        
        })->get();

		if(empty($users)) return array();
		

		return $users;
	}
	
	public function getActiveUsersCount($organisationId, $roleId, $accepted = 1, $active = 1)
	{
		$organisation = $this->model->find($organisationId);

		$users = $organisation->users()->with(array('profile' => function($query) use ($active)
		{
			$query->where('active', $active);

		}))->where( function($query) use ($roleId, $accepted)
		{
			if($accepted) 
			{
				$query->where('accepted', 1)->where('role_id', '=', '1')->orWhere('role_id', '=', '' . $roleId . '');
			}
        	elseif (!$accepted ) 
        	{
        		$query->where('role_id', '=', '' . $roleId . '');
        	}

        
        })->count();

        return $users;
	}

	public function getUsersCount($organisationId, $userId)
	{

		$organisation = $this->model->with(array('users' => function($query) use ($userId)
		{
			$query->where('user_id', $userId);
		}))->find($organisationId);

		if (empty($organisation)) 
		{
			return 0;
		}

		$organisation = $organisation->toArray();

		return count($organisation['users']);
	}

	public function getPhotos($id)
	{
		$organisation = $this->model->with('photos')->find($id);
		$photos = $organisation->photos()->get();

		if(empty($photos)) return array();

		return $photos;
	}

}


?>