<?php namespace Looll\Repo\Organisation;

interface OrganisationInterface
{
	public function getById($id);
	public function getAll();

	/**
 	* Get paginated articles
 	*
 	* @param int Current Page
	* @param int Number of articles per page
	* @return object Object with $items and $totalItems for pagination
	*/
	public function byPage($page=1, $limit=10, $username);
	
	public function savePhoto($photo, $id);
	public function update($organisationId, $input, $country_id, $parent_id);
	public function create($data, $country_id, $parent_id, $user_id, $role_id = 1, $accepted = 1);
	public function addUser($id, $userId, $role_id = 3, $accepted = 0);
	public function updateUser($id, $userId, $role_id, $accepted);
	public function getByUser($username);
	public function getParentsByOrganisation($organisationId);
	public function getChildren($id, $accepted);
	public function getChildrenCount($id, $accepted);
	public function updateChild($organisationId, $data);
	public function getParents($id);
	public function getParentsByType($typeId = 3);
	public function destroy($id);
	public function getUsers($id, $roleId, $accepted, $active);
	public function getActiveUsersCount($organisationId, $roleId, $accepted = 1, $active = 1);
	public function getUsersCount($id, $roleId);
	public function getPhotos($id);
}

?>