<?php namespace Looll\Repo\User;

interface UserInterface
{
	public function create($data);

	public function find($id);

	public function confirm($data = array());

	public function withProfile($username);

	public function getOrganisations($username);

}


?>