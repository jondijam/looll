<?php namespace Looll\Repo\User;


use Looll\Repo\Profile\ProfileInterface;

use Illuminate\Database\Eloquent\Model;
use User;

/**
* 
*/
class EloquentUser implements UserInterface
{
	protected $user;

	function __construct(Model $user)
	{
		$this->user = $user;
	}

	public function create($data)
	{
		$user = $this->user->create($data);
		return $user;
	}

	public function find($id)
	{
		return $this->user->find($id);
	}

	public function confirm($data = array())
	{
		$user = User::where('confirmation_code', $data['code'])->firstOrFail();
		$user->confirmed = true;
		$user->confirmation_code = $user->confirmation_code;
		$user->save();
	}

	public function withProfile($username)
	{
		return $this->user->with('profile')->where('username', $username)->firstOrFail();
	}

	public function getOrganisations($username)
	{
		$users = $this->user->with('organisations')->where('username', $username)->firstOrFail();
		return $users;
	
	}

}
?>