<?php namespace Looll\Repo\PhoneNumber;

interface PhoneNumberInterface
{
	public function create($data, $profile);

	public function update($phoneNumber, $phoneNumberId);
}

?>