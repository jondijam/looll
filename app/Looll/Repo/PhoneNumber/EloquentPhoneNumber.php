<?php namespace Looll\Repo\PhoneNumber;

	use Illuminate\Database\Eloquent\Model;
	/**
	* @param phones numbers
	* @return phones numbers instance
	*/
	class EloquentPhoneNumber implements PhoneNumberInterface
	{
		protected $phoneNumber;

		function __construct(Model $phoneNumber)
		{
			$this->phoneNumber = $phoneNumber;	
		}

		public function create($data, $profile)
		{
			$phoneNumber = $this->phoneNumber->create($data);
			$phoneNumber->profile()->associate($profile);
			$phoneNumber->save();
			
			return $phoneNumber;
		}

		public function update($phoneNumber, $phoneNumberId)
		{
			$phone = $this->phoneNumber->find($phoneNumberId);
			$phone->phone_number = $phoneNumber;
			$phone->save();
			
		}
	}

?>