<?php namespace Looll\Repo\Country;

interface CountryInterface
{
	public function getById($id);
	public function getAll();
}

?>