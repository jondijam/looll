<?php namespace Looll\Repo\Country;

	use Illuminate\Database\Eloquent\Model;
	use Looll\Repo\DbRepository;
	
	class EloquentCountry extends DbRepository implements CountryInterface
	{
		protected $model;

		public function __construct(Model $model)
		{
			$this->model = $model;
		}

		public function getAll()
		{
			return $this->model->lists('name', 'id');
		}
		
	}
?>