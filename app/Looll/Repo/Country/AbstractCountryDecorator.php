<?php namespace Looll\Repo\Country; 

abstract class AbstractCountryDecorator implements CountryInterface
{	
	protected $nextCountry;

	public function __construct(CountryInterface $nextCountry)
	{
		$this->nextCountry = $nextCountry;
	}

	public function getById($id)
	{
		return $this->nextCountry->getById($id);
	}

	public function getAll()
	{
		return $this->nextCountry->getAll();
	}
}

?>