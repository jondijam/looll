<?php namespace Looll\Repo\Country; 
	use Looll\Service\Cache\CacheInterface;

	class CacheDecorator extends AbstractCountryDecorator
	{
		protected $cache;
	
		function __construct(CountryInterface $nextCountry, CacheInterface $cache)
		{
			parent::__construct($nextCountry);
			$this->cache = $cache;
		}
		public function getById($id)
		{
			$key = md5('id.'.$id);

			if( $this->cache->has($key) )
			{
				return $this->cache->get($key);
			}

			$country = $this->nextCountry->getById($id);
			$this->cache->put($key, $country);
			return $country;
		}

		public function getAll()
		{
			$key = md5('getAll');

			if( $this->cache->has($key) )
			{
				return $this->cache->get($key);
			}

			$countries = $this->nextCountry->getAll();
			$this->cache->put($key, $countries);
			
			return $countries;
		}
	}

?>