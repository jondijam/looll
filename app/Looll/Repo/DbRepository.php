<?php namespace Looll\Repo; 

abstract class DbRepository
{
	 /**
     * Eloquent model
     */
    protected $model;

    function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Fetch a record by id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function savePhoto($photo, $id)
    {
        $model = $this->model->find($id);
        $model->photos()->save($photo);
    }

    /*
    public function create($data)
    {
        $model = $this->model->create($data);
        return $model;
    }

    public function update($data, $id)
    {
        $model = $this->getById($id);
        $model = $model->fill($data);
        $model = $model->save();

        return $model;

    }*/

    public function destroy($id)
    {
        $model = $this->model->find($id);
        $model = $model->delete($id);
    }
}

?>