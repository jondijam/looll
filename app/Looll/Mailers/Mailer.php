<?php namespace Looll\Mailers;

use Mail;
/**
* 
*/
abstract class Mailer 
{
	public function sendTo($user, $subject, $view, $data)
	{
		Mail::send($view, $data, function($message) use($user, $subject)
		{
			$message->from('looll.is@looll.is', 'looll.is');
			$message->to($user->email)->subject($subject);
		});
	}
}


?>