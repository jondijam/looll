<?php namespace Looll\Mailers;

use User;
/**
* 
*/
class UserMailer extends Mailer
{
	public function welcome(User $user)
	{
		$view = 'emails.confirmation';
		$data = ['confirmation_code'=>$user->confirmation_code, 'username'=>$user->username];
		$subject = "Welcome to Looll, please confirm!";

		$this->sendTo($user, $subject, $view, $data);
	}
}
?>