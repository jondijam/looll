<?php namespace Looll\Forms;

	use Laracasts\Validation\FormValidator;

	class OrganisationForm extends FormValidator
	{
		protected $rules = [
			'name' => 'required|unique:organisations',
			'email' => 'email|unique:organisations'
		];

		public function excludeNameEmailId($id)
		{
	    	$this->rules['name'] = "required|unique:organisations,name,$id";
	    	$this->rules['email'] = "email|unique:organisations,email,$id";

	    	return $this;
		}
	} 
?>