<?php namespace Looll\Forms;

	use Laracasts\Validation\FormValidator;

	class ProfileForm extends FormValidator
	{
		protected $rules = [
			'name' => 'required',
			'email' => 'email'
		];
	} 


?>