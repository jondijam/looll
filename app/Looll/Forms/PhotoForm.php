<?php namespace Looll\Forms;

	use Laracasts\Validation\FormValidator;

	class PhotoForm extends FormValidator
	{
		protected $rules = [
			"filename" => "required",
	        "file_ori_size" => "required",
	        "file_upload_size" => "required",
	        "image_primary" => "required",
		];
	} 
?>