<?php 

function errors_for($attribute, $errors)
{
	return $errors->first($attribute, '<span class="error">:message</span>');
}

function link_to_profile($text = 'Profile')
{
	return link_to_route('profile', $text, Auth::user()->username);
}

function link_to_create_profile($text = 'Edit')
{
	return link_to_route('profile.create', $text);
}

function link_to_edit_profile($text = 'Edit')
{
	return link_to_route('profile.edit', $text, Auth::user()->username);
}


function set_active($path, $active = "active")
{
	return Request::is($path) ? $active : '';
}

?>