<?php namespace Looll\Service\Cache;

	interface CacheInterface
	{
		public function get($key = null);

		public function put($key, $value, $minutes = null);

		public function forget($key);

		public function has($key);
	}
?>