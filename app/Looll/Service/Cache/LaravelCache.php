<?php namespace Looll\Service\Cache;
	
	use Illuminate\Cache\CacheManager;

	class LaravelCache implements CacheInterface
	{
		protected $cache;
		protected $cachekey;
		protected $minutes;

		public function __construct(CacheManager $cache, $cachekey, $minutes = null)
		{
			$this->cache = $cache;
			$this->cachekey = $cachekey;
			$this->minutes = $minutes;
		}

		public function get($key = null)
		{
			return $this->cache->tags($this->cachekey)->get($key);
		}

		public function put($key, $value, $minutes=null)
		{
			if( is_null($minutes) )
			{
				$minutes = $this->minutes;
			}

			return $this->cache->tags($this->cachekey)->put($key, $value, $minutes);
		}

		public function forget($key)
		{
			return $this->cache->section($this->cachekey)->forget($key);
		}

		public function has($key)
		{
			return $this->cache->tags($this->cachekey)->has($key);
		}
	}

?>