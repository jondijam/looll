<?php namespace Looll\Service\Google;

use \Google\Google_Client;
/**
* 	
*/
class GaService
{
	protected $client;
	public function __construct()
	{
		$client = new \Google\Google_Client;

    	$this->client = $client;
    	$this->init();
	}
	 
	private function init(){
			$this->client->setApplicationName("looll");
	        $this->client->setClientId(Config::get('drive.client_id') );
	        $this->client->setClientSecret(Config::get('drive.client_secret'));
	        $this->client->setDeveloperKey(Config::get('drive.api_key'));
	        $this->client->setRedirectUri('http://looll.is/admin/login');
	        $this->client->setScopes(array('https://www.googleapis.com/auth/drive'));
	}

	public function isLoggedIn()
	{
			if (isset($_SESSION['token'])) 
			{
      				$this->client->setAccessToken($_SESSION['token']);
      				return true;
      		}

      		return $this->client->getAccessToken();
    }

	public function login( $code )
	{
		$this->client->authenticate($code);
		$token = $this->client->getAccessToken();
    	$_SESSION['token'] = $token;

    	return $token;
	}

	public function getLoginUrl()
	{
		$authUrl = $this->client->createAuthUrl();
	}
}

?>