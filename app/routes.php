<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.

*/

#home


Route::get('/', array('as'=>'home','uses' => 'HomeController@getIndex'));

Route::get('about', array(
    'as' => 'about',
    'uses' => 'HomeController@getAbout'
));

#search
Route::group(array('namespace' => 'Search', 
	'prefix' => 'search'), 
	function()
	{
    	Route::post('/contacts', array(
    		'before' => 'csrf',
    		'as' => 'contacts',
    		'uses' => 'SearchController@contacts'
    		));
	});

Route::resource('search', 'SearchController', ['only' => ['index']]);

#registration
Route::get('register/confirm',array(
    'as' => 'confirm',
    'uses' => 'RegistrationController@confirm'
    ));

Route::get('register/confirmed/{username}/{code}', [
    'as' => 'confirmed', 
    'uses' => 'RegistrationController@confirmed'
]);

Route::get('register', array('as'=>'register', 'uses'=>'RegistrationController@create'));
Route::resource('registration', 'RegistrationController', array('only' => array('create', 'confirm', 'confirmed', 'store')));

#Authentication
Route::get('login', ['as'=> 'login', 'uses' => 'SessionsController@create']);
Route::get('logout', 'SessionsController@destroy');

Route::resource('sessions', 'SessionsController' );

Route::get('password/remind', 'RemindersController@getRemind');
Route::post('password/remind', 'RemindersController@postRemind');
Route::controller('password', 'RemindersController');

Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfilesController@index']);
Route::get('/{profile}', ['as' => 'profile', 'uses' => 'ProfilesController@show']);
Route::resource('profile', 'ProfilesController', ['only' => ['index', 'show', 'edit']]);
Route::resource('users.profiles', 'UserProfileController', ['only' => ['store', 'update']]);
Route::resource('photo', 'PhotoController');


#organisations

//Route::get('/organisation/{id}/users', ['as' => 'members', 'uses' => 'SearchController@show']);

Route::resource('organisation', 'OrganisationsController');
Route::post('/organisation/{organisations}/user/{users}/connect', ['as'=>'organisations.users.connect', 'uses'=>'OrganisationUserController@store']);
Route::resource('organisations.users', 'OrganisationUserController', array('only' => array('update', 'destroy','index')));
Route::resource('organisations.children', 'OrganisationChildController', array('only' => array('update')));



Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function()
{
        Route::group(['before' => 'role:admin'], function()
        {
            
        });
        
        Route::get('login', ['as' => 'admin.login', 'uses' => 'SessionsController@create']);
        Route::resource('sessions', 'SessionsController',  array('only' => array('create', 'store', 'destroy')));
});