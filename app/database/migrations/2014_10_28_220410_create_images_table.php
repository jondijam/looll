<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('imageable_id');
			$table->string('imageable_type');
			$table->string('filename');
			$table->boolean('primary');
			$table->string('src');
			$table->string('path');
			$table->decimal('file_ori_size', 10, 2);
			$table->decimal('file_upload_size', 10, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
