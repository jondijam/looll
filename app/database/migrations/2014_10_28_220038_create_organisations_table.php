<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organisations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('email')->unique();
			$table->integer('member_type_id');
			$table->string('description', 500)->nullable();
			$table->string('announcement', 500)->nullable();
			$table->string('address')->nullable();
			$table->string('city')->nullable();
			$table->string('zip')->nullable();
			$table->integer('country_id')->default(265);
			$table->integer('parent_id')->default(0);
			$table->boolean('parent_acceptance')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organisations');
	}

}
