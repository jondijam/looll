<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrganisationUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('organisation_user'))
		{	
			$from = 'organisation_user';
			$to = 'organisation_role_user';

			Schema::rename($from, $to);
		}

		if (Schema::hasTable('organisation_role_user'))
		{
			Schema::table('organisation_role_user', function(Blueprint $table)
			{

				if (Schema::hasColumn('owner', 'member'))
				{

					$table->renameColumn('owner', 'role_id');
					$table->renameColumn('member', 'accepted');

				}

				if (Schema::hasColumn('user_id', 'organisation_id'))
				{
					$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
					$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
				}
			});
		}

	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
