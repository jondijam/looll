<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('active')->default(1);
			$table->string('name');
			$table->string('gender');
			$table->date('birthdate')->nullable();
			$table->string('marital_status');
			$table->string('description', 500)->nullable();
			$table->string('announcement', 500)->nullable();
			$table->string('email')->nullable();
			$table->string('address')->nullable();;
			$table->string('zip')->nullable();;
			$table->string('city')->nullable();;
			$table->integer('country_id');
			$table->string('position');
			$table->string('company');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
