<?php  
class UserTableSeeder extends Seeder {

    public function run()
    {
    	$faker = Faker\Factory::create('is_IS');

        for ($i = 0; $i < 10; $i++)
		{
		  	$user = User::create(array( 'username' => $faker->userName, 'email'=> $faker->email,'password' => "123", 'confirmed' => 1,));
			
			$country = Country::find('104');
			$profile = Profile::create(array('active'=>true, 
				'name' => $faker->name,
				'gender' => 'male',
				'birthdate' => $faker->dateTimeThisCentury($max = 'now'),
				'marital_status' => $faker->numberBetween(1,3)	,
				'description' => $faker->paragraph($nbSentences = 3) ,
				'announcement' => $faker->paragraph($nbSentences = 3) ,
				'email' => $faker->email,
				'address' => $faker->address,
				'zip' => $faker->postcode,
				'city' => $faker->city,
				'position' =>"",
				'company' => $faker->company,
			));

			$profile->country()->associate($country);
			$profile->user()->associate($user);
			$profile->save();
		}
    }

}

?>