<?php  
class OrganisationTableSeeder extends Seeder 
{
	public function run()
	{
		$faker = Faker\Factory::create('is_IS');
		
		for ($i = 0; $i < 10; $i++)
		{
			$user_id = $faker->numberBetween(31, 40);
			
			$user = User::find($user_id);
			$users = User::where('id', '!=', $user_id)->lists('id');

			$member_type_id = $faker->numberBetween(2,3);
			
			$organisation = Organisation::create([
				'name'=>$faker->unique()->company,
				'email' => $faker->unique()->email,
				'member_type_id' => $member_type_id,
				'description'=> $faker->paragraph($nbSentences = 3),
				'announcement' => $faker->paragraph($nbSentences = 3),
				'address' => $faker->streetAddress,
				'city' => $faker->city,
				'zip' => $faker->postcode,
			]);

			$country = Country::find('104');
			$organisation->country()->associate($country);
			$organisation->save();

			$user->organisations()->attach($organisation->id, ['role_id' => 1, 'accepted' => 1]);

			if($member_type_id == 2)
			{
				$organisation->users()->attach($users, ['role_id' => 3, 'accepted' => $faker->numberBetween(0,1)]);
			}
			
			
		}
		
	}
}
?>