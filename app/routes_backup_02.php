Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');

Route::post('/search/suggestions', 'SearchController@searchSuggestions');
Route::post('/search/contacts','SearchController@searchContacts');
Route::post('/search/result','SearchController@searchResult');



Route::get('/register', 'AccountController@register');
Route::post('/register/save', array('before' => 'csrf', 'uses'=>'AccountController@saveUser'));
Route::get('/register/confirm', 'AccountController@confirm');
Route::get('/email/confirmation/{code}', 'AccountController@confirmation');

Route::get('/logout', 'AccountController@logout');
Route::get('/login', 'AccountController@login');
Route::post('/login', 'AccountController@signin');

Route::controller('password', 'RemindersController');



Route::get('/profile/{id}', 'ProfilesController@view');
Route::get('/edit', 'ProfilesController@edit');
Route::post('/profile/save/{id}', 'ProfilesController@store');
Route::resource('profile', 'ProfilesController',
                array('except' => array('index')));



Route::post('/images/primary', 'ImagesController@editPrimaryImages');
Route::post('/images/delete/{id}', 'ImagesController@deleteImage');
Route::post('/images/view', 'ImagesController@view');
Route::post('/images/upload', 'ImagesController@upload');




Route::post('/organisation/{parentId}/accept/children/{childrenId}', 'OrganisationChildrenController@accept');

//Route::resource('organisation.children', 'OrganisationChildrenController'));

//Route::post('/organisation/{parentId}/decline/children/{childrenId}', 'OrganisationChildrenController@decline');
//Route::post('/organisation/{organisationId}/user/{userId}/accept', 'OrganisationController@acceptUser');

Route::post('/organisation/{organisationId}/user/remove/{$user_id}', 'OrganisationUserController@removeUser');
Route::post('/organisation/{id}/user/connect/{$user_id}', 'OrganisationUserController@connectUser');
Route::post('/organisation/{id}/user/disconnect/{$user_id}', 'OrganisationUserController@disconnectUser');
Route::post('/organisation/{id}/user/decline/{$user_id}', 'OrganisationUserController@declineUser');
Route::resource('organisations.users', 'OrganisationUserController');

Route::post('/organisation/{id}/contact/add/{$user_id}', 'OrganisationController@addContact');
Route::post('/organisation/{id}/contact/destroy/{$user_id}', 'OrganisationsController@destroyContact');

Route::resource('organisation', 'OrganisationController',
                array('except' => array('index')));







//Route::post('/organisation/connect/user', 'OrganisationController@connectUser');

/*
Route::get('/organisation/{id}', 'OrganisationsController@view');
Route::get('/organisation/edit/{id}','OrganisationsController@edit');
Route::get('/organisation/delete/{id}', 'OrganisationsController@delete');
Route::post('/organisation/save/{id}','OrganisationsController@store');
*/



Route::get('/organisation/contact/destroy/{id}','OrganisationsController@destroyDelete');




//Route::post('/x', 'HomeController@saveUser');
