<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UserCommand extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'looll:user';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check each user, does the user have profile or not.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->args([])->everyWeekday()->hours(11)->minutes(30);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Log::info("It works");

		$users = User::with('profile')->get();
		$now = new DateTime();
		foreach ($users as $user) 
		{
			$t1 = StrToTime ($user->created_at);
			$t2 = StrToTime ($now->format('Y-m-d H:i:s'));

			$diff = $t2 - $t1;
			$hours = round($diff / ( 60 * 60 ));

			if(!isset($user->profile) && $hours >= 3)
			{
				Log::info($user->username." was deleted successfully");
				$user->delete();
			}
		}
	}

	
	/*
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	/*
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	/*
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
	*/

}
