<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$crawler = $this->client->request('PUT', '/organisations/10/users/15/', ['accept'=>1, 'role_id'=>3, 'usersable'=>'acceptUser']);

		$this->assertTrue($this->client->getResponse()->isOk());
	}

}
