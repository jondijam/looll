<?php 

class Role extends Eloquent 
{
	protected $table = 'roles';
    protected $primaryKey = 'id';

    public function users()
    {
    	return $this->belongsToMany('User', 'organisation_role_user', 'role_id', 'user_id')->withPivot('organisation_id','accepted');
    }

    public function adminUsers()
    {
        return $this->belongsToMany('User');
    }

    public function organisations()
    {
    	return $this->belongsToMany('Organisation', 'organisation_role_user', 'role_id', 'organisation_id')->withPivot('user_id','accepted');
    }

}

?>