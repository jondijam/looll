<?php 
class Country extends Eloquent 
{

    protected $table = 'countries';
    protected $primaryKey = 'id';

    

    public function profiles()
    {
    	return $this->hasMany('Profile');
    }

    public function organisations()
    {
    	return $this->hasMany('Organisation');
    }
}
?>