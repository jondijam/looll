<?php 
class Photo extends Eloquent 
{
	protected $table = 'photos';
    protected $primaryKey = 'id';

    protected $fillable = ['imageable_type', 'imageable_id','filename', 'image_primary', 'file_ori_size', 'file_upload_size', 'path', 'src'];
    protected $guarded = array('id');

    public static $rules = array(
        "filename" => "required",
        "file_ori_size" => "required",
        "file_upload_size" => "required",
        "image_primary" => "required",
    );

    public static function boot()
    {
        static::deleting(function($model)
        {
            (string) $cacheKey = "";
            
            $key = md5('photos.'.$model->imageable_id);
            $key2 = md5('id.'.$model->imageable_id);

            if ($model->imageable_type == "Profile") $cacheKey = "profiles";
            if ($model->imageable_type == "Organisation") $cacheKey = "organisations";
            
            Cache::tags($cacheKey)->flush($key);
            Cache::tags($cacheKey)->flush($key2);       

            if (File::exists($model->path)) 
            {
                File::delete($model->path);
            }
        });

        static::updating(function($model)
        {
            (string) $cacheKey = "";
            
            $key = md5('photos.'.$model->imageable_id);
            $key2 = md5('id.'.$model->imageable_id);

            if ($model->imageable_type == "Profile") $cacheKey = "profiles";
            if ($model->imageable_type == "Organisation") $cacheKey = "organisations";
            
            Cache::tags($cacheKey)->flush($key);
            Cache::tags($cacheKey)->flush($key2);
        });

    }

    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) return true;
        $this->errors = $validation->messages(); 

        // validation pass
        return false;
    }

    public function imageable()
    {
        return $this->morphTo();
    }
}
?>