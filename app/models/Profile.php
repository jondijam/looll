<?php 
class Profile extends Eloquent 
{
	protected $table = 'profiles';
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'active','birthdate','marital_status', 'position', 'company', 'description', 'announcement', 'email', 'address', 'city', 'country_id', 'zip', 'gender'];
    protected $guarded = array('id');


    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            $key = md5('id.'.$model->id);
            Cache::tags('profiles')->flush($key);
        });

        static::deleting(function($model)
        {
            $key = md5('id.'.$model->id);
            Cache::tags('profiles')->flush($key);
        });
    }

    public function phone_numbers()
    {
    	return $this->hasMany('PhoneNumber');
    }

    public function photos()
    {
        return $this->morphMany('Photo', 'imageable');
    }

    public function user()
    {
    	return $this->belongsTo('User');
    }

    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function scopeSearch($query, $search)
    {
        
        return $query->where(function($query) use ($search)
        {

            $query->where('name', 'LIKE', "%$search%")->orWhere('address','LIKE', "%search%");
        });

    }
}
?>