<?php 
class Organisation extends Node 
{
	protected $table = 'organisations';
    protected $primaryKey = 'id';

    protected $fillable = [
    'name',
    'email',
    'member_type_id',
    'description',
    'announcement', 
    'private_announcement',
    'address',
    'city',
    'zip',
    'country_id', 
    'parent_id', 
    'parent_acceptance'];
    
    protected $guarded = array('id');

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $key = md5('id.'.$model->id);
            Cache::tags('organisations')->flush($key);
        });

        static::saving(function($model)
        {
            $key = md5('id.'.$model->id);
            Cache::tags('organisations')->flush($key);
        });

        static::deleting(function($model)
        {
            $key = md5('id.'.$model->id);
            Cache::tags('organisations')->flush($key);
        });
    }

    public function users()
    {
    	return $this->belongsToMany('User', 'organisation_role_user', 'organisation_id', 'user_id')->withPivot('role_id','accepted');
    }

    public function roles()
    {
        return $this->belongsToMany('Role', 'organisation_role_user', 'organisation_id', 'role_id')->withPivot('user_id','accepted');
    }

    public function photos()
    {
    	return $this->morphMany('Photo', 'imageable');
    }

    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function parent()
    {
        //return $this->hasOne('Category', 'category_id', 'parent_id');

        return $this->belongsTo('Organisation', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Organisation','parent_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(function($query) use ($search){
            $query->where('name', 'LIKE', "%$search%")->orWhere('address','LIKE',"%$search%");
        });
    }
}
?>