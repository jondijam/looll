<?php 
class PhoneNumber extends Eloquent 
{
	protected $table = 'phone_numbers';
    protected $primaryKey = 'id';
    protected $fillable = ['phone_number', 'phone_type'];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            $key = md5('id.'.$model->profile_id.'.'.$model->phone_type);
            Cache::tags('profiles')->flush($key);
        });
    }

    public function profile()
    {
    	 return $this->belongsTo('Profile');
    }

    public function scopeSearch($query, $search)
    {
    	if(strlen($search) >= 5)
    	{
    		return $query->where(function($query) use ($search){
	        	join('profiles', 'profile.id', '=', 'phone_number.profile_id')->join('users','profile.user_id','=','user.id')->where('phone_number','LIKE',"%$search%");
	        });
    	}
    	else
    	{
			return null;       
    	}
    }

}
?>