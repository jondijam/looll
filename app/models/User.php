<?php

use Illuminate\Auth\UserInterface;

use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;



class User extends Eloquent implements UserInterface, RemindableInterface {


	use RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey = 'id';
	protected $fillable = ['username', 'email', 'password', 'confirmation_code'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function organisations()
	{
		return $this->belongsToMany('Organisation', 'organisation_role_user', 'user_id', 'organisation_id')->withPivot('role_id', 'accepted');
	}

	public function profile()
	{
		return $this->hasOne('Profile');
	}

	public function roles()
	{
		return $this->belongsToMany('Role', 'organisation_role_user', 'user_id', 'role_id')->withPivot('organisation_id','accepted');
	}

	public function adminRoles()
	{
		return $this->belongsToMany('Role');
	}

	public function hasRole($name)
	{	
		$dd = array();

		$user = $this->with('adminRoles')->first();
		
		foreach ($user->roles as $role) 
		{
			if ($role->name == $name."\r\n" or $role->name == $name) 
			{
				return true;
			}
		} 

		return false;
	}

	public function scopeSearch($query, $search)
    {
        $query->whereHas('profile', function($query) use ($search)
        {
            $query->where(function ($query) use ($search)
        	{

        		return $query->where('name', 'LIKE', "%$search%");
        	});
       		
        });
    }

    public function scopeSearchMembers($query, $search)
    {

    	$query->whereHas('profile', function($query) use ($search)
        {
            $query->where(function ($query) use ($search)
        	{
        		
        		return $query->where('name', 'LIKE', "%$search%");
        	});
       		
        });
    }

    public function scopeSearchContacts($query, $search)
    {
    	//$pivot = $this->organisations()->getTable();

        $query->whereHas('profile', function($query) use ($search)
        {
            $query->where(function ($query) use ($search)
        	{

        		return $query->where('name', 'LIKE', "%$search%")->where('owner', '!=', 2)->where('member', 1);
        	});
       		
        });
    }

	public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function setConfirmationCodeAttribute($confirmation_code)
    {
    	if($confirmation_code != null)
    	{
    		$this->attributes['confirmation_code'] = null;
    	}
    	elseif ($confirmation_code == null) 
    	{
    		$this->attributes['confirmation_code'] = str_random(40);  
    	}
    	
    }

}
