<?php
use Looll\Forms\LoginForm;
class SessionsController extends BaseController 
{
	protected $login;

	/**
	 * @param LoginForm $loginForm
	 */
	function __construct(LoginForm $login)
	{
		$this->login = $login;
		
	}

	public function create()
	{
		$data = array();
		$errors = array();
		$errorMessage = "";

		(int) $profile_id = 0;
		$errors = Session::get('errors');

		if (!empty($errors)) 
		{
			$errorMessage = $errors->first('message');
		}
		
		$data['errorMessage'] = $errorMessage;
		
		return View::make('sessions/create', $data);
	}

	public function store()
	{

		$input = Input::only('username', 'password');

		$this->login->validate($input);

		
		if (Auth::attempt($input))
		{
			$user = Auth::user();

			$url = route('profile.edit', $user->username);
			
			return Redirect::intended($url);
		}

		return Redirect::back()->withInput()->withFlashMessage('Invalid credentials provided');
	}

	public function destroy()
	{
		Auth::logout();
		return Redirect::home();
	}
}
?>