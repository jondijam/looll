<?php  
	use Looll\Repo\Photo\PhotoInterface;
	use Looll\Files\FileInterface;

	class PhotoController extends \BaseController 
	{
		protected $photo;
		protected $file;

		public function __construct(PhotoInterface $photo, FileInterface $file)
		{
			$this->photo = $photo;
			$this->file = $file;
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
			if (Input::hasFile('file')) 
			{
				(object) $file = (object) Input::file('file');
				(int) $profileId = (int) Input::get('profile_id');
				(int) $organisationId = (int) Input::get('organisation_id');
				(array) $input = (array) Input::only('filename','file_ori_size', 'file_upload_size', 'image_primary');
				(array) $file_entry = $this->file->upload($file);
				(boolean) $isValid = true;
				(string) $errors = "";

				$input['src'] = $file_entry['src'];
				$input['path'] = $file_entry['path'];

				(int) $photo_id = (int) $this->photo->create($input, $profileId, $organisationId);
				
				return Response::json(array('photo_id' => $photo_id, 'errors'=>$errors, 'isValid' => $isValid, 'profile_id'=>$profileId, 'organisation_id'=>$organisationId));
			
			}
		}	

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			$photo = $this->photo->getById($id);
			return View::make('includes/image/_image', compact('photo'));	
		}

		public function update($id)
		{
			(array) $input = (array) Input::all();
			$this->photo->update($input, $id);
		}

		public function destroy($id)
		{
			$this->photo->destroy($id);
		}
	}
?>