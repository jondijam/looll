<?php  

	use Looll\Forms\ProfileForm;
	use Looll\Repo\User\UserInterface;
	use Looll\Repo\Profile\ProfileInterface; 
	use Looll\Repo\PhoneNumber\PhoneNumberInterface; 

	class UserProfileController extends BaseController
	{
		protected $profileForm;
		protected $profile;
		protected $user;
		protected $phoneNumber;

		public function __construct(ProfileForm $profileForm, ProfileInterface $profile, UserInterface $user, PhoneNumberInterface $phoneNumber)
		{
			$this->profileForm = $profileForm;
			$this->profile = $profile;
			$this->user = $user;
			$this->phoneNumber = $phoneNumber;

			$this->beforeFilter('currentUser', ['only' => ['store']]);
			$this->beforeFilter('csrf', ['only' => ['store']]);
		}

		public function store($username)
		{
			$user = Auth::user();
			$user_id = Auth::id();

			$country_id = Input::get('country_id');

			$homePhoneNumber = Input::get('home_phone_number');
			$homePhoneId = 0;

			$mobilePhoneNumber = Input::get('mobile_phone_number');
			$mobilePhoneId = 0;

			$buisnessPhoneNumber = Input::get('buisness_phone_number');
			$buisnessPhoneId = 0;

			$phone_numbers = array();
			$homes = array();
			$mobiles = array();
			$buisnesses = array();

			$input = Input::only('active', 'name', 'gender', 'birthdate', 'marital_status', 'description', 'announcement', 'email', 'address', 'zip', 'city', 'position', 'company'); 

			$validate = $this->profileForm->validate($input);

			if ($validate) 
			{
				$profile = $this->profile->create($input, $country_id, $user_id);

				$homePhones['phone_number'] = $homePhoneNumber;
				$homePhones['phone_type'] = 'telephone';

				$mobilePhones['phone_number'] = $mobilePhoneNumber;
				$mobilePhones['phone_type'] = 'mobile';

				$buisnessPhones['phone_number'] = $buisnessPhoneNumber;
				$buisnessPhones['phone_type'] = 'buisness';

				Elephant::emit('eventMsg', array('foo' => 'bar'));
				
				if($homePhoneNumber != "")
				{
					$homePhone = $this->profile->createPhoneNumber($homephones, $profile->id);
					$homePhoneId = $homePhone->id;
				}
				
				if ($mobilePhoneNumber != "") 
				{
					$mobilePhone = $this->profile->createPhoneNumber($mobilePhones, $profile->id);
					$mobilePhoneId = $mobile->id;
				}
				
				if ($buisnessPhoneNumber != "") 
				{
					$buisnessPhone = $this->profile->createPhoneNumber($buisnessPhones, $profile->id);
					$buisnessPhoneId = $buisness->id;
				}
			}

			$errors = $this->profileForm->getValidationErrors();

			return Response::json(array('errors'=>$errors, 'validate' => $validate, 'home_phone_id'=>$homePhoneId, 'mobile_phone_id'=>$mobilePhoneId, 'buisness_phone_id'=>$buisnessPhoneId));	
		}

		public function update($username, $profile_id)
		{
			(object) $profile = (object) $this->profile->getByUser($username);
			(array) $input = (array) Input::all();
			(array) $data = array();
			(array) $postData = array();

			(int) $homePhoneId = (int) $input['home_phone_id']; 
			(int) $mobilePhoneId = (int) $input['mobile_phone_id'];
			(int) $buisnessPhoneId =  (int) $input['buisness_phone_id'];
			(int) $countryId =  (int) $input['country_id'];

			(string) $homePhoneNumber = (string) $input['home_phone_number'];
			(string) $mobilePhoneNumber = (string) $input['mobile_phone_number'];
			(string) $buisnessPhoneNumber = (string) $input['buisness_phone_number'];

			$validate = $this->profileForm->validate($input);

			if (isset($profile) && ($validate)) 
			{
				$input = Input::only('active','name', 'gender', 'birthdate', 'marital_status', 'description', 'announcement', 'email', 'address', 'zip', 'city', 'country_id', 'position', 'company'); 
				$this->profile->update($input, $countryId, $profile->id);
			}

			if(($homePhoneNumber) && ($homePhoneId == 0))
	 		{
	 			$data['phone_number'] = $homePhoneNumber;
		 		$data['phone_type'] = "telephone";
		 		$homePhone = $this->profile->createPhoneNumber($data, $profile->id);
		 		$homePhoneId = $homePhone->id;
	 		}
		 	elseif ($homePhoneId > 0 && ($homePhoneNumber)) 
		 	{
		 		$this->phoneNumber->update($homePhoneNumber, $homePhoneId);
		 	}

		 	if(($mobilePhoneNumber) && ($mobilePhoneId == 0))
		 	{
		 		$data['phone_number'] = $mobilePhoneNumber;
			 	$data['phone_type'] = "mobile";

			 	$mobilePhone = $this->profile->createPhoneNumber($data, $profile->id);
			 	$mobilePhoneiId = $mobilePhone->id;
		 	}
		 	elseif ($mobilePhoneId > 0 && ($mobilePhoneNumber)) 
		 	{
		 		$this->phoneNumber->update($mobilePhoneNumber, $mobilePhoneId);
		 	}

		 	if(($buisnessPhoneNumber) && ($buisnessPhoneId == 0))
		 	{
		 		$data['phone_number'] = $buisnessPhoneNumber;
			 	$data['phone_type'] = "buisness";

			 	$buisnessPhone = $this->profile->createPhoneNumber($data, $profile->id);
			 	$buisnessPhoneId = $buisnessPhone->id;
		 	}
		 	elseif ($buisnessPhoneId > 0 && ($buisnessPhoneNumber)) 
		 	{
		 		$this->phoneNumber->update($buisnessPhoneNumber, $buisnessPhoneId);
		 	}

		 	$errors = $this->profileForm->getValidationErrors();

			return Response::json(array('errors'=>$errors, 'validate' => $validate, 'telephone_id'=>$homePhoneId, 'mobile_id'=>$mobilePhoneId, 'buisness_phone_id'=>$buisnessPhoneId));	
		}
	}
?>