<?php  
use Looll\Forms\OrganisationForm;
use Looll\Repo\Organisation\OrganisationInterface;
use Looll\Repo\Country\CountryInterface;

class OrganisationsController extends BaseController
{
	protected $organisation;
	protected $country;
	protected $organisationForm;

	public function __construct(OrganisationForm $organisationForm, OrganisationInterface $organisation, CountryInterface $country)
	{
		$this->organisationForm = $organisationForm;
		$this->organisation = $organisation;
		$this->country = $country;
		
		$this->beforeFilter('csrf', ['only' => ['update','store']]);
		$this->beforeFilter('auth', ['only' => ['update','create','edit','store']]);
		$this->beforeFilter('currentOrganisation', ['only' => ['edit', 'update', 'destroy']]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$images = array();
		$organisation = array();
		$countries = array();
		$country_id = 265;
		$member_type_id = 3;
		$id = 0;
		$description = "";
		$announcement = "";
		$private_announcement = "";
		$parents = $this->organisation->getParents($id, $member_type_id);	
		$countries = $this->country->getAll();
		
		$contacts = array();	

		$organisation_id = 0;
		$parent_id = 0;

		$url = array('organisation.store');
		$method = 'POST';

		if (count($parents)) 
		{
			$parents['0'] = Lang::get('looll.NoOrganisationSelected');
		}

		return View::make('organisation/create', compact(
			'images', 
			'organisation', 
			'description',
			'private_announcement',
			'announcement',
			'id',
			'organisation_id', 
			'member_type_id', 
			'url', 
			'method', 
			'countries', 
			'country_id', 
			'parent_id', 
			'parents', 
			'contacts'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		(array) $input = (array) Input::all();
		(int) $countryId = (int) $input['country_id'];
		(int) $parentId = (int) $input['parent_id'];
		(string) $username = (string) Auth::user()->username;
		(int) $userId = (int) Auth::id();	
		(bool) $ajax = (bool) Request::ajax();

		$input = Input::only('name','email','member_type_id','description','announcement', 'private_announcement', 'address', 'city', 'zip');
		
		(bool) $validate = (bool) $this->organisationForm->validate($input);
		(string) $redirect = (string) route('profile.edit', $username);

		if ($validate) 
		{
			$organisation = $this->organisation->create($input, $countryId, $parentId, $userId);

			if(!Request::ajax())
			{
				return Redirect::route('profile.edit', $username);
			}
		}

		$messages = array();
		(object) $errors = (object) $this->organisationForm->getValidationErrors();
		(array) $messages = (array) $errors->toArray();

		$messages['validate'] = $validate;
		$messages['redirect'] = $redirect;

		if(Request::ajax())
		{
			return Response::json($messages);
		}

		return Redirect::back()->withInput()->withErrors($errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		(array) $associated_organizations = array();
		(array) $images = array();
		(array) $contacts = array();
		(array) $associated_organizations = array();
		(int) $organisation_count = 0;
		(string) $name = "";
		(int) $user_id = 0;

		$organisation = $this->organisation->getById($id);
		if(isset($organisation))
		{
			$contacts = $this->organisation->getUsers($id, 2, 1, 1);
			$members = $this->organisation->getUsers($id, 3, 1, 1);

			$associated_organizations = $this->organisation->getChildren($id, 1);
			$images = $this->organisation->getPhotos($id);

			if (Auth::check()) 
			{
				$user = Auth::user();
				$user_id = Auth::id();

				$organisation_count = $this->organisation->getUsersCount($id, $user_id);
			}

			return View::make('organisation/show', compact(
				'organisation',
				'id',
				'user_id', 
				'name',
				'members', 
				'images', 
				'organisation_count', 
				'contacts', 
				'associated_organizations'
			));


		}

		return Redirect::home();	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		(array) $contacts = array();
		(array) $members = array();
		(array) $organisation = array();
		(array) $countries = array();
		(int)	$parent_id = 0;
		(string) $description = "";
		(string) $announcement = "";
		(string) $private_announcement = "";

		$organisation = $this->organisation->getById($id);

		if(!isset($organisation))
		{
			return Redirect::route('home');
		}

		$url = array('organisation.update', $id);
		$method = 'PUT';
		
		$parent_id = $organisation->parent_id;
		$countries = $this->country->getAll();

		$country_id = $organisation->country_id;
		$member_type_id = $organisation->member_type_id;
		$parents = $this->organisation->getParents($id);
		
		$accepted = 0;
		$roleId = 2;
		$active = 1;

		$contacts = $this->organisation->getUsers($id, $roleId, $accepted, $active);
		$contacts_counts = count($contacts);
		
		$roleId = 3;
		$users = $this->organisation->getUsers($id, $roleId, $accepted, $active);
		$children = $this->organisation->getChildren($id, 0);

		$description = $organisation->description;
		$private_announcement = $organisation->private_announcement;
		$announcement = $organisation->announcement;
		
		if (count($parents)) 
		{
			$parents['0'] = Lang::get('looll.NoOrganisationSelected');
		}
		
		return View::make('organisation/edit', compact(
			'organisation',
			'announcement',
			'private_announcement',
			'description',
			'parent_id',
			'children', 
			'contacts', 
			'contacts_counts', 
			'users',
			'parents' , 
			'url',
			'method', 
			'id',
			'countries', 
			'country_id', 
			'member_type_id'
		));		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		(array) $input = array();
		(array) $messages = array();
		(string) $username = (string) "";
		(int) $userId = (int) 0;
		(int) $parentId = (int) 0;
		(int) $countryId = (int) 0;
		(string) $redirect = "";
		(bool) $validate = (bool) false;

		if(!isset($input['parent_id'])) $input['parent_id'] = 0;
		if(!isset($input['country_id'])) $input['country_id'] = 265;

		$parentId = $input['parent_id'];
		$countryId = $input['country_id'];
		$username = Auth::user()->username;
		$userId = Auth::id();

		$input = Input::only(
			'name', 
			'email', 
			'member_type_id', 
			'description', 
			'announcement', 
			'private_announcement', 
			'address', 
			'city', 
			'zip');

		$validate = $this->organisationForm->excludeNameEmailId($id)->validate($input);

		if ($validate) 
		{
			$this->organisation->update($id, $input, $countryId, $parentId);

			if(!Request::ajax())
			{
				return Redirect::route('profile.edit', $username);
			}
		}

		(object) $errors = (object) $this->organisationForm->getValidationErrors();
		$redirect = route('profile.edit', $username);

		$messages = (array) $errors->toArray();
		$messages['validate'] = $validate;
		$messages['redirect'] = $redirect;

		if(Request::ajax())
		{
			return Response::json($messages);
		}
			
		return Redirect::back()->withInput()->withErrors($errors);

	}

	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		(object) $photos = $this->organisation->photos($id);

		if(!empty($photos))
		{
			foreach ($photos as $photo) 
			{
				if (File::exists($photo->path)) 
				{
					File::delete($photo->path);
				}
				
				$photo->delete();
			}

		}

		$this->organisation->destroy($id);
	}
}

?>