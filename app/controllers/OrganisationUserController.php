<?php  
use Looll\Repo\Organisation\OrganisationInterface;
use Looll\Repo\User\UserInterface;

class OrganisationUserController extends BaseController 
{
	protected $organisation;
	protected $user;

	public function __construct(OrganisationInterface $organisation, UserInterface $user)
	{
		$this->organisation = $organisation;
		$this->user = $user;

		//$this->beforeFilter('csrf', ['only' => ['update','store']]);
		$this->beforeFilter('auth', ['only' => ['update','create','edit','store', 'destroy']]);
	}

	public function index($organisationId)
	{
		(array) $temp = array();		
		(array) $data = array();
		$organisation = $this->organisation->getById($organisationId);
		(int) $contactsCount = 0;
		
		$q = Request::get('q');
		$contactsCount = Request::get('contacts_count');

		$results = Search::members($q, $organisation);
		$roles = array();

		foreach ($results as $result) 
		{
			if( $result->pivot->role_id == 2)
			{
				$temp[] = (int) $result->id; 
			}
		}
		
		return View::make('search/contacts', compact('results','organisationId','contactsCount','temp'));
	}

	public function store($organisationId, $userId)
	{
		if (Request::ajax()) 
		{
			$this->organisation->addUser($organisationId, $userId);	
			return 1;
		}
	}

	public function update($organisationId, $userId)
	{
		if(Request::ajax())
		{
			$input = Input::all();
			
			(bool) $accepted = (bool) $input['accept'];
			(int) $roleId = (int) $input['roleId'];
			(int) $active = (int) 1;
			(string) $usersable = (string) $input['usersable'];
			
			$user = $this->user->find($userId);
			$this->organisation->updateUser($organisationId, $userId, $roleId, $accepted);

			$contacts_count = $this->organisation->getActiveUsersCount($organisationId, $roleId, $accepted, $active);
			

			if ($usersable == "addContact") 
			{
				return View::make('includes/organisation/addContact', compact('organisationId', 'userId', 'user', 'contacts_count'));
			}
		}	
	}

	public function destroy($organisationId, $userId)
	{
		$user = Auth::user();
		
		$organisation = Organisation::find($organisationId);
		$organisation->users()->detach($userId);
	}
}
?>