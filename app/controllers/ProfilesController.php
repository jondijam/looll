<?php
use Looll\Repo\Profile\ProfileInterface as Profile; 
use Looll\Repo\Organisation\OrganisationInterface as Organisation;
use Looll\Repo\Country\CountryInterface as Country;
use Looll\Forms\ProfileForm as Form;
use Looll\Repo\PhoneNumber\PhoneNumberInterface as PhoneNumber;


class ProfilesController extends BaseController
{
	protected $profile;
	protected $organisation;
	protected $country;
	protected $form;

	public function __construct(Profile $profile, Country $country, Organisation $organisation, Form $form, PhoneNumber $phoneNumber)
	{
		$this->profile = $profile;
		$this->country = $country;
		$this->organisation = $organisation;
		$this->form = $form;
		$this->phoneNumber = $phoneNumber;
	}

	public function show($username)
	{
		(object) $profile = (object) $this->profile->getByUser($username);
		
		if(!isset($profile)) return View::make('profile.index');

		(int) $mobilePhoneId = 0;
		(int) $homePhoneId = 0;
		(int) $buisnessPhoneId = 0;
		(int) $page = Input::get('page', 1);
		(array) $photos = array();
		(string) $email = str_replace('@', ' [at] ', $profile->email);
		(string) $homePhoneNumber = "";
		(string) $buisnessPhoneNumber = "";
		(string) $mobilePhoneNumber = "";
		(string) $phoneType = "";

		$phoneType = "mobile";
		(object) $mobilePhone = $this->profile->getPhoneNumber($profile->id, $phoneType);

		$phoneType = "telephone";
		(object) $homePhone = $this->profile->getPhoneNumber($profile->id, $phoneType);

		$phoneType = "buisnes";
		(object) $buisnessPhone = $this->profile->getPhoneNumber($profile->id, $phoneType);
		
		(object) $organisations = (object) $this->organisation->getByUser($username);
		
        // Candidate for config item
        $perPage = 3;
        $pagiData = $this->organisation->byPage($page, $perPage, $username);
        $announcements = Paginator::make($pagiData->items, $pagiData->totalItems, $perPage);

		(array) $photos = (array) $this->profile->photos($profile->id);

		if (count($mobilePhone)) 
		{
			$mobilePhoneNumber = $mobilePhone['phone_number'];	
 			$mobilePhoneId = $mobilePhone['id'];
		}

		if(count($homePhone))
		{
			$homePhoneNumber = $homePhone['phone_number'];
			$homePhoneId = $homePhone['id'];
		}

		if (count($buisnessPhone)) 
		{
			$buisnessPhoneId = $buisnessPhone['id'];
			$buisnessPhoneNumber = $buisnessPhone['phone_number'];
		}

		return View::make('profile.show', compact(
				'photos', 
				'email', 
				'organisations',
				'announcements', 
				'profile', 
				'profileId', 
				'username', 
				'homePhone', 
				'homePhoneId',
				'homePhoneNumber',
				'mobilePhoneId', 
				'mobilePhone', 
				'mobilePhoneNumber',
				'buisnessPhone', 
				'buisnessPhoneId',
				'buisnessPhoneNumber'));	
	}

	public function edit($username)
	{
		(object) $profile = $this->profile->getByUser($username);
		(int) $profileId = 0;
		(int) $active = 1;
		(string) $announcement = "";
		(array) $countries = (array) $this->country->getAll();
		(int) $marital_status = 0;
		(int) $country_id = 265;
		(array) $mobilePhone = array();
		(array) $homePhone = array();
		(array) $buisnessPhone = array();
		(int) $mobilePhoneId = 0;
 		(int) $homePhoneId = 0;
 		(int) $buisnessPhoneId = 0;
 		(string) $homePhoneNumber = "";
 		(string) $buisnessPhoneNumber = "";
 		(string) $mobilePhoneNumber = "";
 		(object) $organisations = (object) $this->organisation->getByUser($username);
		(object) $user = Auth::user();
		(array) $parents = array();

		(array) $photos = (array) $this->profile->photos($profile->id);
 		(array) $url = array('users.profiles.update', $username, $profile->id);
 		(int) $page = Input::get('page', 1);

 		(int) $perPage = 3;

        $pagiData = $this->organisation->byPage($page, $perPage, $username);
        $announcementsByOrganisations = Paginator::make($pagiData->items, $pagiData->totalItems, $perPage);

 		$mobilePhone = $this->profile->getPhoneNumber($profile->id, 'mobile');
 		$homePhone = $this->profile->getPhoneNumber($profile->id, 'telephone');
 		$buisnessPhone = $this->profile->getPhoneNumber($profile->id, 'buisness');

 		if (count($mobilePhone)) 
 		{
 			$mobileNumber = $mobilePhone['phone_number'];
 			$mobilePhoneId = $mobilePhone['id'];
 		}

 		if(count($homePhone))
		{
			$homePhoneNumber = $homePhone['phone_number'];
			$homePhoneId = $homePhone['id'];
		}

		if (count($buisnessPhone)) 
		{
			$buisnessPhoneId = $buisnessPhone['id'];
			$buisnessPhoneNumber = $buisnessPhone['phone_number'];
		}

 		(int) $profileId = (int) $profile->id;
 		(int) $active = $profile->active;
 			  $announcement = $profile->announcement;
 			  $marital_status = $profile->marital_status;
 			  $country_id = $profile->country_id;

		return View::make('profile/edit', compact(
				'user', 
				'profile',
				'active',
				'announcement',
				'announcementsByOrganisations',
				'marital_status', 
				'profileId',
				'country_id', 
				'countries', 
				'organisations', 
				'children',
				'photos',
				'user',
				'username', 
				'homePhone', 
				'homePhoneNumber', 
				'homePhoneId', 
				'mobilePhone',
				'mobilePhoneNumber', 
				'mobilePhoneId', 
				'buisnessPhone',
				'buisnessPhoneId', 
				'buisnessPhoneNumber'));	

	}

	public function destroy($username)
	{

	}
}

?>