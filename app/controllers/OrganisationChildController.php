<?php

use Looll\Repo\Organisation\OrganisationInterface;

class OrganisationChildController extends \BaseController {

	protected $organisation;
	
	public function __construct(OrganisationInterface $organisation)
	{
		$this->organisation = $organisation;
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /organisationsusers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($organisationId, $childId)
	{
		$input = Input::all();
		$this->organisation->updateChild($childId, $input);	
	}
}