<?php  

	/**
	* 
	*/
	use Looll\Repo\Organisation\OrganisationInterface;

	class SearchController extends \BaseController
	{
		protected $organisation;

		public function __construct(OrganisationInterface $organisation)
		{
			$this->organisation = $organisation;
		}

		public function index()
		{

			$results = array();
			//arse_str(urldecode($_GET['q']), $_GET);
			$query = Request::get('q');

			$results = Search::looll($query);
			$data['results'] = $results;
			
			//dd($results);
			$paginator = Paginator::make($results->toArray(), $results->count(), 10);

			return View::make('search/results', $data);
		}
	}

?>