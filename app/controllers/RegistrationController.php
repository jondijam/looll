<?php
use Looll\Forms\RegistrationForm;
use Looll\Mailers\UserMailer as Mailer;
use Looll\Repo\User\UserInterface;
use Looll\Repo\Profile\ProfileInterface;

class RegistrationController extends \BaseController {

	private $registrationForm;
	protected $user;
	protected $profile;
	protected $mailer;

	function __construct(RegistrationForm $registrationForm, UserInterface $user, ProfileInterface $profile, Mailer $mailer )
	{
		$this->registrationForm = $registrationForm;
		$this->user = $user;
		$this->profile = $profile;
		$this->mailer = $mailer;

		$this->beforeFilter('csrf', ['only' => ['store']]);
		$this->beforeFilter('guest', ['only' => ['store', 'create']]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('registration/create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$input = Input::all();
		$name = array();
		$validate = $this->registrationForm->validate($input);

		if ($validate) 
		{
			$name['name'] = Input::get('name');

			$input = Input::only('username', 'email', 'password','password_confirmation', 'confirmation_code');
			$user = $this->user->create($input);

			$profile = $this->profile->create($name, 265, $user->id);
			
			$this->mailer->welcome($user);
		}
		
		return Redirect::route('confirm')->with($input);
	}

	public function confirmed($username, $code)
	{
		$data = array();
		$data['username'] = $username;
		$data['code'] = $code;

		if (!$username || !$code) 
		{
			throw new InvalidConfirmationCodeException;
		}

		$this->user->confirm($data);

		return Redirect::route('login');
	}

	public function confirm()
	{
		return View::make('registration/confirm');
	}

}
