<?php namespace Admin;
use Config;
use View;

class SessionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin/sessions
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/sessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		echo 'dd';
		//$client = new \Google\Client(); 
		$google_client = new \Google_Client();
		$google_client->setApplicationName("looll");
	    $google_client->setClientId(Config::get('drive.client_id') );
	    $google_client->setClientSecret(Config::get('drive.client_secret'));
	    $google_client->setDeveloperKey(Config::get('drive.api_key'));
	    $url = route('admin.sessions.store');
	    $google_client->setRedirectUri($url);
	    $google_client->setScopes(array('https://www.googleapis.com/auth/drive'));
	    $authUrl = $google_client->createAuthUrl();
	   	
	   	

	    return View::make('admin/sessions/create', [ 'url' => $authUrl ]);
		/**/
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/sessions
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}

	/**
	 * Display the specified resource.
	 * GET /admin/sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/sessions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}