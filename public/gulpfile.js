var gulp = require('gulp');
var minifycss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var sass = require()

gulp.task('css', function(){
	return gulp.src('css/looll.css')
	.pipe(autoprefixer("last 15 version"))
	.pipe(minifycss())
	.pipe(gulp.dest('css/min'))
	.pipe(notify({message: 'All done, master!!'}));
});

gulp.task('default', function()
{
	gulp.run('css');
	gulp.watch('css/*.css', function()
	{
		gulp.run('css');
	});
});