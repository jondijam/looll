$(document).ready(function(){
	var organisation_id = 0;
	var user_id = 0;
	var form;
	var method = "";
	var url = "";
	var update_url = "";

	$('form[data-remote]').on('submit', function(e){
		e.preventDefault();
		form = $(this); 
		method = form.find('input[name=_method]').val() || 'POST';
		url = form.prop('action');


		$('.alert').addClass('hidden');

		$.ajax({
			type: method,
            url: url,
            data: form.serialize(),
            success: function(data) {
            	if (data.validate === true) 
            	{
            		$('#createorganisation').removeClass('hidden');
					$('.alert').addClass('hidden');
					
					$('#save').attr('data-profile-id', data.profile_id);
					$('#droparea').attr('data-profile-id', data.profile_id);

					$('#success').removeClass('hidden');
					$('#save').addClass('hidden');

					$('input[name="telephone_id"]').val(data.telephone_id);
					$('input[name="mobile_id"]').val(data.mobile_id);
					$('input[name="buisness_phone_id"]').val(data.buisness_phone_id);

					setTimeout(function() 
					{
						    //$('#mydiv').fadeOut('fast');
						$('#save').removeClass('hidden');
						$('#success').addClass('hidden');
					}, 2000);

            	}
            	else
            	{
            		$('.message-name').removeClass('hidden');
            		$('.message-name').text(data.errors.name);
            	}
            }
		});
	});

	$('.deleteGroup').on('click', function(e){
		e.preventDefault();

		organisation_id = $(this).attr('data-organisation-id');
		user_id = $(this).attr('data-user-id');

		var c = confirm("Do you really want to delete this group ?");

		if (c == true) 
		{
			$('#'+organisation_id).remove();	
			$.ajax({
				type:'DELETE',
				url:'/organisation/'+organisation_id+'/',
				susccess: function()
				{
					
				} 
			});
		};
	});

	$('.leaveGroup').on('click', function(e){
		e.preventDefault();

		url = $(this).attr('href');
		organisation_id = $(this).attr('data-organisation-id');

		var cl = confirm("Are you sure you want to leave the group");
		
		if (cl == true) 
		{
			$('#'+organisation_id).remove();
			
			$.ajax({
				type:'DELETE',
				url:url,
				susccess: function()
				{
					
				} 
				});
		};
	});

	
});