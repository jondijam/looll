(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var organisation = require('./organisation')();
},{"./organisation":2}],2:[function(require,module,exports){
module.exports = function()
{
	console.log("works great");
}

/*
$(document).ready(function()
{
	var app = {};

	app.organisationId = 0;
	app.userId = 0;
	app.typingTimer;
	app.doneTypingInterval = 1000;
	app.contactsCount = 0;
	app.roleId = 3;
	app.accept = 0;
	app.usersable = "acceptUser";
	app.validate = 1;
	app.token = "";
	app.method = "POST";

	app.connect = function (e)
	{
		e.preventDefault();
		app.organisationId = $(this).attr('data-organisation-id');
		app.userId = $(this).attr('data-user-id');
		app.token = $(this).attr('data-token');

		$.ajax({
			type: app.method,
			data: {"token":app.token},
			url: '/organisation/'+app.organisationId+'/user/'+app.userId+'/connect',
			success: function(data)
			{
				$('#connect').addClass('hidden');
			}
		});
	};

	app.acceptUser = function(e)
	{
		e.preventDefault();
		app.userId = $(this).attr('data-user-id');
		app.organisationId = $(this).attr('data-organisation-id');
		app.token = $(this).attr('data-token');
		app.accept = 1;
		app.method = "PUT";

		$.ajax({
			type:app.method,
			url:'/organisations/'+organisation_id+'/users/'+user_id+'/',
			data: { 'roleId' : app.roleId , 'accept' : app.accept, 'usersable':app.usersable, 'token':app.token },
			success: function(data)
			{
				if(data.accept == 0)
				{
					$('#declineUser'+app.userId).addClass('hidden');
					$('#acceptUser'+app.userId).removeClass('hidden');
				}

				if(data.accept == 1)
				{
					$('#declineUser'+app.userId).removeClass('hidden');
					$('#acceptUser'+app.userId).addClass('hidden');
				}

			
			}
		});
	}

	app.declineUser = function(e)
	{
		e.preventDefault();
		app.userId = $(this).attr('data-user-id');
		app.organisationId = $(this).attr('data-organisation-id');
		app.token = $(this).attr('data-token');
		app.accept = 0;
		app.method = "PUT";

		$.ajax({
			type:app.method,
			url:'/organisations/'+organisation_id+'/users/'+user_id+'/',
			data: { 'roleId' : app.roleId , 'accept' : app.accept, 'usersable':app.usersable, 'token':app.token },
			success: function(data)
			{
				if(data.accept == 0)
				{
					$('#declineUser'+app.userId).addClass('hidden');
					$('#acceptUser'+app.userId).removeClass('hidden');
				}

				if(data.accept == 1)
				{
					$('#declineUser'+app.userId).removeClass('hidden');
					$('#acceptUser'+app.userId).addClass('hidden');
				}

			
			}
		});
	}

	app.removeUser = function(e)
	{
		e.preventDefault();
		app.userId = $(this).attr('data-user-id');
		app.organisationId = $(this).attr('data-organisation-id');
		app.method = "DELETE";

		$.ajax({
				type:app.method,
				url:'/organisations/'+organisation_id+'/users/'+user_id+'/',
				data:'',
				success: function(data)
				{
					$("#deleteUser"+user_id).remove();
				}
		});
	}

	app.formSubmit = function(e)
	{
		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name=_method]').val() || 'POST';
		var url = form.prop('action');

		$.ajax({
			type:method,
			url:url,
			data:form.serialize(),
			enctype: 'multipart/form-data',
			success: function(data)
			{	
				if (data.validate == app.validate) 
				{
					window.location = data.redirect;
				}

				if (typeof data.name !== 'undefined') 
				{
					$('.name').removeClass('hidden');
					$('.name').text(data.name);
				}

				if (typeof data.email !== 'undefined') 
				{
					$('.email').removeClass('hidden');
					$('.email').text(data.email);
				}
			}
		});
	}

	app.addContact = function(e)
	{
		e.preventDefault();
		app.userId = $(this).attr('data-user-id');
		app.organisationId = $(this).attr('data-organisation-id');
		app.contactsCount = $(this).attr('contacts_count');
		app.usersable = 'addContact';


		if ($('input[name="addContact"]').prop("checked"))
		{
			app.roleId = 2;
			app.accept = 1;
		}
		else
		{
			app.roleId = 2;
			app.accept = 0;
		}

		$.ajax({
				type:'PUT',
				url:'/organisations/'+organisation_id+'/users/'+user_id+'/',
				data: { 'role_id' : role_id , 'accept' : accept, 'usersable':usersable },
				success: function(data)
				{
					if (role_id == 2) 
					{
						$('.contacts-tbody').prepend(data);
						
						contacts_count++;
						
						$(this).attr('contacts_count', contacts_count);

						if (contacts_count == 2) 
						{
							$('.result').addClass('hidden');
							$('#contact_search').addClass('hidden');
						};
					}
					else
					{
						$('#deleteContact'+user_id).remove();
						contacts_count--;
						$(this).attr('contacts_count', contacts_count);

						if (contacts_count == 1) 
						{
							$('.result').removeClass('hidden');
							$('#contact_search').removeClass('hidden');
						};

					}
				}
		});
	}

	app.removeContact = function(e)
	{
		e.preventDefault();
		app.userId = $(this).attr('data-user-id');
		app.organisationId = $(this).attr('data-organisation-id');
		app.token = $(this).attr("data-token");

		app.roleId = 2;
		app.accept = 0;
		app.usersable = "addContact";
		app.method = 'PUT';


		$.ajax({
			type:app.method,
			url:'/organisations/'+app.organisationId+'/users/'+app.userId+'/',
			data:{'role_id' : app.roleId , 'accept' : accept, 'usersable':usersable, 'token':app.token },
			success: function(data)
			{
				if (app.roleId == 2 && app.accept === 1) 
				{
					$('.contacts-tbody').html('');
					$('.contacts-tbody').prepend(data);
					app.contactsCount++;
					$(this).attr('contactsCount', app.contactsCount);

					if (app.contactsCount == 2) 
					{
						$('.result').addClass('hidden');
						$('#contact_search').addClass('hidden');
					};
				}
				else if(app.roleId == 2 && app.accept === 0)
				{
					$('#deleteContact'+user_id).remove();
					
					app.contactsCount--;
					$(this).attr('contacts_count', app.contactsCount);

					if (app.contactsCount < 2) 
					{
						$('.result').removeClass('hidden');
						$('#contact_search').removeClass('hidden');
					};

				}
			}
		});
	}

	app.acceptGroup = function(e)
	{
		e.preventDefault();
		var url = $(this).data('url');
		var method = "PATCH";

		$(this).addClass('hidden');
		$('.declineGroup').removeClass('hidden');

		$.ajax({
			type:method,
			url: url,
			data:'parent_acceptance=1',
			success: function(data)
			{
				
			}
		});
	}

	app.declineGroup = function(e)
	{
		e.preventDefault();
		var url = $(this).data('url');
		var method = "PATCH";


		$(this).addClass('hidden');
		$('.acceptGroup').removeClass('hidden');

		$.ajax({
			type:method,
			url: url,
			data:'parent_acceptance=0',
			success: function(data)
			{

			}
		});
	}

	app.keyPress = function(e)
	{
		clearTimeout(app.typingTimer);
	    if ($('#myInput').val) 
	    {
	        app.typingTimer = setTimeout(app.doneTyping, app.doneTypingInterval);
	    }
	}

	app.doneTyping = function ()
	{
		var query = $('#contact_search').val();

    	app.organisationId = $('#contact_search').attr('data-organisation-id');
   	 	app.contactsCount = $('#contact_search').attr('data-contacts-count');
   	 	app.method = "GET";

   	 	$.ajax({
   	 		type:app.method,
   	 		url:'/organisations/'+organisation_id+'/users/',
   	 		data: 'q='+query+'&contacts_count='+app.contactsCount,
   	 		success: function(data)
   	 		{
   	 			$('.result').html(data);
   	 		}
   	 	});
	} 

	$('form[data-remote]').on('submit', app.formSubmit);
	$('#connect').on('click', app.connect);
	$('.acceptUser').on('click', app.acceptUser);
	$('.declineUser').on('click', app.declineUser);
	$('.removeUser').on('click', app.removeUser );
	$('body').on('change', '.addContact', app.addContact);
	$('body').on('click', '.removeContact', app.removeContact);
	$('#contact_search').keyup(app.keyPress);
	$('.acceptGroup').click(app.acceptGroup);
	$('.declineGroup').click(app.declineGroup);
});
*/
},{}]},{},[1])