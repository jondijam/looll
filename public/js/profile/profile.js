module.exports = function()
{
	var profile = {};

	profile.organisationId = 0;
	profile.userId = 0;
	profile.form;
	profile.method = "";
	profile.url = "";

	profile.save = function(e)
	{
		e.preventDefault();
		profile.form = $(this); 
		profile.method = profile.form.find('input[name=_method]').val() || 'POST';
		profile.url = profile.form.prop('action');

		$('.alert').addClass('hidden');

		$.ajax({
			type: profile.method,
            url: profile.url,
            data: profile.form.serialize(),
            success: function(data) 
            {
            	if (data.validate === true) 
            	{
            		$('#createorganisation').removeClass('hidden');
					$('.alert').addClass('hidden');
					
					$('#save').attr('data-profile-id', data.profile_id);
					$('#droparea').attr('data-profile-id', data.profile_id);

					$('#success').removeClass('hidden');
					$('#save').addClass('hidden');

					$('input[name="telephone_id"]').val(data.telephone_id);
					$('input[name="mobile_id"]').val(data.mobile_id);
					$('input[name="buisness_phone_id"]').val(data.buisness_phone_id);

					setTimeout(function() 
					{
						    //$('#mydiv').fadeOut('fast');
						$('#save').removeClass('hidden');
						$('#success').addClass('hidden');
					}, 2000);

            	}
            	else
            	{
            		$('.message-name').removeClass('hidden');
            		$('.message-name').text(data.errors.name);
            	}
            }
		});
	}

	profile.deleteGroup = function(e)
	{
		e.preventDefault();
		profile.organisationId = $(this).attr('data-organisation-id');
		profile.userId = $(this).attr('data-user-id');

		var c = confirm("Do you really want to delete this group ?");

		if (c == true) 
		{
			$('#'+profile.organisationId).remove();	
			$.ajax({
				type:'DELETE',
				url:'/organisation/'+profile.organisationId+'/',
				susccess: function()
				{
					
				} 
			});
		};
	}

	profile.leaveGroup = function(e)
	{
		e.preventDefault();
		profile.url = $(this).attr('href');
		profile.organisationId = $(this).attr('data-organisation-id');

		var cl = confirm("Are you sure you want to leave the group");
		if (cl == true) 
		{
			$('#'+profile.organisationId).remove();
			
			$.ajax({
				type:'DELETE',
				url:profile.url,
				susccess: function()
				{
					
				} 
				});
		};
	}

	$(document).ready(function(){
		$('form[data-remote]').on('submit', profile.save);
		$('.deleteGroup').on('click', profile.deleteGroup);
		$('.leaveGroup').on('click', profile.leaveGroup);
	});
}