var organisation_id = 0;
var user_id = 0;
var name = "";
var email = "";
var description = "";
var announcement = "";
var address = "";
var city = "";
var zip = "";
var country_id = 0;
var parent_id = 0;
var images = new Array();
var member_status = 0;
var contacts_count = 0;

$(document).ready(function()
{
	$('#save').click(function(e)
	{	

		e.preventDefault();
		console.log('organisation works');

		organisation_id = $(this).attr('data-organisation-id');

		name = $('#name').val();
		description = $('#description').val()
		announcement = $('#announcement').val();
		email = $('#email').val();
		address = $('#address').val();
		city = $('#city').val();
		zip = $('#zip').val();
		country_id = $('#country').val();
		parent_id = $('#organisation_parent').val();
		member_status = $('#member_status').val();

		$.post('/organisation/save/'+organisation_id, {
			name:name, 
			address:address,
			city:city, 
			zip:zip,  
			email:email,
			description:description, 
			announcement:announcement, 
			country_id:country_id, 
			parent_id:parent_id,
			member_status:member_status, 
		},
		function(data)
		{
			if (data.isValid) 
				{
					window.location.href = "/edit";
					$('.alert').addClass('hidden');
				}
				else
				{
					$('.alert').removeClass('hidden');
					$('.error').html(data.errors);
				}
		});
		
		/*
		$.each($(".image"), function(){
			images.push($(this).attr('data-image-id'));
		})*/

		/*$.post('/organisation/save/'+organisation_id, {name:name, members_status:members_status, images:images, description:description, announcement:announcement, address:address, city:city, zip:zip, country_id:country_id, parent_id:parent_id}, function(data){
				
				if (data.isValid) 
				{
					window.location.href = "/organisation";
					$('.alert').addClass('hidden');
				}
				else
				{
					$('.alert').removeClass('hidden');
					$('.error').html(data);
				}

		});*/

	});

	$('.accept').click(function(e){
		e.preventDefault();
		
		organisation_id = $(this).attr('data-organisation-id');

		$.post('/organisation/accept/'+organisation_id,{}, function(data){});
		$('#decline'+organisation_id).removeClass('hidden');
		$(this).addClass('hidden');
	});

	$('.decline').click(function(e){
		e.preventDefault();
		organisation_id = $(this).attr('data-organisation-id');

		$.post('/organisation/decline/'+organisation_id,{}, function(data){
			$('#decline'+organisation_id).addClass('hidden');
			$('#accept'+organisation_id).removeClass('hidden');
			
		});
	});


	$('.acceptUser').click(function(e){
		user_id = $(this).attr('data-user-id');
		organisation_id = $(this).attr('data-organisation-id');

		$.post('/organisation/user/accept/'+user_id, {organisation_id:organisation_id}, function(e){
			$('#declineUser'+user_id).removeClass('hidden');
			$('#acceptUser'+user_id).addClass('hidden');
		});
	});

	$('.declineUser').click(function(e){
		user_id = $(this).attr('data-user-id');
		organisation_id = $(this).attr('data-organisation-id');

		$.post('/organisation/user/decline/'+user_id, {organisation_id:organisation_id}, function(e){
			$('#declineUser'+user_id).addClass('hidden');
			$('#acceptUser'+user_id).removeClass('hidden');

		});
	});

	$('.deleteUser').click(function(e){
		organisation_id = $(this).attr('data-organisation-id');
		user_id = $(this).attr('data-user-id');

		$.post('/organisation/user/delete/'+user_id, {organisation_id:organisation_id}, function(e){
			$('#deleteUser'+user_id).remove();
		});
	});

	$('#connect').click(function(e){
		e.preventDefault();
		organisation_id = $(this).attr('data-organisation-id');
		user_id = $(this).attr('data-user-id');

		$.post('/organisation/connect/user/'+user_id, {organisation_id:organisation_id}, function(data){
		});

		$(this).addClass('hidden');	
	});	

	$('.disconnect').click(function(e){
		e.preventDefault();

		organisation_id = $(this).attr('data-organisation-id');
		user_id = $(this).attr('data-user-id');

		$.post('/organisation/disconnect/user/'+user_id, {organisation_id:organisation_id}, function(data){
		});

	});

});

var the_search_term = "";

function removeContact(id)
{
	

	organisation_id = $('#removeContact'+id).attr('data-organisation-id');
    user_id = $('#removeContact'+id).attr('data-user-id');
    contacts_count = $('#removeContact'+id).attr('data-contacts-count');
    contacts_count = contacts_count = parseInt(contacts_count) - 1;

    if (contacts_count < 2) 
	{
		$('#contact_search').removeClass('hidden');
	};

	$('#contact_search').val('');
	$('#addContact'+id).attr("checked", false);

	$('.addContact').attr('data-contacts-count', contacts_count);
	$('.removeContact').attr('data-contacts-count', contacts_count);
	$('#contact_search').attr('data-contacts-count', contacts_count);

	$('#deleteContact'+user_id).remove();	

    $.post('/organisation/contact/delete/'+user_id, {organisation_id:organisation_id}, function(data){
		
	});
}

function searchContacts()
{
	the_search_term = $('#contact_search').val();
    organisation_id = $('#contact_search').attr('data-organisation-id');
    contacts_count = $('#contact_search').attr('data-contacts-count');
    
    $.post('/searchContacts', {query:the_search_term, organisation_id:organisation_id, contacts_count:contacts_count}, function(data)
    {
        $('.result').html(data);
    });
}



function addContact(id)
{
	
	user_id = id;
	organisation_id = $('#addContact'+user_id).attr('data-organisation-id');
	contacts_count = $('#addContact'+user_id).attr('data-contacts-count');



	if ($('#addContact'+user_id).is(':checked')) 
	{
		$('#addContact'+id).attr('checked', 'checked');

		contacts_count = parseInt(contacts_count) + 1;

		if (contacts_count == 1)
		{
			$('.empty').addClass('hidden');
		}
		
		console.log(contacts_count);
		
		if (contacts_count == 2) 
		{
			$('#contact_search').addClass('hidden');
			$('.result').html('');
		};


		$.post('/addContact',{user_id:user_id, organisation_id:organisation_id, contacts_count:contacts_count}, function(data){
			$('.contacts-tbody').prepend(data);
		});

	}
	else
	{
		$('#addContact'+id).prop("checked", false);

		$('#deleteContact'+user_id).remove();

		$.post('/organisation/contact/delete/'+user_id, {organisation_id:organisation_id}, function(data){
			
		});

		contacts_count = parseInt(contacts_count) - 1;

		if (contacts_count < 2) 
		{
			$('#contact_search').removeClass('hidden');
		};

	}

	

	$('.addContact').attr('data-contacts-count', contacts_count);
	$('.removeContact').attr('data-contacts-count', contacts_count);
	$('#contact_search').attr('data-contacts-count', contacts_count);
}





