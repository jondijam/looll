var imageApp = {};

imageApp.imageCount = 0;
imageApp.imageId = 0;
imageApp.primaryImage = 0;
imageApp.primary = 0;
imageApp.organisationId = 0;
imageApp.profileId = 0;
imageApp.imgWidth = 180;
imageApp.imgHeight = 180;
imageApp.zindex = 0;
imageApp.dropzone = $('#droparea');
imageApp.uploadBtn = $('#uploadbtn');
imageApp.defaultUploadBtn = $('#upload');
imageApp.primary = 0;
imageApp.no_primary_images = new Array();
imageApp.method = 'PUT';
imageApp.item = {};
imageApp.count = 0;

imageApp.counting = function()
{
	imageApp.imageCount++;
};

imageApp.dragover = function()
{
	//add hover class when drag over
	imageApp.dropzone.addClass('hover');
	return false;
};

imageApp.dragleave = function()
{
	//remove hover class when drag out
	imageApp.dropzone.removeClass('hover');
	return false;
};

imageApp.drop = function(e)
{
	e.stopPropagation();
	e.preventDefault();
	imageApp.dropzone.removeClass('hover');

	var files = e.originalEvent.dataTransfer.files;
	imageApp.processFiles(files);
	return false;
};

imageApp.click = function(e)
{
	e.stopPropagation();
	e.preventDefault();
	
	//trigger default file upload button
	imageApp.defaultUploadBtn.click();
};

imageApp.change = function()
{
	//retrieve selected uploaded files data
	var files = $(this)[0].files;
	imageApp.processFiles(files);
	
	return false;
};

imageApp.primaryClick = function()
{
	imageApp.primary = $(this).attr('data-primary');
	imageApp.imageId = $(this).attr('data-image-id');
	imageApp.profileId = $(this).attr('data-profile-id');
	imageApp.organisationId = $(this).attr('data-organisation-id');

	if (imageApp.primary) 
	{
		$('.primary-image').removeClass('fa-star');
		$('.primary-image').addClass('fa-star-o');
		$('.primary-image').attr('data-primary', 0);
		$(this).attr('data-primary', 1);

		$(this).removeClass('fa');
		$(this).removeClass('fa-star-o');
		$(this).addClass('fa');
		$(this).addClass('fa-star');

		$.ajax(
		{
			type: imageApp.method,
			url:'/photo/'+imageApp.imageId,
			data: "profile_id="+imageApp.profileId+"&organisation_id="+imageApp.organisationId+"",
			success: function(data) 
			{

			}
		});
	};
};

imageApp.remove = function()
{
	var r = confirm("Are you sure you want to delete!");
	if (r == true) 
	{
		imageApp.imageId = $(this).attr('data-image-id');

		$.ajax({
			type:'DELETE',
			url:'/photo/'+imageApp.imageId,
			data:"",
			success: function(data)
			{
				$('#imageholder'+imageApp.imageId).remove();
				$('.form-images').removeClass('hidden');
			
				imageApp.imageCount--;
			}
		});
	} 
};

/***************************** 
		internal functions
*****************************/	
//Bytes to KiloBytes conversion

imageApp.convertToKBytes = function(number)
{
	return (number / 1024).toFixed(1);
};

imageApp.compareWidthHeight = function(width, height)
{
	var diff = [];
	if(width > height) 
	{
		diff[0] = width - height;
		diff[1] = 0;
	} 
	else 
	{
		diff[0] = 0;
		diff[1] = height - width;
	}
	return diff;
};

imageApp.dataURItoBlob = function(dataURI)
{
	// convert base64 to raw binary data held in a string
	// doesn't handle URLEncoded DataURIs
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(dataURI.split(',')[1]);
	else
		byteString = unescape(dataURI.split(',')[1]);
	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
	// write the bytes of the string to an ArrayBuffer
	var ab = new ArrayBuffer(byteString.length);
	var ia = new Uint8Array(ab);
	for (var i = 0; i < byteString.length; i++) 
	{
		ia[i] = byteString.charCodeAt(i);
	}
	
	//Passing an ArrayBuffer to the Blob constructor appears to be deprecated, 
	//so convert ArrayBuffer to DataView
	var dataView = new DataView(ab);
	var blob = new Blob([dataView], {type: mimeString});
	return blob;
};

/***************************** 
	Process FileList 
*****************************/
imageApp.processFiles = function (files)
{
	if(files && typeof FileReader !== "undefined") 
	{
		
		for(var i = 0; i < files.length; i++)
		{
			if (imageApp.imageCount === 0)
			{
				imageApp.primaryImage = 1;
			} 
			
			if (imageApp.imageCount < 5) 
			{
				imageApp.readFile(files[i], imageApp.primaryImage);
			};
			
			if (imageApp.imageCount === 4) 
			{
				$('.form-images').addClass('hidden');
			}
			
			imageApp.imageCount++;
		}
	}
};

/***************************** 
	Read the File Object
*****************************/
imageApp.readFile = function (file, primaryImage)
{

	if( (/image/i).test(file.type) ) 
	{
		//define FileReader object
		var reader = new FileReader();
				
		//init reader onload event handlers
		reader.onload = function(e) 
		{	
			var image = $('<img/>').load(function() 
			{
				//when image fully loaded
				var newimageurl = imageApp.getCanvasImage(this);
				imageApp.uploadToServer(file, imageApp.dataURItoBlob(newimageurl), newimageurl, primaryImage);
						
			})
			.attr('src', e.target.result);	
		};
				
				//begin reader read operation
		reader.readAsDataURL(file);		
		$('#err').text('');
	} 
	else 
	{
		//some message for wrong file format
		$('#err').text('*Selected file format not supported!');
	}
};

/***************************** 
	Draw Image Preview
*****************************/
imageApp.createPreview = function(file, newURL, imgId, primaryImage) 
{	
	//populate jQuery Template binding object
	var imageObj = {};	
	imageObj.file_path = newURL;
	imageObj.image_id = imgId;
	imageObj.image_primary = primaryImage;
	imageObj.file_name = file.name.substr(0, file.name.lastIndexOf('.')); //subtract file extension
	imageObj.file_ori_size = imageApp.convertToKBytes(file.size);
	imageObj.file_upload_size = imageApp.convertToKBytes(imageApp.dataURItoBlob(newURL).size); //convert new image URL to blob to get file.size
						
	//extend filename
	var effect = $('input[name=effect]:checked').val();			
	if(effect == 'grayscale') 
	{
		imageObj.fileName += " (Grayscale)";
	} 
	else if(effect == 'blurry') 
	{
		imageObj.fileName += "(Blurry)";
	} 			
						
	//append new image through jQuery Template
	var randvalue = Math.floor(Math.random()*31)-15;  //random number
	/*
		var img = $("#imageTemplate").tmpl(imageObj).prependTo("#result").hide().show();
	*/	

	$.ajax(
	{
		type:'GET',
		url:'/photo/'+imgId,
		data: "",
		success: function(data)
		{
			$('#result').prepend(data);
		}
	});	
			
	/*
	.css({
	'Transform': 'scale(1) rotate('+randvalue+'deg)',
	'msTransform': 'scale(1) rotate('+randvalue+'deg)',
	'MozTransform': 'scale(1) rotate('+randvalue+'deg)',
	'webkitTransform': 'scale(1) rotate('+randvalue+'deg)',
	'OTransform': 'scale(1) rotate('+randvalue+'deg)',
	'z-index': zindex++
	})
	*/
			
	if(isNaN(imageObj.fileUploadSize)) 
	{
		//$('.imageholder span').last().hide();
	}
};

imageApp.uploadToServer = function(oldFile, newFile, newimageurl, primaryImage)
{
			// prepare FormData
			var formData = new FormData();  
			var imageid = 0;	

			imageApp.organisationId = $('#droparea').attr('data-organisation-id');
			imageApp.profileId = $('#droparea').attr('data-profile-id');	

			//we still have to use back old file
			//since new file doesn't contains original file data
			formData.append('file_ori_size', imageApp.convertToKBytes(oldFile.size));
			formData.append('file_upload_size', imageApp.convertToKBytes(oldFile.size))
			formData.append('filename', oldFile.name);
			formData.append('filetype', oldFile.type);
			formData.append('file', oldFile); 
			formData.append('image_primary', primaryImage);
			formData.append('profile_id', imageApp.profileId);
			formData.append('organisation_id', imageApp.organisationId);
							
			//submit formData using $.ajax			
			 $.ajax({
				url: '/photo',
				type: 'POST',
				data: formData,
				processData: false,
				enctype: 'multipart/form-data',
				contentType: false,
				success: function(data) 
				{	
	               imageApp.createPreview(oldFile, newimageurl, data.photo_id, primaryImage);
	        	}
			});
}

imageApp.processFileInIE = function(file)
{
	var imageObj = {};
	var extension = ['jpg', 'jpeg', 'gif', 'png'];
	var filepath = file.value;
			
	if (filepath) 
	{
		//get file name
		var startIndex = (filepath.indexOf('\\') >= 0 ? filepath.lastIndexOf('\\') : filepath.lastIndexOf('/'));
				var filedetail = filepath.substring(startIndex);
				if (filedetail.indexOf('\\') === 0 || filedetail.indexOf('/') === 0) {
					filedetail = filedetail.substring(1);
				}
				var filename = filedetail.substr(0, filedetail.lastIndexOf('.'));
				var fileext = filedetail.slice(filedetail.lastIndexOf(".")+1).toLowerCase();	

				//check file extension
				if($.inArray(fileext, extension) > -1) 
				{
					//append using template
					/*
					$('#err').text('');
					imageObj.filepath = filepath;
					imageObj.filename = filename;
					var randvalue = Math.floor(Math.random()*31)-15;
					
					$("#imageTemplate").tmpl(imageObj).prependTo( "#result" )
					.hide()
					.css({
						'Transform': 'scale(1) rotate('+randvalue+'deg)',
						'msTransform': 'scale(1) rotate('+randvalue+'deg)',
						'MozTransform': 'scale(1) rotate('+randvalue+'deg)',
						'webkitTransform': 'scale(1) rotate('+randvalue+'deg)',
						'oTransform': 'scale(1) rotate('+randvalue+'deg)',
						'z-index': zindex++
					})
					.show();
					*/
					//$('#result').find('figcaption span').hide();
				} 
				else 
				{
					$('#err').text('*Selected file format not supported!');
				}
			}
}

/***************************** 
	Get New Canvas Image URL
*****************************/
imageApp.getCanvasImage = function(image) 
{
	//get selected effect
	var effect = $('input[name=effect]:checked').val();
	var croping = $('input[name=croping]:checked').val();
	
	//define canvas
	var canvas = document.createElement('canvas');
	canvas.width = imageApp.imgWidth;
	canvas.height = imageApp.imgHeight;
	var ctx = canvas.getContext('2d');
		
	//default resize variable
	var diff = [0, 0];
	if(croping == 'crop') 
	{
		//get resized width and height
		diff = imageApp.compareWidthHeight(image.height);
	}
		
	//draw canvas image	
	ctx.drawImage(image, diff[0]/2, diff[1]/2, image.width-diff[0], image.height-diff[1], 0, 0, imageApp.imgWidth, imageApp.imgHeight);
					
	//apply effects if any					
	if(effect == 'grayscale') 
	{
		grayscale(ctx);
	} 
	else if(effect == 'blurry') 
	{
		blurry(ctx, image, diff);
	} 
	else if(effect == 'sepia') 
	{
		sepia(ctx);
	} 
	else {}
		
	//convert canvas to jpeg url
	return canvas.toDataURL("image/jpeg");
}

module.export = function()
{
	$(document).ready(function()
	{
		$.each($(".image"), imageApp.counting);

		/*****************************
			Events Handler
		*****************************/
		imageApp.dropzone.on('dragover', imageApp.dragover);
		imageApp.dropzone.on('dragleave', imageApp.dragleave);
		imageApp.dropzone.on('drop', imageApp.drop);
		imageApp.uploadBtn.on('click', imageApp.click);
		imageApp.defaultUploadBtn.on('change', imageApp.change);

		$('body').on('click','.primary-image', imageApp.primaryClick);
		$(document.body).on('click','.remove-images', imageApp.remove);
		
		/****************************
				Browser compatible text
		****************************/
		if (typeof FileReader === "undefined") 
		{
			//$('.extra').hide();
			$('#err').html('Hey! Your browser does not support <strong>HTML5 File API</strong> <br/>Try using Chrome or Firefox to have it works!');
		} 
		else if (!Modernizr.draganddrop) 
		{
			$('#err').html('Ops! Look like your browser does not support <strong>Drag and Drop API</strong>! <br/>Still, you are able to use \'<em>Select Files</em>\' button to upload file =)');
		}
		else 
		{
			$('#err').text('');
		}	

	});
}()