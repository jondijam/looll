-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 27, 2014 at 04:32 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.6.0-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `looll`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `CategoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) CHARACTER SET utf8 NOT NULL,
  `name` varchar(225) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=265 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(2, 'AL', 'Albania', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(3, 'DZ', 'Algeria', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(4, 'AS', 'American Samoa', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(5, 'AD', 'Andorra', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(6, 'AO', 'Angola', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(7, 'AI', 'Anguilla', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(8, 'AQ', 'Antarctica', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(9, 'AG', 'Antigua and Barbuda', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(10, 'AR', 'Argentina', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(11, 'AM', 'Armenia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(12, 'AW', 'Aruba', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(13, 'AU', 'Australia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(14, 'AT', 'Austria', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(15, 'AZ', 'Azerbaijan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(16, 'BS', 'Bahamas', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(17, 'BH', 'Bahrain', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(18, 'BD', 'Bangladesh', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(19, 'BB', 'Barbados', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(20, 'BY', 'Belarus', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(21, 'BE', 'Belgium', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(22, 'BZ', 'Belize', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(23, 'BJ', 'Benin', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(24, 'BM', 'Bermuda', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(25, 'BT', 'Bhutan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(26, 'BO', 'Bolivia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(27, 'BA', 'Bosnia and Herzegovina', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(28, 'BW', 'Botswana', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(29, 'BV', 'Bouvet Island', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(30, 'BR', 'Brazil', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(31, 'BQ', 'British Antarctic Territory', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(32, 'IO', 'British Indian Ocean Territory', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(33, 'VG', 'British Virgin Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(34, 'BN', 'Brunei', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(35, 'BG', 'Bulgaria', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(36, 'BF', 'Burkina Faso', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(37, 'BI', 'Burundi', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(38, 'KH', 'Cambodia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(39, 'CM', 'Cameroon', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(40, 'CA', 'Canada', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(41, 'CT', 'Canton and Enderbury Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(42, 'CV', 'Cape Verde', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(43, 'KY', 'Cayman Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(44, 'CF', 'Central African Republic', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(45, 'TD', 'Chad', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(46, 'CL', 'Chile', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(47, 'CN', 'China', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(48, 'CX', 'Christmas Island', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(49, 'CC', 'Cocos [Keeling] Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(50, 'CO', 'Colombia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(51, 'KM', 'Comoros', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(52, 'CG', 'Congo - Brazzaville', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(53, 'CD', 'Congo - Kinshasa', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(54, 'CK', 'Cook Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(55, 'CR', 'Costa Rica', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(56, 'HR', 'Croatia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(57, 'CU', 'Cuba', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(58, 'CY', 'Cyprus', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(59, 'CZ', 'Czech Republic', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(60, 'CI', 'Côte d’Ivoire', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(61, 'DK', 'Denmark', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(62, 'DJ', 'Djibouti', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(63, 'DM', 'Dominica', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(64, 'DO', 'Dominican Republic', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(65, 'NQ', 'Dronning Maud Land', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(66, 'DD', 'East Germany', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(67, 'EC', 'Ecuador', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(68, 'EG', 'Egypt', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(69, 'SV', 'El Salvador', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(70, 'GQ', 'Equatorial Guinea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(71, 'ER', 'Eritrea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(72, 'EE', 'Estonia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(73, 'ET', 'Ethiopia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(74, 'FK', 'Falkland Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(75, 'FO', 'Faroe Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(76, 'FJ', 'Fiji', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(77, 'FI', 'Finland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(78, 'FR', 'France', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(79, 'GF', 'French Guiana', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(80, 'PF', 'French Polynesia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(81, 'TF', 'French Southern Territories', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(82, 'FQ', 'French Southern and Antarctic Territories', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(83, 'GA', 'Gabon', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(84, 'GM', 'Gambia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(85, 'GE', 'Georgia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(86, 'DE', 'Germany', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(87, 'GH', 'Ghana', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(88, 'GI', 'Gibraltar', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(89, 'GR', 'Greece', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(90, 'GL', 'Greenland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(91, 'GD', 'Grenada', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(92, 'GP', 'Guadeloupe', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(93, 'GU', 'Guam', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(94, 'GT', 'Guatemala', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(95, 'GG', 'Guernsey', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(96, 'GN', 'Guinea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(97, 'GW', 'Guinea-Bissau', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(98, 'GY', 'Guyana', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(99, 'HT', 'Haiti', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(100, 'HM', 'Heard Island and McDonald Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(101, 'HN', 'Honduras', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(102, 'HK', 'Hong Kong SAR China', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(103, 'HU', 'Hungary', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(104, 'IS', 'Iceland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(105, 'IN', 'India', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(106, 'is', 'Indonesia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(107, 'IR', 'Iran', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(108, 'IQ', 'Iraq', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(109, 'IE', 'Ireland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(110, 'IM', 'Isle of Man', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(111, 'IL', 'Israel', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(112, 'IT', 'Italy', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(113, 'JM', 'Jamaica', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(114, 'JP', 'Japan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(115, 'JE', 'Jersey', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(116, 'JT', 'Johnston Island', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(117, 'JO', 'Jordan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(118, 'KZ', 'Kazakhstan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(119, 'KE', 'Kenya', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(120, 'KI', 'Kiribati', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(121, 'KW', 'Kuwait', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(122, 'KG', 'Kyrgyzstan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(123, 'LA', 'Laos', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(124, 'LV', 'Latvia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(125, 'LB', 'Lebanon', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(126, 'LS', 'Lesotho', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(127, 'LR', 'Liberia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(128, 'LY', 'Libya', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(129, 'LI', 'Liechtenstein', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(130, 'LT', 'Lithuania', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(131, 'LU', 'Luxembourg', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(132, 'MO', 'Macau SAR China', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(133, 'MK', 'Macedonia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(134, 'MG', 'Madagascar', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(135, 'MW', 'Malawi', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(136, 'MY', 'Malaysia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(137, 'MV', 'Maldives', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(138, 'ML', 'Mali', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(139, 'MT', 'Malta', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(140, 'MH', 'Marshall Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(141, 'MQ', 'Martinique', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(142, 'MR', 'Mauritania', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(143, 'MU', 'Mauritius', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(144, 'YT', 'Mayotte', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(145, 'FX', 'Metropolitan France', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(146, 'MX', 'Mexico', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(147, 'FM', 'Micronesia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(148, 'MI', 'Misoway Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(149, 'MD', 'Moldova', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(150, 'MC', 'Monaco', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(151, 'MN', 'Mongolia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(152, 'ME', 'Montenegro', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(153, 'MS', 'Montserrat', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(154, 'MA', 'Morocco', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(155, 'MZ', 'Mozambique', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(156, 'MM', 'Myanmar [Burma]', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(157, 'NA', 'Namibia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(158, 'NR', 'Nauru', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(159, 'NP', 'Nepal', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(160, 'NL', 'Netherlands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(161, 'AN', 'Netherlands Antilles', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(162, 'NT', 'Neutral Zone', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(163, 'NC', 'New Caledonia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(164, 'NZ', 'New Zealand', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(165, 'NI', 'Nicaragua', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(166, 'NE', 'Niger', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(167, 'NG', 'Nigeria', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(168, 'NU', 'Niue', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(169, 'NF', 'Norfolk Island', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(170, 'KP', 'North Korea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(171, 'VD', 'North Vietnam', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(172, 'MP', 'Northern Mariana Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(173, 'NO', 'Norway', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(174, 'OM', 'Oman', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(175, 'PC', 'Pacific Islands Trust Territory', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(176, 'PK', 'Pakistan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(177, 'PW', 'Palau', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(178, 'PS', 'Palestinian Territories', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(179, 'PA', 'Panama', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(180, 'PZ', 'Panama Canal Zone', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(181, 'PG', 'Papua New Guinea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(182, 'PY', 'Paraguay', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(183, 'YD', 'People''s Democratic Republic of Yemen', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(184, 'PE', 'Peru', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(185, 'PH', 'Philippines', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(186, 'PN', 'Pitcairn Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(187, 'PL', 'Poland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(188, 'PT', 'Portugal', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(189, 'PR', 'Puerto Rico', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(190, 'QA', 'Qatar', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(191, 'RO', 'Romania', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(192, 'RU', 'Russia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(193, 'RW', 'Rwanda', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(194, 'RE', 'Réunion', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(195, 'BL', 'Saint Barthélemy', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(196, 'SH', 'Saint Helena', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(197, 'KN', 'Saint Kitts and Nevis', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(198, 'LC', 'Saint Lucia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(199, 'MF', 'Saint Martin', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(200, 'PM', 'Saint Pierre and Miquelon', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(201, 'VC', 'Saint Vincent and the Grenadines', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(202, 'WS', 'Samoa', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(203, 'SM', 'San Marino', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(204, 'SA', 'Saudi Arabia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(205, 'SN', 'Senegal', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(206, 'RS', 'Serbia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(207, 'CS', 'Serbia and Montenegro', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(208, 'SC', 'Seychelles', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(209, 'SL', 'Sierra Leone', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(210, 'SG', 'Singapore', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(211, 'SK', 'Slovakia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(212, 'SI', 'Slovenia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(213, 'SB', 'Solomon Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(214, 'SO', 'Somalia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(215, 'ZA', 'South Africa', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(216, 'GS', 'South Georgia and the South Sandwich Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(217, 'KR', 'South Korea', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(218, 'ES', 'Spain', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(219, 'LK', 'Sri Lanka', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(220, 'SD', 'Sudan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(221, 'SR', 'Suriname', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(222, 'SJ', 'Svalbard and Jan Mayen', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(223, 'SZ', 'Swaziland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(224, 'SE', 'Sweden', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(225, 'CH', 'Switzerland', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(226, 'SY', 'Syria', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(227, 'ST', 'São Tomé and Príncipe', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(228, 'TW', 'Taiwan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(229, 'TJ', 'Tajikistan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(230, 'TZ', 'Tanzania', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(231, 'TH', 'Thailand', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(232, 'TL', 'Timor-Leste', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(233, 'TG', 'Togo', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(234, 'TK', 'Tokelau', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(235, 'TO', 'Tonga', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(236, 'TT', 'Trinisoad and Tobago', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(237, 'TN', 'Tunisia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(238, 'TR', 'Turkey', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(239, 'TM', 'Turkmenistan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(240, 'TC', 'Turks and Caicos Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(241, 'TV', 'Tuvalu', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(242, 'UM', 'U.S. Minor Outlying Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(243, 'PU', 'U.S. Miscellaneous Pacific Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(244, 'VI', 'U.S. Virgin Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(245, 'UG', 'Uganda', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(246, 'UA', 'Ukraine', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(247, 'SU', 'Union of Soviet Socialist Republics', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(248, 'AE', 'United Arab Emirates', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(249, 'GB', 'United Kingdom', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(250, 'US', 'United States', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(251, 'ZZ', 'Unknown or Invaliso Region', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(252, 'UY', 'Uruguay', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(253, 'UZ', 'Uzbekistan', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(254, 'VU', 'Vanuatu', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(255, 'VA', 'Vatican City', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(256, 'VE', 'Venezuela', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(257, 'VN', 'Vietnam', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(258, 'WK', 'Wake Island', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(259, 'WF', 'Wallis and Futuna', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(260, 'EH', 'Western Sahara', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(261, 'YE', 'Yemen', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(262, 'ZM', 'Zambia', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(263, 'ZW', 'Zimbabwe', '2014-10-25 12:57:53', '2014-10-25 12:57:53'),
(264, 'AX', 'Åland Islands', '2014-10-25 12:57:53', '2014-10-25 12:57:53');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_primary` tinyint(1) NOT NULL DEFAULT '0',
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_ori_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_upload_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=147 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `filename`, `image_primary`, `src`, `file_ori_size`, `file_upload_size`, `created_at`, `updated_at`) VALUES
(30, 'Loftur mynd.jpg', 0, '/img/profile/OzHnEyXiZqSm.jpg', '486.4', '486.4', '2014-07-01 20:05:09', '2014-07-01 20:05:09'),
(31, 'Loftur mynd.jpg', 0, '/img/profile/rQrwdNW7Y3TE.jpg', '486.4', '486.4', '2014-07-01 20:21:51', '2014-07-01 20:21:51'),
(33, 'ég.jpg', 0, '/img/profile/3IsZWEAW6aCu.jpg', '19.5', '19.5', '2014-07-02 09:25:53', '2014-07-02 09:25:53'),
(37, 'matis_hrolfur_MThumb.jpg', 0, '/img/profile/BGxhIPksX97L.jpg', '2.5', '2.5', '2014-07-03 21:03:27', '2014-07-03 21:03:27'),
(38, 'matis_hrolfur_MThumb.jpg', 0, '/img/profile/0rHF7U7HGBYh.jpg', '2.5', '2.5', '2014-07-03 21:04:45', '2014-07-22 17:39:33'),
(39, 'Loftur.png', 0, '/img/profile/BHMydSQi4Vmu.png', '142.8', '142.8', '2014-07-04 08:08:30', '2014-07-04 08:08:30'),
(62, '166470_1814298076113_1034058_n.jpg', 1, '/img/profile/MgtRSXghBWBX.jpg', '50.0', '50.0', '2014-07-07 23:32:54', '2014-07-07 23:33:30'),
(66, 'Loftur FB.jpg', 1, '/img/profile/ncaaHVMIHG8D.jpg', '162.5', '162.5', '2014-07-14 18:35:29', '2014-07-14 18:36:53'),
(73, '2014-03-08 12.23.24.jpg', 0, '/img/profile/XfW2JAXvxlmp.jpg', '1771.4', '1771.4', '2014-07-15 20:56:38', '2014-07-15 20:56:38'),
(74, '2013-12-22 12.38.01.jpg', 0, '/img/profile/Uqo44qCnaElT.jpg', '1340.5', '1340.5', '2014-07-15 20:56:57', '2014-07-15 20:56:57'),
(76, 'New Bitmap Image.png', 1, '/img/profile/6evYqBgOSHiM.png', '2.4', '2.4', '2014-07-20 12:49:35', '2014-07-20 12:49:35'),
(77, 'masi.jpg', 1, '/img/profile/caDIB6JyvpRB.jpg', '9.5', '9.5', '2014-07-20 14:57:37', '2014-07-20 14:57:37'),
(78, 'awesome sloth.jpg', 1, '/img/profile/nmE9xvUc4OoK.jpg', '51.2', '51.2', '2014-07-20 18:48:16', '2014-07-20 18:48:16'),
(79, 'awesome sloth.jpg', 1, '/img/profile/UYs6J1VzSFhZ.jpg', '51.2', '51.2', '2014-07-20 18:52:54', '2014-07-20 18:52:54'),
(81, 'Portfolio myndir af Sigga 006.JPG', 1, '/img/profile/n9GytYQuI0ky.JPG', '2618.8', '2618.8', '2014-07-20 21:30:51', '2014-07-20 21:33:13'),
(84, 'JCI-House-2.jpg', 1, '/img/profile/4FtDtf4UtFrM.jpg', '1529.3', '1529.3', '2014-07-20 21:37:50', '2014-10-05 15:39:41'),
(85, '1006009_10200650174869619_2144049857_n.jpg', 0, '/img/profile/Fqr7NRtbVudq.jpg', '109.0', '109.0', '2014-07-20 21:38:38', '2014-10-05 15:39:41'),
(86, 'jondi.jpg', 0, '/img/profile/X4bRSdXmXtyt.jpg', '46.0', '46.0', '2014-07-21 06:14:32', '2014-07-21 06:14:32'),
(87, 'Fimmvörðuháls 007.JPG', 1, '/img/profile/bTZlFzy5CYKp.JPG', '2121.5', '2121.5', '2014-07-21 22:14:59', '2014-07-22 17:39:33'),
(88, 'Fimmvörðuháls 015.JPG', 0, '/img/profile/HkS8pwklrTnf.JPG', '1741.5', '1741.5', '2014-07-21 22:16:15', '2014-07-22 17:39:33'),
(89, '1069323_484144091664602_1552006834_n.jpg', 0, '/img/profile/CRJnRwNGggOU.jpg', '69.3', '69.3', '2014-07-22 15:55:10', '2014-07-22 15:55:10'),
(90, '010.JPG', 0, '/img/profile/xR8Mgi3YGqtt.JPG', '113.2', '113.2', '2014-07-22 15:56:24', '2014-07-22 15:56:24'),
(92, '10441401_10203334225729177_3012675234519118154_n.jpg', 0, '/img/profile/Dx4oeMoaKcr6.jpg', '43.7', '43.7', '2014-07-22 18:26:45', '2014-07-22 18:26:45'),
(93, 'bij.jpg', 1, '/img/profile/8aeoPmlKV0W3.jpg', '30.4', '30.4', '2014-07-26 11:19:43', '2014-07-26 11:19:43'),
(94, 'bij.jpg', 1, '/img/profile/ffuUGxrXFqRa.jpg', '30.4', '30.4', '2014-07-26 11:24:10', '2014-07-26 11:24:10'),
(95, 'DSC_0028.JPG', 1, '/img/profile/JjooCswndodK.JPG', '2455.2', '2455.2', '2014-07-28 19:35:43', '2014-07-28 19:35:43'),
(97, 'DSC_0069.JPG', 0, '/img/profile/fw6kbJ9T73uS.JPG', '2306.8', '2306.8', '2014-07-28 19:37:41', '2014-07-28 19:37:41'),
(98, 'DSC_0370.JPG', 0, '/img/profile/cHEwWbwXN5x0.JPG', '2348.8', '2348.8', '2014-07-28 19:38:24', '2014-07-28 19:38:24'),
(101, 'Copie (3) de 100_3717.JPG', 0, '/img/profile/DWUQkPdO8Zi7.JPG', '575.2', '575.2', '2014-07-28 19:49:25', '2014-07-28 19:49:25'),
(102, 'DSC_0028.JPG', 1, '/img/profile/IFQlkMwKUW27.JPG', '2455.2', '2455.2', '2014-07-28 19:53:41', '2014-07-28 19:53:41'),
(103, '10301189_10152561280787985_5546967337910875769_n.jpg', 1, '/img/profile/jTd6WMQjJxGs.jpg', '63.1', '63.1', '2014-09-07 14:40:14', '2014-09-07 14:40:14'),
(104, '1238176_10200840910685991_857837914_n.jpg', 1, '/img/profile/lXmK74VmCkz0.jpg', '23.7', '23.7', '2014-09-07 15:00:21', '2014-09-07 15:00:21'),
(105, 'DSC02131.JPG', 1, '/img/profile/CVtm3GdwF9f7.JPG', '1388.5', '1388.5', '2014-09-07 15:17:31', '2014-09-07 15:17:31'),
(106, '248302_10151194681513049_1836864501_n.jpg', 1, '/img/profile/17IDwKRpxuPv.jpg', '9.3', '9.3', '2014-09-07 15:20:10', '2014-09-07 15:20:10'),
(107, '248302_10151194681513049_1836864501_n.jpg', 1, '/img/profile/QILco9fD41bY.jpg', '9.3', '9.3', '2014-09-07 15:25:44', '2014-09-07 15:25:44'),
(108, '10523683_10152705703647645_5111967904094171874_n.jpg', 1, '/img/profile/f4aPsMRDcpQX.jpg', '62.6', '62.6', '2014-09-29 21:28:02', '2014-09-29 21:28:02'),
(109, '10501607_10154559810810398_4773495479097562026_n.jpg', 1, '/img/profile/h3zwYxNX1Gtg.jpg', '109.9', '109.9', '2014-09-29 21:55:49', '2014-09-29 21:55:49'),
(111, '2014-08-23 11.42.37.jpg', 0, '/img/profile/XfVccc8nM9pj.jpg', '2075.3', '2075.3', '2014-10-04 11:49:35', '2014-10-04 11:49:35'),
(112, 'mynd esja 1.jpg', 0, '/img/profile/msFhrEoaz4Aj.jpg', '54.8', '54.8', '2014-10-05 12:23:51', '2014-10-05 12:23:51'),
(113, 'mynd esja 2.jpg', 0, '/img/profile/3Kp0xTSK16TF.jpg', '54.6', '54.6', '2014-10-05 12:24:03', '2014-10-05 12:24:03'),
(114, 'mynd esja 3.jpg', 0, '/img/profile/g15lv5YEnJcp.jpg', '63.2', '63.2', '2014-10-05 12:24:13', '2014-10-05 12:24:13'),
(115, 'mynd esja 4.jpg', 0, '/img/profile/m4vZwqiMWnFS.jpg', '62.8', '62.8', '2014-10-05 12:24:21', '2014-10-05 12:24:21'),
(116, 'Loftur 10.jpg', 0, '/img/profile/ViP6RrMndUfE.jpg', '53.3', '53.3', '2014-10-05 14:44:37', '2014-10-05 14:44:37'),
(117, '10678629_10152486357318477_8095898891947575531_n.jpg', 0, '/img/profile/87yrWs90Ahz5.jpg', '124.7', '124.7', '2014-10-05 15:35:53', '2014-10-05 15:39:40'),
(118, '984189_10202899494781413_8331792599670753405_n.jpg', 0, '/img/profile/z7WD3KnaNwc0.jpg', '77.9', '77.9', '2014-10-05 15:39:24', '2014-10-05 15:39:40'),
(119, '10408766_10203279722566632_3935405384445492868_n.jpg', 0, '/img/profile/BnU7Q1g9fsxR.jpg', '72.0', '72.0', '2014-10-05 15:39:30', '2014-10-05 15:39:40'),
(120, 'jondi.jpg', 1, '/img/profile/tQOKJ4A88f0y.jpg', '56.8', '56.8', '2014-10-25 20:35:33', '2014-10-25 20:35:33'),
(121, '10557673_10152323195199716_6958065225360165586_o.jpg', 1, '/img/profile/HaxILrgQRn4l.jpg', '129.2', '129.2', '2014-10-25 20:36:01', '2014-10-25 20:36:01'),
(122, 'jondi.jpg', 0, '/img/profile/17Z2NS3beh9X.jpg', '56.8', '56.8', '2014-10-25 20:37:34', '2014-10-25 20:37:34'),
(129, 'jondi.jpg', 0, '/img/profile/huCRLvcuNLcp.jpg', '56.8', '56.8', '2014-10-26 14:46:29', '2014-10-27 13:05:33'),
(131, '10557673_10152323195199716_6958065225360165586_o.jpg', 1, '/img/profile/8nBGF26iJsnL.jpg', '129.2', '129.2', '2014-10-26 14:48:21', '2014-10-27 13:05:34'),
(132, 'jondi.jpg', 1, '/img/profile/ZnskBOhQjabn.jpg', '56.8', '56.8', '2014-10-26 18:29:32', '2014-10-26 18:29:32'),
(143, 'devalt.jpg', 0, '/img/profile/TM2eBLXK94te.jpg', '100.5', '100.5', '2014-10-27 13:00:41', '2014-10-27 13:00:41'),
(144, 'devalt.jpg', 0, '/img/profile/HvBjahCHDc5v.jpg', '100.5', '100.5', '2014-10-27 13:03:26', '2014-10-27 13:03:26'),
(146, 'fishfinder02.jpg', 0, '/img/profile/B2lV49YIFZRf.jpg', '13.2', '13.2', '2014-10-27 13:51:54', '2014-10-27 13:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `image_organisation`
--

CREATE TABLE IF NOT EXISTS `image_organisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `image_organisation`
--

INSERT INTO `image_organisation` (`id`, `organisation_id`, `image_id`) VALUES
(4, 8, 84),
(5, 8, 85),
(6, 10, 103),
(7, 12, 108),
(8, 13, 109),
(9, 10, 112),
(10, 10, 113),
(11, 10, 114),
(12, 10, 115),
(13, 8, 117),
(14, 8, 118),
(15, 8, 119),
(16, 1, 132),
(17, 1, 133),
(18, 1, 134),
(19, 1, 135),
(20, 1, 136),
(21, 1, 137),
(22, 1, 138),
(23, 1, 139),
(24, 1, 140),
(25, 1, 141),
(26, 1, 142),
(27, 1, 143),
(28, 1, 144);

-- --------------------------------------------------------

--
-- Table structure for table `image_profile`
--

CREATE TABLE IF NOT EXISTS `image_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=77 ;

--
-- Dumping data for table `image_profile`
--

INSERT INTO `image_profile` (`id`, `profile_id`, `image_id`, `created_at`, `updated_at`) VALUES
(9, 8, 38, '2014-07-03 21:04:49', '2014-07-03 21:04:49'),
(32, 4, 62, '2014-07-07 23:32:54', '2014-07-07 23:32:54'),
(36, 6, 66, '2014-07-14 18:35:29', '2014-07-14 18:35:29'),
(43, 6, 73, '2014-07-15 20:56:38', '2014-07-15 20:56:38'),
(44, 6, 74, '2014-07-15 20:56:57', '2014-07-15 20:56:57'),
(46, 11, 76, '2014-07-20 12:49:35', '2014-07-20 12:49:35'),
(47, 14, 77, '2014-07-20 14:57:37', '2014-07-20 14:57:37'),
(48, 15, 79, '2014-07-20 18:52:54', '2014-07-20 18:52:54'),
(50, 7, 81, '2014-07-20 21:30:51', '2014-07-20 21:30:51'),
(51, 4, 86, '2014-07-21 06:14:32', '2014-07-21 06:14:32'),
(52, 8, 87, '2014-07-21 22:14:59', '2014-07-21 22:14:59'),
(53, 8, 88, '2014-07-21 22:16:15', '2014-07-21 22:16:15'),
(54, 7, 89, '2014-07-22 15:55:10', '2014-07-22 15:55:10'),
(55, 7, 90, '2014-07-22 15:56:25', '2014-07-22 15:56:25'),
(57, 7, 92, '2014-07-22 18:26:45', '2014-07-22 18:26:45'),
(58, 19, 94, '2014-07-26 11:24:10', '2014-07-26 11:24:10'),
(59, 21, 102, '2014-07-28 19:53:41', '2014-07-28 19:53:41'),
(60, 16, 104, '2014-09-07 15:00:21', '2014-09-07 15:00:21'),
(61, 12, 105, '2014-09-07 15:17:31', '2014-09-07 15:17:31'),
(62, 31, 107, '2014-09-07 15:25:44', '2014-09-07 15:25:44'),
(64, 6, 111, '2014-10-04 11:49:35', '2014-10-04 11:49:35'),
(65, 6, 116, '2014-10-05 14:44:37', '2014-10-05 14:44:37'),
(72, 33, 129, '2014-10-26 14:46:29', '2014-10-26 14:46:29'),
(74, 33, 131, '2014-10-26 14:48:21', '2014-10-26 14:48:21'),
(76, 33, 146, '2014-10-27 13:51:54', '2014-10-27 13:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_11_151405_create_users_table', 1),
('2014_05_11_152445_create_users_login_table', 2),
('2014_05_11_160325_create_ContactCategory_table', 3),
('2014_05_22_160536_create_images_table', 4),
('2014_05_24_120604_create_organisations_have_users_table', 6),
('2014_05_24_120937_create_contacts_table', 7),
('2014_05_24_125609_create_profiles_table', 8),
('2014_05_27_113028_create_profiles_have_images_table', 9),
('2014_07_16_145928_create_password_reminders_table', 10),
('2014_10_21_085405_rename_users_table', 11),
('2014_10_21_090343_rename_phone_numbers_table', 11),
('2014_10_21_090653_rename_images_table', 11),
('2014_10_21_091116_rename_organisations_table', 11),
('2014_10_21_091343_rename_profile_table', 11),
('2014_10_21_091435_rename_profiles_table', 11),
('2014_10_21_091701_rename_profiles_have_images_table', 11),
('2014_10_21_091904_rename_image_profile_table', 11),
('2014_10_21_092051_rename_organisations_have_images_table', 11),
('2014_10_21_092239_rename_image_organisation_table', 11),
('2014_10_21_092421_rename_organisationshaveusers_table', 11),
('2014_10_21_092624_rename_organisation_user_table', 11),
('2014_10_21_094442_rename_organisations_table', 12),
('2014_10_25_124330_create_country_table', 13),
('2014_10_25_130333_add_country_id_to_profiles', 14),
('2014_10_25_141013_add_gender_to_profiles', 15),
('2014_05_23_151515_create_organisations_table', 17),
('2014_10_26_161653_add_country_id_to_organisation_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE IF NOT EXISTS `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `announcement` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_active` tinyint(1) NOT NULL,
  `member_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `name`, `address`, `city`, `zip`, `email`, `description`, `announcement`, `country_id`, `parent_id`, `parent_active`, `member_status`, `created_at`, `updated_at`) VALUES
(1, 'Name1', 'Vesturberg 70', 'Reykjavik', '111', 'name1@name1.is', 'Lýsing virkar', 'Tilkynning virkar', 104, 2, 1, 2, '2014-10-26 18:23:22', '2014-10-27 12:40:04'),
(2, 'Name', '', '', '', NULL, '', NULL, 1, 0, 0, 3, '2014-10-26 18:24:09', '2014-10-27 06:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_user`
--

CREATE TABLE IF NOT EXISTS `organisation_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `member` tinyint(1) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=109 ;

--
-- Dumping data for table `organisation_user`
--

INSERT INTO `organisation_user` (`id`, `organisation_id`, `user_id`, `owner`, `member`) VALUES
(103, 1, 42, 1, 1),
(104, 2, 42, 1, 1),
(106, 1, 43, 0, 1),
(107, 1, 7, 2, 1),
(108, 1, 35, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_reminders`
--

INSERT INTO `password_reminders` (`email`, `token`, `created_at`) VALUES
('sigurdurvs@gmail.com', '370c37251b231f120a354ef0eb8d30befcbfb394', '2014-07-24 17:35:41'),
('jondi@formastudios.com', '45ceb57bab62b5629a2310604e1c252c21a7606b', '2014-08-20 09:53:58'),
('jondi@formastudios.com', 'a4567ae32410d167ef24d3a829cdb03747aabb9b', '2014-08-20 09:55:41'),
('jondi@formastudios.com', '2f52f000c79c4cd2480020967acaccdbbe959499', '2014-08-20 09:57:24'),
('jondi@formastudios.com', 'fb79069304b500582de00bb704fd729561836194', '2014-08-20 09:58:42'),
('goldor23@hotmail.com', '9ad54a9da7d1c8f0eacae31787866337f9b0b350', '2014-09-07 15:17:11'),
('goldor23@hotmail.com', 'd55178d1ca53c5c0f99aa250495f0e4d0edad503', '2014-09-07 15:17:19'),
('goldor23@hotmail.com', 'c022c2b39b246b929462ed9796d398dee35b3e4b', '2014-09-07 15:17:49'),
('jondi@formastudios.com', '6ff96882431a74b8f810f6534cdaaa95e5ae8644', '2014-09-29 14:35:09'),
('jondi@formastudios.com', '95ba223809765848bde1bfaa9042321d435e72c2', '2014-10-19 22:37:52'),
('jondi@formastudios.com', '43115058834678496da986ee434fcb1cacf93ecd', '2014-10-19 22:38:44'),
('jondi@formastudios.com', 'c856599fc3026c9d76c28cbbd762227c3a873e81', '2014-10-19 22:39:05'),
('jondi@formastudios.com', 'ef8b3bae4220250b789cfc0221e8284353dc82ba', '2014-10-19 22:40:18'),
('jondi@formastudios.com', 'cef9c50e12af33b5bdaad4233f4a1895390fb7c0', '2014-10-19 22:41:18'),
('jondi@formastudios.com', '1c93e91f1aad0d4cfc51cdd0d5023f2b754ec094', '2014-10-19 22:41:26'),
('jondi@formastudios.com', '910ed6567bb2948e9a10909803863d636ad5d8ff', '2014-10-19 22:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `phone_numbers`
--

CREATE TABLE IF NOT EXISTS `phone_numbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purpose` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Dumping data for table `phone_numbers`
--

INSERT INTO `phone_numbers` (`id`, `phone_number`, `purpose`, `profile_id`, `created_at`, `updated_at`) VALUES
(3, '866 4500', 'mobile', 6, '2014-07-01 20:21:28', '2014-07-18 17:15:55'),
(4, '00 354 8699024', 'mobile', 7, '2014-07-01 21:03:07', '2014-07-01 21:03:07'),
(5, '8670698', 'mobile', 8, '2014-07-01 22:26:00', '2014-07-01 22:26:00'),
(6, '847-3619', 'mobile', 4, '2014-07-02 10:15:18', '2014-07-18 17:10:44'),
(7, '6947185', 'mobile', 10, '2014-07-04 19:50:40', '2014-07-04 19:50:40'),
(8, 'Ekki með heimasíma', 'home', 6, '2014-07-16 17:11:36', '2014-07-18 23:02:35'),
(9, '591 2721', 'buisness', 6, '2014-07-16 17:11:36', '2014-07-18 17:15:55'),
(10, '354 8473619', 'home', 4, '2014-07-18 17:06:34', '2014-07-18 17:06:34'),
(11, '354 8473619', 'home', 4, '2014-07-18 17:06:48', '2014-07-18 17:06:48'),
(12, '354 8473619', 'buisness', 4, '2014-07-18 17:06:48', '2014-07-18 17:06:48'),
(13, '354 8473619', 'home', 4, '2014-07-18 17:08:38', '2014-07-18 17:08:38'),
(14, '847 3619', 'buisness', 4, '2014-07-18 17:08:38', '2014-07-18 17:08:38'),
(15, '847 3619', 'home', 4, '2014-07-18 17:10:44', '2014-07-18 17:10:44'),
(16, '847 3619', 'buisness', 4, '2014-07-18 17:10:44', '2014-07-18 17:10:44'),
(17, '5650533', 'home', 14, '2014-07-20 14:56:53', '2014-07-20 14:56:53'),
(18, '8203060', 'mobile', 14, '2014-07-20 14:56:53', '2014-07-20 14:56:53'),
(19, '8926162', 'home', 18, '2014-07-24 17:27:44', '2014-07-24 17:27:44'),
(20, '8926162', 'mobile', 18, '2014-07-24 17:27:44', '2014-07-24 17:27:44'),
(21, '354 6597169', 'home', 19, '2014-07-26 11:22:52', '2014-07-26 11:22:52'),
(22, '5672025', 'home', 25, '2014-07-29 19:18:50', '2014-07-29 19:18:50'),
(23, '8588306', 'mobile', 25, '2014-07-29 19:18:50', '2014-07-29 19:18:50'),
(24, '4588306', 'buisness', 25, '2014-07-29 19:18:50', '2014-07-29 19:18:50'),
(25, '8669228', 'home', 26, '2014-07-29 20:44:17', '2014-07-29 20:44:17'),
(26, '8669228', 'home', 27, '2014-07-29 20:44:28', '2014-07-29 20:44:28'),
(27, '6994859', 'home', 28, '2014-08-01 13:37:10', '2014-08-01 13:37:10'),
(28, '6994859', 'home', 28, '2014-08-01 13:37:21', '2014-08-01 13:37:21'),
(29, '6994859', 'home', 29, '2014-08-01 13:39:30', '2014-08-01 13:39:30'),
(30, '6152190', 'mobile', 12, '2014-09-07 15:17:49', '2014-09-07 15:17:49'),
(31, '7763665', 'home', 31, '2014-09-07 15:25:11', '2014-09-07 15:25:11'),
(32, '6604802', 'home', 32, '2014-09-14 12:09:13', '2014-09-14 12:09:13'),
(33, '6604802', 'mobile', 32, '2014-09-14 12:09:13', '2014-09-14 12:09:13'),
(34, '8926162', 'home', 9, '2014-09-19 20:20:59', '2014-09-19 20:20:59'),
(35, '8926162', 'mobile', 9, '2014-09-19 20:20:59', '2014-09-19 20:20:59'),
(36, '8926162', 'buisness', 9, '2014-09-19 20:20:59', '2014-09-19 20:20:59'),
(37, '8926162', 'home', 9, '2014-09-19 20:21:51', '2014-09-19 20:21:51'),
(38, '8926162', 'mobile', 9, '2014-09-19 20:21:51', '2014-09-19 20:21:51'),
(39, '8926162', 'buisness', 9, '2014-09-19 20:21:51', '2014-09-19 20:21:51'),
(40, '8926162', 'home', 9, '2014-09-19 20:23:13', '2014-09-19 20:23:13'),
(41, '8926162', 'mobile', 9, '2014-09-19 20:23:13', '2014-09-19 20:23:13'),
(42, '8926162', 'buisness', 9, '2014-09-19 20:23:13', '2014-09-19 20:23:13'),
(43, '8473619', 'home', 33, '2014-10-20 05:50:40', '2014-10-20 05:50:40'),
(44, '8473619', 'mobile', 33, '2014-10-20 05:50:40', '2014-10-20 05:50:40'),
(45, '8473619', 'buisness', 33, '2014-10-20 05:50:40', '2014-10-20 05:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `birthday` date DEFAULT NULL,
  `marital_status` int(11) NOT NULL DEFAULT '0',
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(355) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `announcement` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Sveitarfélag',
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT '000',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(11) NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `active`, `birthday`, `marital_status`, `position`, `company`, `description`, `announcement`, `email`, `address`, `city`, `zip`, `user_id`, `created_at`, `updated_at`, `country_id`, `gender`) VALUES
(6, 'Loftur Már Sigurðsson', 1, '1969-10-10', 1, 'Viðskiptastjóri', 'Orka Náttúrunnar ohf.', 'Vörustjórnunarfræðingur&nbsp;<br><span style="line-height: 1.42857143;">Viðskiptastjóri hjá Orku Náttúrunnar ( ON )<br></span><span style="line-height: 1.42857143;">Senator #68068 í JCI<br></span><span style="line-height: 1.42857143;">Rotary félagi</span><p></p><p></p><p></p><p></p>', 'Vinna við looll.is heldur áfram um helgina  :-) ', 'loftur@btnet.is', 'Veghús 31', 'Reykjavík', '112', 7, '2014-07-01 20:21:28', '2014-10-05 15:25:50', 0, ''),
(7, 'Sigurdur Sigurdsson', 1, '1986-01-14', 2, 'President', 'JCI Iceland', 'National President of JCI Iceland', 'Looking forward to the national convention of JCI Finland next weekend', 'sigurdursigurds@gmail.com', 'Hellusund 3', 'Reykjavik', '101', 9, '2014-07-01 21:03:07', '2014-10-05 15:42:14', 0, ''),
(8, 'Hrólfur Sigurðsson', 1, '1972-08-11', 2, 'Sérfræðingur', 'Matís ohf', '', '', 'hrolfur@matis.is', 'Baldursgata 30', 'Reykjavík', '101', 10, '2014-07-01 22:26:00', '2014-07-21 22:19:27', 0, ''),
(9, 'Sigurður Vilberg Svavarsson', 1, '0000-00-00', 2, 'Sérfræðingur ', 'Síminn hf', '', '', 'sigurdurvs@siminn.is', 'Hraunbær 32', 'Reykjavík', '110', 11, '2014-07-02 09:30:15', '2014-09-19 20:23:13', 0, ''),
(10, 'Þórey Rúnarsdóttir', 1, '0000-00-00', 0, '', '0', '', NULL, 'thorey.runarsdottir@gmail.com', 'Heima', 'Reykjavik', '100', 14, '2014-07-04 19:50:40', '2014-07-04 19:56:00', 0, ''),
(11, 'Kjaran Sveinsson', 1, '0000-00-00', 0, '', '', '', '', 'kjaransv@gmail.com', 'Hellusund 3', 'Reykjavík', '101', 15, '2014-07-20 12:47:25', '2014-07-20 13:00:40', 0, ''),
(12, 'Nína María Magnúsdóttir', 1, '0000-00-00', 1, '', '', '', '', 'ninamariamg@gmail.com', 'Holtsgata 6', 'Reykjavík', '101', 16, '2014-07-20 14:05:37', '2014-09-07 15:17:49', 0, ''),
(14, 'Már Karlsson', 1, '1977-11-30', 0, '', '', '', '', 'mar_karlsson@hotmail.com', 'Funalind 1', 'Kópavogur', '201', 17, '2014-07-20 14:56:53', '2014-07-20 14:56:53', 0, ''),
(15, 'Svava Arnardóttir', 1, '0000-00-00', 0, '', '', 'test test test test', 'Hæ hó-announcement!', 'svavaarnardottir@gmail.com', 'Tröllagil 29', 'Akureyri', '603', 18, '2014-07-20 18:49:26', '2014-07-20 18:49:49', 0, ''),
(16, 'Fanney Þórisdóttir', 1, '0000-00-00', 0, '', '', '', '', 'fanney.89@gmail.com', 'Heima hjá mér', 'Garðabær', '210', 19, '2014-07-20 22:19:57', '2014-10-05 12:40:19', 0, ''),
(17, 'Helgi Ólafsson', 1, '0000-00-00', 2, '', '', '', '', 'helgio06@ru.is', 'Dalhús 56', 'Reykjavík', '112', 21, '2014-07-21 19:35:30', '2014-07-21 19:59:39', 0, ''),
(18, 'JCI Lind', 1, '0000-00-00', 0, '', '', 'Aðildarfélagið JCI Lind', '', 'sigurdurvs@gmail.com', 'Hellusund 3', 'Reykjavík', '101', 23, '2014-07-24 17:27:44', '2014-07-24 17:27:44', 0, ''),
(19, 'Bjarni Ingvar Jóhannsson', 1, '1977-04-03', 1, '', '', '', '', 'bjarniij@gmail.com', 'Mánagata 11', 'Reykjavik', '105', 24, '2014-07-26 11:22:52', '2014-07-26 11:22:52', 0, ''),
(20, 'Þórður Páll Jónínuson', 1, '1991-11-29', 1, '', '', '', '', 'rage23123@gmail.com', 'bogahlíð 17', 'Reykjavík', '105', 25, '2014-07-28 19:16:27', '2014-07-28 19:16:27', 0, ''),
(21, 'Sigga Jörgensdóttir', 1, '0000-00-00', 1, '', '', 'Íslandsvinur, þýðandi og tungumálakennari.', '', 'jne3@hi.is', 'Holtagerði', 'Kópavogur', '200', 26, '2014-07-28 19:52:23', '2014-07-28 19:54:01', 0, ''),
(22, 'Bogna', 1, '0000-00-00', 2, '', '', '', '', 'bognabraw@hotmail.com', 'Dworcowa', 'Bojanowo', '63-940', 28, '2014-07-28 20:15:34', '2014-07-28 20:15:39', 0, ''),
(23, 'Jóhanna', 1, '0000-00-00', 0, '', '', '', '', 'johannamagn@gmail.com', 'Grafarvogur', 'Reykjavík', '112', 30, '2014-07-29 00:02:29', '2014-07-29 00:03:00', 0, ''),
(24, 'Eyvindur Elí Albertsson', 1, '1981-12-20', 1, '', '', 'Cool gaur', 'Sjá lísingu :)', 'Eyvindureli@gmail.com', 'Grettisgata 29', 'Reykjavík', '101', 33, '2014-07-29 19:17:47', '2014-07-29 19:17:47', 0, ''),
(25, 'Hafþór Húni Guðmundsson', 1, '1982-06-25', 1, 'Sérfræðingur í Hugbúnaðardeild', 'Samskip', '', '', 'hafthor.huni.gudmundsson@samskip.com', 'Berjarimi 2', 'Reykjavík', '112', 34, '2014-07-29 19:18:50', '2014-07-29 19:18:50', 0, ''),
(26, 'Loftur Jónsson', 1, '1986-03-29', 0, 'Plant', 'Becromal Iceland', 'sveitamaður frá hornafirði', 'ha ?? er eitthvað grín í gangi ??', 'lofjon@simnet.is', 'Árbær', 'Hornafjörður', '781', 35, '2014-07-29 20:44:17', '2014-07-29 20:44:17', 0, ''),
(28, 'Herborg Sörensen', 1, '1988-05-02', 2, '', '', '', '', 'herborg88@gmail.com', 'Kirkjuteigur 18', 'Reykjavik', '105', 31, '2014-08-01 13:37:10', '2014-08-01 13:39:55', 0, ''),
(31, 'Hannes Hall', 1, '1977-04-25', 1, '', '', 'Ég um mig, frá mér, til mín.', 'Gaman að sjá þig.', 'goldor23@hotmail.com', 'JC heimilið', 'Reykjavík', '101', 32, '2014-09-07 15:25:11', '2014-09-07 15:25:11', 0, ''),
(32, 'Kjartan Hansson', 1, '1986-07-22', 0, '', '', '', '', 'kjahansson@gmail.com', 'Súluhöfði 29', 'Mosfellsbær', '270', 41, '2014-09-14 12:09:13', '2014-09-14 12:09:13', 0, ''),
(33, 'Jon Arnar Magnusson', 1, '1981-02-20', 1, 'Forritari', 'Formastudios', 'Lýsing virkar', 'Tilkynning virkar mjög vel.', '', 'Vesturberg 70', 'Reykjavik', '111', 42, '2014-10-19 23:42:17', '2014-10-25 18:04:47', 104, 'male'),
(34, 'Jon Arnar Magnusson', 1, '1981-02-20', 1, '', '', '', '', 'jon_magnusson@hotmail.com', 'Vesturberg 70', 'Reykjavik', '111', 43, '2014-10-26 21:55:59', '2014-10-26 21:55:59', 104, 'male');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `confirmation` varchar(355) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `remember_token`, `role_id`, `confirmation`, `confirmed`, `created_at`, `updated_at`) VALUES
(7, 'loftur@btnet.is', 'loftur', '$2y$10$VZhlsnUH1Ui/bHVB9xoOKOB6Az76ilHxy6CrB0Te8Ww2k5nz2J99K', 'uZ7v6HnBSGu0yx916NRIeYk7CCXpvnHzpmdk6IyR3Gi0bX4CBjZZNV0VdvOP', 0, '8IHKpr16UC5tP0TGvPgw6wAkU8vMINr35qmVkXcl', 1, '2014-07-01 18:04:08', '2014-10-05 19:13:06'),
(9, 'sigurdursigurds@gmail.com', 'Siggi', '$2y$10$F9B0ZRcSvCHRt3IhbCLnZus2POGAOuMqzGYPVZUIG1IFQSeIq0KX2', 'fAwTmC2cKOxhwcJAKeq6TZ0ARzQcLy9VDmMNsfVzdOfmT0kcM5WGPxX6miAr', 0, 'peLtNjhefbSbBZkuseBt6zJqxAvQPFk7BwscpMeW', 1, '2014-07-01 20:58:18', '2014-07-21 16:51:16'),
(10, 'hrolfur@matis.is', 'hrolfur', '$2y$10$CAM1GlHN/9DnmXMnU3OgieUmRMmIutWK8ZQT1qNgkaoktSgBWUKY.', 'Gu7J1DUH2NrvgBYBrIPtzPV4F7mRTPJxq4qPL9Nz5HiVNTI8hW964n7XKls0', 0, 'LZztMw8CP7s59PRDEV2yRZeuGB1Bt2b7fOJGYjWs', 1, '2014-07-01 22:22:52', '2014-07-24 17:55:29'),
(11, 'sigurdurvs@siminn.is', 'Siggivs', '$2y$10$V4BubZJK22fjjABcfQouE.t3si6OBrc6/xVckY1TP4c3Vfx.UJmXy', 'ix9BkAH2F0Z44MI8Q3Ek8nak3HfpI141osanRmHHPgtlwrMEYboKUtluElwS', 0, 'VCzfpCGa87WZ7yNaIm5DY6hbClccSyNVdAKVI30O', 1, '2014-07-02 09:24:23', '2014-09-19 20:27:02'),
(12, 'jcilind@jci.is', 'JCILIND', '$2y$10$fHjAEy39PMxyuI4G7MWnkeSUXZEeKiFZ0gbpxVemzqbuyBuTEhTR2', NULL, 0, 'TfDExzgS8Pim7NPQg6CQbuBRYMoxNQFC7lmmJ8ZJ', 0, '2014-07-02 09:31:46', '2014-07-02 09:31:46'),
(13, 'sigurdurvs@jci.is', 'Jci Lind', '$2y$10$JrtuG/dz3Wuj/tjZkONBg.xJYzhaZKwhpKqPF4xuGlw2qb9Yp/IW6', NULL, 0, 'Owu6AQmHakBhF9Ia9sGr2F8cl1CqeWghuXngA4TC', 0, '2014-07-02 09:32:23', '2014-07-02 09:32:23'),
(14, 'thorey.runarsdottir@gmail.com', 'thorey', '$2y$10$MNtfm//cq6O8K8tpLwGXiOPAwGqTwrkRtWifWvPQ5JdjUr5m89MjW', 'uePIeLNdWolbaNG85HfNNvSX7dlfpukscCo3dTlFGKM5vTPSl8uXIU73CmXI', 0, '0BolMs9KLLYOZwfbuKvAcLp14GZLB1F7uq3hVQdM', 1, '2014-07-04 19:42:57', '2014-07-04 19:56:05'),
(15, 'kjaransv@gmail.com', 'kjaransv', '$2y$10$XzXRkbg7ArghbLlew.Z3q.aW3hF/PAUodBBei7F/chwb6OzNflNa2', NULL, 0, 'YfNufdIOpOUv8DDRNQCsoB3WG2vOUJnAYufXkrM8', 1, '2014-07-20 12:44:17', '2014-09-30 12:38:11'),
(16, 'nina@jci.is', 'ninamaria', '$2y$10$cP5a6zsmAwLeeC2Up8yH0emC8menZNsglTLFfws3DtmF3YQgK/nNO', 'T6zsTwkazyK9kCbJhaVuAuUHIvf7Y4IcunOJjlxoxR8AFk38OBwDXLdF36xR', 0, 'hpzG2vs8G8W15J9EDsIC52xo9DUG0DicjpHa2rZ5', 1, '2014-07-20 14:02:02', '2014-09-07 15:19:32'),
(17, 'mar_karlsson@hotmail.com', 'Már Karlsson', '$2y$10$S1RgQbIqv9MIZMC87MqhJO1Wu2gs3pz3lmyWbFTGWAGjCuap4Ue8G', 'gsG2o5G1MvZiNg5I5hl8YDGbgMZcLnrJSkyzWZHaldjMQiysy9YJ6vlcqVRI', 0, 'Ilj43PlHEM4tWDV0dqLuxKvEXsdxdTKaAx1rty6a', 1, '2014-07-20 14:48:57', '2014-07-20 15:25:55'),
(18, 'svavaarnardottir@gmail.com', 'svava', '$2y$10$gExEAZ1VSYQHWX1ob4iW2edjF90HOLyUW76ZfL9sgKDOXZqwRGBR6', NULL, 0, 'L4jyhLtXYWI3pQP1meDCUPG5cyqDQmuAvvbjJOx7', 1, '2014-07-20 18:43:10', '2014-07-20 18:47:36'),
(19, 'fanney.89@gmail.com', 'fanneyth', '$2y$10$EN9ys7AWjf8SRPNOyeV1Ke52R8ZyGj2YyUaH2QOnYAFk2p7yttVB2', '1tMLyWx21ReG51E2eHfS9Hxx95LCvuP58nYqJtnN9M62wcHv20lHFoSiepc9', 0, 'k2DBzfPMe9tS4s7Ja5jotxBXaLhbXKyvEqEeGb1T', 1, '2014-07-20 22:11:09', '2014-10-05 12:40:24'),
(20, 'benni@tengja.net', 'bernhard', '$2y$10$KaSJg0mxvDpglm/aCXWO5uLcuXax2/YUHkjVy1k2YfQr9knOPXODm', NULL, 0, 'mkVh7RyNeuqnqavX6rQIz2LkcYylcCLyupaGGWuM', 0, '2014-07-20 22:34:28', '2014-07-20 22:34:28'),
(21, 'helgio06@ru.is', 'helgi', '$2y$10$GvdDqzvbt0AdcLoizhrFMupWgSaGELEAUMaZWKxPqScmRMsDyipnm', NULL, 0, 'udSPfBcz6YpFFgzNbOqgQXquCCOAuTquIYlkXNrb', 1, '2014-07-21 19:32:18', '2014-07-21 19:34:33'),
(22, 'anna@simnet.is', 'annag', '$2y$10$XcUZ7Uu8TOK7kd2SXX8gN.IrsiGo.iZgTFX7L6BccpggYIceeM6dC', NULL, 0, 'q19BSMCvUfpzlirtrUFc6u7DPCsqM12aZz1VINuC', 0, '2014-07-21 21:06:58', '2014-07-21 21:06:58'),
(23, 'sigurdurvs@gmail.com', 'lindin', '$2y$10$x3Hs19YciGHK9KD38rDgpuSxTnOggu.gPBGO4MI8MpMqk7IAQP.Oe', 'd5SHaMdsBwFDsMKhuGmR6bM6yowdZkIX1G4z3K9deWHgoEjJpG1fSZZJ98kg', 0, 't6jADE3lKoFTyxXhSnwexEeqiPdnCN0Gl1dWgoxB', 1, '2014-07-24 17:25:43', '2014-07-24 17:37:57'),
(24, 'bjarniij@gmail.com', 'Bjarni', '$2y$10$RpQwR8JNKztUA4sIDcNYeujeA0Aw/ymMe9diQYD4jx3f2Is/QN8qi', 'Ye1AHfThXxCAFgxjXYVxo0iFmKir4d9wUbhypGSR1hIYNCHEh7mBtfxR482E', 0, 'ZeaayiFCX79K8PX7PYnQl4UvdWelvZ74hY6bFf9T', 1, '2014-07-26 11:14:29', '2014-10-08 23:48:47'),
(25, 'rage23123@gmail.com', 'Mossbeard', '$2y$10$a7rOPU2rvLSS6CceDwEkau2Vses/1AJJBe.LVmLE7w4in3.UN1ctG', '0IKMqAqLBk8cWEc7mf9d8Z9UTieOlRlk2I3ziOfbT20LfkhsyKEWbWdmP6YP', 0, 'bLKH4QFJGvDSQxAWcYZOEJuOX8NhwqmXFitbashc', 1, '2014-07-28 19:12:35', '2014-07-28 19:14:50'),
(26, 'jne3@hi.is', 'Sigga Íslandsdóttir', '$2y$10$CBnGikEXZds7tYx5URiwGew2x8StMrsNqHAfAOLyJef5eDk6hRHlG', '0tblLpwOvbfRczAwfENRtJPBhjNfZWjsD0kDHkaCtOfyBjrY9zjcakCyg9ae', 0, '28hlOkyjxjQSOnmkV1R8lN5kD1SGbQ0nvseHSCXT', 1, '2014-07-28 19:26:59', '2014-07-28 19:58:18'),
(27, 'Gudjontg@gmail.com', 'Gudjontg', '$2y$10$I/QsJZu3AzyYmckCmElOTeBdtfhueBXLz39qCr7dXOXgLIMXwAqIa', 'swRFjcfmjJo3LPFbXK0gCjF5LSFTM6Tk8DQooTBp7AmZVkN8hJRkAV6HTqqc', 0, 'FSgi0OIi9zTpRXFi50oPIOznwl0IYXAPAZ9AcbZO', 1, '2014-07-28 19:30:32', '2014-07-28 19:31:20'),
(28, 'bognabraw@hotmail.com', 'bogna', '$2y$10$qiMM5qtUP7Wz4WNKiApRteokHn6A7dYUCIFWIa1TxI8B0bMdI1.8.', '21bJ4LikrR90o8j3SbA5F8PamWBejAt6r2v3xtNXOnrHnOMycuL2WNSfoF3x', 0, 'm3APWiCkkNW8xetL54Njoiwsedg5wpy46OlrLIb5', 1, '2014-07-28 20:11:52', '2014-07-28 20:16:14'),
(29, 'harpa.gretarsdottir@gmail.com', 'harpa', '$2y$10$IyXxYzOV1.y90m3JKIVylecEVv2ugBtKEsmlRg5/HFyixuK3QqhEO', NULL, 0, 'Olfy1XlvfoNtS8SQDlbYTtNrx7WFMQc4Hl2l6QT7', 1, '2014-07-28 21:19:39', '2014-07-28 21:25:50'),
(30, 'johannamagn@gmail.com', 'johannamagn', '$2y$10$Q2TVhayPx6cWoyjjgMi3Ju84/RQW1gGlDw7SGW93BLnR0LPepx.N6', '0eMvhNAlq4tYGZqsU4YIrayFG0J29WHVfxh9lL49SbeyZAcBXmkvA4RSJjis', 0, 'rwHUp7IjDUHzhSK0Lib46HyEi7DIY6Qw6CvvnAPJ', 1, '2014-07-28 23:59:21', '2014-07-29 00:03:09'),
(31, 'herborg88@gmail.com', 'Herborg', '$2y$10$A4gfgnypPwrVmzXRA.sIPO6HDDeKLWSZ4PB/2FnyfpZvIbYLlUkjy', 'pU9aOiNX0kX2Q6oMjknYRmRB4y1jITQvKpycqueMDtdC91dqkLrOJTkGnMD8', 0, 'BN6aU9zajaNDYjWuz77u2aehYnzdKY997eq6jGa9', 1, '2014-07-29 15:41:02', '2014-08-01 13:40:21'),
(32, 'goldor23@hotmail.com', 'goldor', '$2y$10$tHH9JrRUGGRVcdIl99CON.rSYMrlhi3A13uPOsxQSc6xi8BXwkiVG', '4zI5koRSimg9LVNTOmHV6lUTvdwcdiWqBql5mlnTdleHDg57VQ403w4k7Flw', 0, 'uu6aEamNoKxyxtX7OCXKWAjXId6QDpclmHli4oUk', 1, '2014-07-29 19:05:38', '2014-09-07 15:19:54'),
(33, 'eyvindureli@gmail.com', 'eyvindur', '$2y$10$MBh6INsexH4kWp1ilvNCDO7PkY890.47SLwm3GrJhYWZfpjr642o6', NULL, 0, 'vyWNiMENmuRObPuOpq8SvWfWF6EWdKUIfjm8sCBW', 1, '2014-07-29 19:10:58', '2014-07-29 19:14:22'),
(34, 'hafthor.huni.gudmundsson@samskip.com', 'haftor02', '$2y$10$RXtVbhqErntcHsM1g5sy1eP4cCap4oUC0ZdO67FSPEjHI9t/pktqW', NULL, 0, 'SnIzWNuqBSxcC9jt8BYSqnmMBCes4Ku34GKWbOBt', 1, '2014-07-29 19:16:17', '2014-07-29 19:16:47'),
(35, 'lofjon@simnet.is', 'lofjon', '$2y$10$ci.7e2PqQAzo1DxY1lJ98Oz9LXJfQqGZ2E.4yhIzE1L5twcJsG8Fa', 'L8ohmNxdvc1gpAsbxX9IBmqZvUT0ViIHALZkzl7HBR22YJMGZ00dqg8b3R3o', 0, 'ypchNaDZPb76VTOx3jTWCeMvZ7xfPpRQ2zWO0jM5', 1, '2014-07-29 20:39:17', '2014-07-29 20:46:07'),
(36, 'gretarsdottir.kristin@gmail.com', 'kristin.gretarsdottir', '$2y$10$3KNInuI1ZRl8Qcq1iD5tjOppvFWCTPKBquwr0eFQ.fnwkgvpQ7.6S', NULL, 0, '1zEqXDp83XiHGotUIF4g3Jj7BXUnnnD3SWujOHzP', 0, '2014-07-29 21:44:05', '2014-07-29 21:44:05'),
(37, 'sunnamagg86@gmail.com', 'sum1', '$2y$10$6Ae9utpNF0dtxtmAO6eo5Ot6854G6xRWqPW7A3SzZsFMm8KkjUlD6', NULL, 0, 'XOsinICT2HgiO31kVDpSzbFgC7Xe62Iputwb5TfY', 0, '2014-07-30 10:35:07', '2014-07-30 10:35:07'),
(38, 'maggahg@gmail.com', 'maggahg', '$2y$10$3mnL1L6vg/jvKrUKUa5FEeouwrqQZljH3DIBfZqS73rPNhm0u5t0y', NULL, 0, 'xofJMRyJ659NeDKTnixe4pXJ4QgNjiXikeZ0Lrwr', 0, '2014-07-30 22:36:21', '2014-07-30 22:36:21'),
(40, 'snorri.jakobsson@gmail.com', 'Snorri Ja', '$2y$10$8FzqNRMtMkGcFd5riqO6uOlGHwzbqRHAUR75Iy77n9MIPrligCTMy', NULL, 0, '7eEJLuqupIJnlAnOelenawuAEeE8JthwAi6IME45', 0, '2014-08-27 14:16:10', '2014-08-27 14:16:10'),
(41, 'kjahansson@gmail.com', 'kjarri1', '$2y$10$BpctL3.pICE8Hoop0IoswOT.B0sb33qxd6iv/zqToXEAiGuAxNbZi', NULL, 0, 'uZbyhI7qa3sEpbnsu2raUolHADVKRZSfEi5eulGd', 1, '2014-09-14 11:51:13', '2014-09-30 13:18:59'),
(42, 'jondi@formastudios.com', 'jondijam', '$2y$10$9aAc/5zlDhHanGvTneThJuh0LOz8a9YRHoUdL1ASls1PFNIX93wga', 'M7uBs1QgksAXjLXQRoHyarkw4ckyoMHnaRkCcUsAmcBUEOlvDJEYs4vpWbI3', 0, 'y8iuIXjfias7CPuIZJIOuikBwcQdxiSh5vUrKo8x', 1, '2014-10-19 22:02:32', '2014-10-26 22:39:29'),
(43, 'jon_magnusson@hotmail.com', 'jondijam01', '$2y$10$3sM9rXcSkrQ4S2O6GJMJ8O1TH5p4wyIAjgfxpAIC7UhukLBtBJbki', 'Vrdax7aridcjZ8T7rjyqzgwCHVTohfAnbWqa8RwKxZMHQKVVowkzX4wXTyFC', 0, '', 1, '2014-10-26 21:34:34', '2014-10-26 23:08:15');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
