var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var sass = require('gulp-ruby-sass');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var bower = require('gulp-bower');
var coffee = require('gulp-coffee');
var source = require('vinyl-source-stream');
var eventstream = require("event-stream");
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var watchify = require('watchify');
var rename = require('gulp-rename');
var exec = require('child_process').exec;
var sys = require('sys');



var paths = {
    'dev': {
        'coffeeDir':'app/assets/coffee',
        'targetJSDir':'./public/js',
        'targetProfileJSDir':'./public/js/profile',
        'targetOrganisationJSDir':'./public/js/organisations',
        'sassDir':'app/assets/sass',
        'bowerDir':'public/components',
        'targetCSSDir':'public/css/min'
    }
}

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(paths.dev.bowerDir)) 
});


gulp.task('icons', function() { 
    return gulp.src(paths.dev.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('public/fonts')); 
});

gulp.task('sass', function () {
    
    return gulp.src(paths.dev.sassDir+'/looll.scss')
    .pipe(sass({
        style: 'compressed',
        loadPath: [
                paths.dev.sassDir,
                 paths.dev.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                 paths.dev.bowerDir + '/fontawesome/scss',
                paths.dev.bowerDir + '/jqte/assets'

         ]
    })).on('error', gutil.log)
    .pipe(gulp.dest(paths.dev.targetCSSDir));
});

gulp.task('css', function()
{
    return gulp.src(paths.dev.targetCSSDir+'/looll.css')
    .pipe(autoprefix('last 10 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(paths.dev.targetCSSDir)); 
});

gulp.task('js', function () {
    return gulp.src(paths.dev.coffeeDir + '/**/*.coffee')
        .pipe(coffee().on('error', gutil.log))
        .pipe(gulp.dest(paths.dev.targetJSDir))
});


gulp.task('browserify-organisations', function(){
   return bundle(paths.dev.targetOrganisationJSDir+"/main.js", { debug: true })
    .pipe(rename({ suffix: ".bundle.min" }))
    .pipe(uglify())
    .pipe(gulp.dest(paths.dev.targetOrganisationJSDir+"/dist"));
})


gulp.task('browserify-profile', function()
  {
     return bundle(paths.dev.targetProfileJSDir+"/main.js", { debug: true })
    .pipe(rename({ suffix: ".bundle.min" }))
    .pipe(uglify())
    .pipe(gulp.dest(paths.dev.targetProfileJSDir+"/dist"));
  });

 
// Run all PHPUnit tests
gulp.task('phpunit', function() {
    exec('phpunit', function(error, stdout) {
        sys.puts(stdout);
    });
});

// Keep an eye on Sass, Coffee, and PHP files for changes...
gulp.task('watch', function () {
    //gulp.watch(paths.dev.sassDir + '/**/*.scss', ['sass']);
    gulp.watch(paths.dev.targetProfileJSDir + '/**/*.js', ['browserify-profile']);
    gulp.watch(paths.dev.targetOrganisationJSDir + '/**/*.js', ['browserify-organisations']);
    //gulp.watch('app/**/*.php', ['phpunit']);

});
 



// What tasks does running gulp trigger?
gulp.task('default', ['browserify-organisations', 'browserify-profile', 'watch']);

    
function bundle(files, opts) {
    var streams = [],
        bundler = function(file) {
            opts.entries = "./" + file;

            return browserify(opts).bundle()
            .on("error", function(err) {
                console.log(err);
                // End the stream to prevent gulp from crashing
                this.end();
            })
            .pipe(source(file.split(/[\\/]/).pop()));
        };

    opts = opts || {};

    if (files && files instanceof Array) {
        for (var i = 0, l = files.length; i < l; i++) {
            if (typeof files[i] === "string") {
                streams.push(bundler(files[i]));
            }
        }
    } else if (typeof files === "string") {
        streams.push(bundler(files));
    }

    return eventstream.merge.apply(null, streams).pipe(buffer());
}